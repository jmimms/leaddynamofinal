var config = {};

config.price = .15;
config.adminEmail = 'admin@leaddynamo.co';
config.gmailPassword;
config.domain = 'https://leaddynamo.com'
config.issueInvitesCode;
config.domainName = 'LeadDynamo';
config.stripeSecret;
config.stripePublic;
config.twitterKey;
config.twitterSecret;
config.linkedinKey;
config.linkedinSecret;

module.exports = config;