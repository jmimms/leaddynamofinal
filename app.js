var cluster = require('cluster');
var worker;

if (cluster.isMaster) {
    var cronJob = require('cron').CronJob
        , nodemailer = require('nodemailer')
        , config = require('./config')
        , $ = require('jQuery')
        , _ = require('underscore')
        , stripe = require('stripe')(config.stripeSecret)
        , async = require('async')
        , moment = require('moment')
        , solr = require('solr')
        , fs = require('fs')
        , solrSearch = solr.createClient()
        , parseString = require('xml2js').parseString
        , request = require('request')
        , linkedinClient = require('linkedin-js')(config.linkedinKey, config.linkedinSecret, config.domain)
        , s3 = require('aws2js').load('s3', config.s3Key, config.s3Secret)
        , mongoose = require('mongoose');

    mongoose.connect(config.mongoString);

    var Schema = mongoose.Schema,
        ObjectId = Schema.ObjectId;

    var emailBlasts = new Schema({
        id    : ObjectId
        , followUpMessage : String
        , emailFrom : String
        , nameFrom : String
        , templateMessage : String
        , subject : String
        , leads : [{ type: Schema.Types.ObjectId, ref: 'Lead' }]
        , date : Date
        , dateSend : Date
        , sentDate : Date
        , dateFollowUpSend : Date
        , followUpMessage : String
        , attachments : Schema.Types.Mixed
        , followUpDate : Date
        , type : String
        , activated : Boolean
        , blastId : String
        , creditsSpent : Number
    });

    emailBlasts.set('autoIndex', false);

    var leads = new Schema({
        id    : ObjectId
        , pubMedId : String
        , title : String
        , searchTerm : String
        , authorInfo : String
        , email : String
        , country : String
        , affiliation : String
        , firstname : String
        , lastname : String
        , initials : String
        , institution : String
        , department : String
        , phone : String
        , state : String
        , paperDate : Date
        , address : String
        , type : String
        , linkedinUrl : String
        , linkedinId : String
        , linkedinPictureUrl : String
        , linkedinDate : Date
    });

    leads.set('autoIndex', false);

    leads.index({ pubMedId: 1, email: 1, firstname : 1, lastname : 1, type : 1 });

    var papers = new Schema({
        id    : ObjectId
        , pubMedId : String
        , contents : String
        , type : String
        , database : String
        , json : { type: String, index: false }
        , filename : String
        , dateAdded : Date
        , leadsParsed : Boolean
    });

    papers.set('autoIndex', false);

    papers.index({ pubMedId: 1, filename: 1 });

    var blastHistories = new Schema({
        id    : ObjectId
        , searchTerms : [String]
        , searchArray : [String]
        , isAnd : Boolean
        , date : Date
        , startDate : Date
        , endDate : Date
        , dateUpdated : Date
        , activated : Boolean
        , totalLeads : Number
        , leadCount : Number
        , newLeads : [String]
        , pubMedIds : [String]
        , activatedDate : Date
        , autoRefresh: Boolean
        , leads : [{ type: Schema.Types.ObjectId, ref: 'Lead' }]
        , inactiveLeads : [{ type: Schema.Types.ObjectId, ref: 'Lead' }]
        , emailBlasts : [emailBlasts]
    });

    blastHistories.set('autoIndex', false);

    var lists = new Schema({
        id    : ObjectId
        , searchTerms : [String]
        , searchArray : [String]
        , name : String
        , periodic : Boolean
        , period : String
        , date : Date
        , dateUpdated : Date
        , activated : Boolean
        , leadCount : Number
        , activatedDate : Date
        , leads : [{ type: Schema.Types.ObjectId, ref: 'Lead' }]
        , emailBlasts : [emailBlasts]
    });

    lists.set('autoIndex', false);

    var subscriptionHistories = new Schema({
        id    : ObjectId
        , date : Date
        , price : Number
        , currency : String
        , payMethod : String
        , paymentId : String
    });

    subscriptionHistories.set('autoIndex', false);

    var creditHistories = new Schema({
        id    : ObjectId
        , date : Date
        , price : Number
        , amount : Number
        , currency : String
        , payMethod : String
        , paymentId : String
        , typeId : String
        , type : String
    });

    creditHistories.set('autoIndex', false);

    var emailsValidated = new Schema({
        id    : ObjectId
        , date : Date
        , email : String
        , valid : Boolean
        , firstname : String
        , lastname : String
        , initials : String
        , organization : String
        , ending : String
        , institutionParsed : String
        , topLevelDomain : String
        , paper : String
        , pubMedId : String
        , userSearched : String
    });

    emailsValidated.set('autoIndex', false);

    emailsValidated.index({ pubMedId: 1, email: 1, firstname : 1, lastname : 1, valid : 1, organization : 1, ending : 1 });

    var inviteCodes = new Schema({
        id    : ObjectId
        , code : String
        , user : String
        , used : Boolean
        , usedDate: Date
        , expirationDate : Date
        , issuedDate: Date
        , dateRangeStart : Date
        , dateRangeEnd : Date
        , type : String
        , duration : Schema.Types.Mixed
        , email : String
    });

    inviteCodes.set('autoIndex', false);

    inviteCodes.index({ user: 1, used: 1, code : 1, email : 1 });

    var users = new Schema({
        id    : ObjectId
        , email     : String // used for login confirmation
        , username  : String // optional CRUD on profile view // if added display on all headings
        , password  : String // U on profile view
        , name : String
        , salutation : String
        , firstname : String
        , lastname : String
        , organization : String
        , stripeCustomerId : String
        , title : String
        , credits : Number
        , alreadyEmailed : [String]
        , joinDate  : Date // display on profile view
        , activeDate : Date // display on profile view
        , verified: Boolean
        , subscribed: Boolean
        , lists : [lists]
        , monthFreeStart: Date
        , paidStart: Date
        , verifyCode : String
        , leadTermArray : [Schema.Types.Mixed]
        , creditHistories : [creditHistories]
        , subscriptionHistories: [subscriptionHistories]
        , blastHistories: [blastHistories]
        , signature: String
        , credits : Number
        , tweeted: Boolean
        , linkedin: Boolean
        , emails: [String]
        , dontTalk : [String]
        , emailBlasts : [emailBlasts]
    });

    users.set('autoIndex', false);

    users.index({ email: 1, username: 1, verifyCode : 1 });

    var searchEmails = new Schema({
        id    : ObjectId
        , searchTerms     : [String] // used for login confirmation
        , searchArray : [String]
        , ids  : [String] // optional CRUD on profile view // if added display on all headings
        , titles  : [String] // U on profile view
        , authorInfo  : String // display on profile view
        , emails : [String] // display on profile view
        , customMessage : String
        , from : String
        , date : Date
    });

    searchEmails.set('autoIndex', false);

    var Lead = mongoose.model('Lead', leads)
        , EmailBlast = mongoose.model('EmailBlast', emailBlasts)
        , BlastHistory = mongoose.model('BlastHistory', blastHistories)
        , List = mongoose.model('List', lists)
        , Paper = mongoose.model('Paper', papers)
        , SubscriptionHistory = mongoose.model('SubscriptionHistory', subscriptionHistories)
        , CreditHistory = mongoose.model('CreditHistory', creditHistories)
        , InviteCode = mongoose.model('InviteCode', inviteCodes)
        , EmailValidated = mongoose.model('EmailValidated', emailsValidated)
        , User = mongoose.model('User', users)
        , SearchEmail = mongoose.model('SearchEmail', searchEmails);

    var domainList = [
        {domain : '.ad', country : 'Andorra'},
        {domain : '.ae', country : 'United Arab Emirates'},
        {domain : '.af', country : 'Afghanistan'},
        {domain : '.ag', country : 'Antigua and Barbuda'},
        {domain : '.ai', country : 'Anguilla'},
        {domain : '.al', country : 'Albania'},
        {domain : '.am', country : 'Armenia'},
        {domain : '.an', country : 'Netherlands Antilles'},
        {domain : '.ao', country : 'Angola'},
        {domain : '.aq', country : 'Antarctica'},
        {domain : '.ar', country : 'Argentina'},
        {domain : '.as', country : 'American Samoa'},
        {domain : '.at', country : 'Austria'},
        {domain : '.au', country : 'Australia'},
        {domain : '.aw', country : 'Aruba'},
        {domain : '.az', country : 'Azerbaijan'},
        {domain : '.ba', country : 'Bosnia/Herzegovinia'},
        {domain : '.bb', country : 'Barbados'},
        {domain : '.bd', country : 'Bangladesh'},
        {domain : '.be', country : 'Belgium'},
        {domain : '.bf', country : 'Burkina Faso'},
        {domain : '.bg', country : 'Bulgaria'},
        {domain : '.bh', country : 'Bahrain'},
        {domain : '.bi', country : 'Burundi'},
        {domain : '.bj', country : 'Benin'},
        {domain : '.bm', country : 'Bermuda'},
        {domain : '.bn', country : 'Brunei Darussalam'},
        {domain : '.bo', country : 'Bolivia'},
        {domain : '.br', country : 'Brazil'},
        {domain : '.bs', country : 'Bahamas'},
        {domain : '.bt', country : 'Bhutan'},
        {domain : '.bv', country : 'Bouvet Island'},
        {domain : '.bw', country : 'Botswana'},
        {domain : '.by', country : 'Belarus'},
        {domain : '.bz', country : 'Belize'},
        {domain : '.ca', country : 'Canada'},
        {domain : '.cc', country : 'Cocos Islands - Keelings'},
        {domain : '.cf', country : 'Central African Republic'},
        {domain : '.cg', country : 'Congo'},
        {domain : '.ch', country : 'Switzerland'},
        {domain : '.ci', country : 'Cote D&#226;â‚¬â„¢Ivoire, or Ivory Coast'},
        {domain : '.ck', country : 'Cook Islands'},
        {domain : '.cl', country : 'Chile'},
        {domain : '.cm', country : 'Cameroon'},
        {domain : '.cn', country : 'China'},
        {domain : '.co', country : 'Colombia'},
        {domain : '.cr', country : 'Costa Rica'},
        {domain : '.cs', country : 'Czechoslovakia (former)'},
        {domain : '.cu', country : 'Cuba'},
        {domain : '.cv', country : 'Cape Verde'},
        {domain : '.cx', country : 'Christmas Island'},
        {domain : '.cy', country : 'Cyprus'},
        {domain : '.cz', country : 'Czech Republic'},
        {domain : '.de', country : 'Germany'},
        {domain : '.dj', country : 'Djibouti'},
        {domain : '.dk', country : 'Denmark'},
        {domain : '.dm', country : 'Dominica'},
        {domain : '.do', country : 'Dominican Republic'},
        {domain : '.dz', country : 'Algeria'},
        {domain : '.ec', country : 'Ecuador'},
        {domain : '.ee', country : 'Estonia'},
        {domain : '.eg', country : 'Egypt'},
        {domain : '.eh', country : 'Western Sahara '},
        {domain : '.er', country : 'Eritrea'},
        {domain : '.es', country : 'Spain'},
        {domain : '.et', country : 'Ethiopia'},
        {domain : '.eu', country : 'European Union'},
        {domain : '.fi', country : 'Finland'},
        {domain : '.fj', country : 'Fiji'},
        {domain : '.fk', country : 'Falkland Islands/Malvinas'},
        {domain : '.fm', country : 'Micronesia'},
        {domain : '.fo', country : 'Faroe Islands'},
        {domain : '.fr', country : 'France'},
        {domain : '.fx', country : 'Metropolitan France'},
        {domain : '.ga', country : 'Gabon'},
        {domain : '.gb', country : 'Great Britain'},
        {domain : '.gd', country : 'Grenada'},
        {domain : '.ge', country : 'Georgia'},
        {domain : '.gf', country : 'French Guiana'},
        {domain : '.gh', country : 'Ghana'},
        {domain : '.gi', country : 'Gibraltar'},
        {domain : '.gl', country : 'Greenland'},
        {domain : '.gm', country : 'Gambia'},
        {domain : '.gn', country : 'Guinea'},
        {domain : '.gp', country : 'Guadeloupe'},
        {domain : '.gq', country : 'Equatorial Guinea'},
        {domain : '.gr', country : 'Greece'},
        {domain : '.gs', country : 'South Georgia and South Sandwich Islands'},
        {domain : '.gt', country : 'Guatemala'},
        {domain : '.gu', country : 'Guam'},
        {domain : '.gw', country : 'Guinea-Bissau'},
        {domain : '.gy', country : 'Guyana'},
        {domain : '.hk', country : 'Hong Kong'},
        {domain : '.hm', country : 'Heard and McDonald Islands'},
        {domain : '.hn', country : 'Honduras'},
        {domain : '.hr', country : 'Croatia/Hrvatska'},
        {domain : '.ht', country : 'Haiti'},
        {domain : '.hu', country : 'Hungary'},
        {domain : '.id', country : 'Indonesia'},
        {domain : '.ie', country : 'Ireland'},
        {domain : '.il', country : 'Israel'},
        {domain : '.in', country : 'India'},
        {domain : '.io', country : 'British Indian Ocean Territory'},
        {domain : '.iq', country : 'Iraq'},
        {domain : '.ir', country : 'Iran'},
        {domain : '.is', country : 'Iceland'},
        {domain : '.it', country : 'Italy'},
        {domain : '.jm', country : 'Jamaica'},
        {domain : '.jo', country : 'Jordan'},
        {domain : '.jp', country : 'Japan'},
        {domain : '.ke', country : 'Kenya'},
        {domain : '.kg', country : 'Kyrgyzstan'},
        {domain : '.kh', country : 'Cambodia'},
        {domain : '.ki', country : 'Kiribati'},
        {domain : '.km', country : 'Comoros'},
        {domain : '.kn', country : 'Saint Kitts and Nevis'},
        {domain : '.kp', country : 'North Korea'},
        {domain : '.kr', country : 'South Korea'},
        {domain : '.kw', country : 'Kuwait'},
        {domain : '.ky', country : 'Cayman Islands'},
        {domain : '.kz', country : 'Kazakhstan'},
        {domain : '.la', country : 'Laos'},
        {domain : '.lb', country : 'Lebanon'},
        {domain : '.lc', country : 'Saint Lucia'},
        {domain : '.li', country : 'Liechtenstein'},
        {domain : '.lk', country : 'Sri Lanka'},
        {domain : '.lr', country : 'Liberia'},
        {domain : '.ls', country : 'Lesotho'},
        {domain : '.lt', country : 'Lithuania'},
        {domain : '.lu', country : 'Luxembourg'},
        {domain : '.lv', country : 'Latvia'},
        {domain : '.ly', country : 'Libya'},
        {domain : '.ma', country : 'Morocco'},
        {domain : '.mc', country : 'Monaco'},
        {domain : '.md', country : 'Moldova'},
        {domain : '.mg', country : 'Madagascar'},
        {domain : '.mh', country : 'Marshall Islands'},
        {domain : '.mk', country : 'Macedonia'},
        {domain : '.ml', country : 'Mali'},
        {domain : '.mm', country : 'Myanmar'},
        {domain : '.mn', country : 'Mongolia'},
        {domain : '.mo', country : 'Macau'},
        {domain : '.mp', country : 'Northern Mariana Islands'},
        {domain : '.mq', country : 'Martinique'},
        {domain : '.mr', country : 'Mauritania'},
        {domain : '.ms', country : 'Montserrat'},
        {domain : '.mt', country : 'Malta'},
        {domain : '.mu', country : 'Mauritius'},
        {domain : '.mv', country : 'Maldives'},
        {domain : '.mw', country : 'Malawi'},
        {domain : '.mx', country : 'Mexico'},
        {domain : '.my', country : 'Malaysia'},
        {domain : '.mz', country : 'Mozambique'},
        {domain : '.na', country : 'Namibia'},
        {domain : '.nc', country : 'New Caledonia'},
        {domain : '.ne', country : 'Niger'},
        {domain : '.nf', country : 'Norfolk Island'},
        {domain : '.ng', country : 'Nigeria'},
        {domain : '.ni', country : 'Nicaragua'},
        {domain : '.nl', country : 'Netherlands'},
        {domain : '.no', country : 'Norway'},
        {domain : '.np', country : 'Nepal'},
        {domain : '.nr', country : 'Nauru'},
        {domain : '.nt', country : 'Neutral Zone'},
        {domain : '.nu', country : 'Niue'},
        {domain : '.nz', country : 'New Zealand (Aotearoa)'},
        {domain : '.om', country : 'Oman'},
        {domain : '.pa', country : 'Panama'},
        {domain : '.pe', country : 'Peru'},
        {domain : '.pf', country : 'French Polynesia'},
        {domain : '.pg', country : 'Papua New Guinea'},
        {domain : '.ph', country : 'Philippines'},
        {domain : '.pk', country : 'Pakistan'},
        {domain : '.pl', country : 'Poland'},
        {domain : '.pm', country : 'St. Pierre and Miquelon'},
        {domain : '.pn', country : 'Pitcairn'},
        {domain : '.pr', country : 'Puerto Rico'},
        {domain : '.pt', country : 'Portugal'},
        {domain : '.pw', country : 'Palau'},
        {domain : '.py', country : 'Paraguay'},
        {domain : '.qa', country : 'Qatar'},
        {domain : '.re', country : 'Reunion'},
        {domain : '.ro', country : 'Romania'},
        {domain : '.ru', country : 'Russian Federation'},
        {domain : '.rw', country : 'Rwanda'},
        {domain : '.sa', country : 'Saudi Arabia'},
        {domain : '.sb', country : 'Solomon Islands'},
        {domain : '.sc', country : 'Seychelles'},
        {domain : '.sd', country : 'Sudan'},
        {domain : '.se', country : 'Sweden'},
        {domain : '.sg', country : 'Singapore'},
        {domain : '.sh', country : 'Saint Helena'},
        {domain : '.si', country : 'Slovenia'},
        {domain : '.sj', country : 'Svalbard and Jan Mayen Islands'},
        {domain : '.sk', country : 'Slovakia'},
        {domain : '.sl', country : 'Sierra Leone'},
        {domain : '.sm', country : 'San Marino'},
        {domain : '.sn', country : 'Senegal'},
        {domain : '.so', country : 'Somalia'},
        {domain : '.sr', country : 'Suriname'},
        {domain : '.st', country : 'Sao Torme and Principe'},
        {domain : '.su', country : 'Former USSR'},
        {domain : '.sv', country : 'El Salvador'},
        {domain : '.sy', country : 'Syria'},
        {domain : '.sz', country : 'Swaziland'},
        {domain : '.tc', country : 'Turks and Caicos Islands'},
        {domain : '.td', country : 'Chad'},
        {domain : '.tf', country : 'French Southern Territory'},
        {domain : '.tg', country : 'Togo'},
        {domain : '.th', country : 'Thailand'},
        {domain : '.tj', country : 'Tajikistan'},
        {domain : '.tk', country : 'Tokelau'},
        {domain : '.tm', country : 'Turkmenistan'},
        {domain : '.tn', country : 'Tunisia'},
        {domain : '.to', country : 'Tonga'},
        {domain : '.tp', country : 'East Timor'},
        {domain : '.tr', country : 'Turkey'},
        {domain : '.tt', country : 'Trinidad and Tobago'},
        {domain : '.tv', country : 'Tuvalu'},
        {domain : '.tw', country : 'Taiwan'},
        {domain : '.tz', country : 'Tanzania'},
        {domain : '.ua', country : 'Ukraine'},
        {domain : '.ug', country : 'Uganda'},
        {domain : '.uk', country : 'United Kingdom'},
        {domain : '.um', country : 'Minor Outlying Islands'},
        {domain : '.us', country : 'USA'},
        {domain : '.uy', country : 'Uruguay'},
        {domain : '.uz', country : 'Uzbekistan'},
        {domain : '.va', country : 'Vatican City State'},
        {domain : '.vc', country : 'Saint Vincent and the Grenadines'},
        {domain : '.ve', country : 'Venezuela'},
        {domain : '.vg', country : 'British Virgin Islands'},
        {domain : '.vi', country : 'Virgin Islands'},
        {domain : '.vn', country : 'Viet Nam'},
        {domain : '.vu', country : 'Vanuatu'},
        {domain : '.wf', country : 'Wallis and Futuna Islands'},
        {domain : '.ws', country : 'Samoa'},
        {domain : '.ye', country : 'Yemen'},
        {domain : '.yt', country : 'Mayotte'},
        {domain : '.yu', country : 'Yugoslavia'},
        {domain : '.za', country : 'South Africa'},
        {domain : '.zm', country : 'Zambia'},
        {domain : '.zr', country : 'Zaire'},
        {domain : '.zw', country : 'Zimbabwe'}
    ];

    function checkLinkedIn(leadId){
        Lead.findById(leadId, function(err,leadz){
            if (!isEmpty(leadz) && leadz.country && leadz.lastname && leadz.institution && !leadz.linkedinUrl){
                var passThrough = true;

                if (leadz.linkedinDate){
                    var then = leadz.linkedinDate.getTime(),
                        now  = new Date().getTime(),
                        thirtyDaysInMilliseconds = 1592000000;
                    if (now - then < thirtyDaysInMilliseconds) {
                        passThrough = false;
                    }
                }

                if (passThrough){
                    domainList.forEach(function(dou){
                        if (dou.country == leadz.country){
                            var countryCode = dou.domain.substring(1);
                            linkedinClient.apiCall('GET', '/people-search?keywords=' + encodeURIComponent(leadz.institution) + '&last-name=' + leadz.lastname + '&country-code=' + countryCode,
                                {
                                    token: {
                                        oauth_token_secret: config.linkedinOAuthSecret
                                        , oauth_token: config.linkedinOAuth
                                    }
                                }
                                , function (error, result) {
                                    if (result && result[0]){
                                        linkedinClient.apiCall('GET', '/people/id=' + result[0].id + ':(public-profile-url,picture-url)',
                                            {
                                                token: {
                                                    oauth_token_secret: config.linkedinOAuthSecret
                                                    , oauth_token: config.linkedinOAuth
                                                }
                                            }
                                            , function (error, resulted) {
                                                if (resulted){
                                                    leadz.linkedinUrl = resulted.publicProfileUrl;
                                                    leadz.linkedinId = result[0].id;
                                                    leadz.linkedinPictureUrl = resulted.pictureUrl;
                                                    leadz.linkedinDate = new Date();
                                                    leadz.save(function(err,lead){});
                                                }
                                            }
                                        );
                                    }
                                    else {
                                        leadz.linkedinDate = new Date();
                                        leadz.save(function(err,lead){});
                                    }
                                }
                            );
                        }
                    });
                }
            }
        });
    }

    function isEmpty(ob){
        for(var i in ob){ if(ob.hasOwnProperty(i)){return false;}}
        return true;
    }

    function escapeRegExp(str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }

    function sendEmail(toEmail, plainText, html, subject, fromed, name, attachments) {
        var smtpTransport = nodemailer.createTransport("SMTP",{
            service: "Gmail",
            auth: {
                user: config.adminEmail,
                pass: config.gmailPassword
            }
        });
        var from = name + " <" + fromed + ">";

        if (attachments){
            var mailOptions = {
                from: from,
                to: toEmail,
                subject: subject,
                text: plainText,
                html: html,
                replyTo: from,
                attachments: attachments
            };
        }
        else {
            var mailOptions = {
                from: from,
                to: toEmail,
                subject: subject,
                text: plainText,
                replyTo: from,
                html: html
            };
        }

        smtpTransport.sendMail(mailOptions, function(error, response){
            if (!isEmpty(error)){

            }else{

            }
            smtpTransport.close();
        });
    }

    var jobArray = [];

    var cpuCount = require('os').cpus().length;

    for (var i = 0; i < cpuCount; i += 1) {
        worker = cluster.fork();
        worker.on('message', function(msg) {
            if (msg.cancel){
                if (jobArray && jobArray[0]){
                    var count = 0;
                    jobArray.forEach(function(jobs){
                        if (jobs.id.toString() == msg.cancel.id.toString() && jobs.type == msg.type){
                            jobs.job.stop();
                            jobArray.splice(count, 1);
                        }
                        count += 1;
                    });
                }
            }
            else if (msg.add && msg.add.userId){
                if (msg.add.type == 'followUp'){
                    User.findById(msg.add.userId, '_id emailBlasts').exec(function(err,used){
                        if (!isEmpty(used)){
                            var jobOb = {};
                            var emailBlast = used.emailBlasts.id(msg.add.id);

                            if (emailBlast.dateFollowUpSend){

                                var job = new cronJob(emailBlast.dateFollowUpSend, function(){
                                        User.findById(msg.add.userId).select('_id emailBlasts').exec(function(err,used){
                                            if (!isEmpty(used)){
                                                var blast = used.emailBlasts.id(msg.add.id);

                                                Lead.find({_id: { $in: blast.leads}}).batchSize(100000).exec(function(err,leads){
                                                    if (!isEmpty(leads)){
                                                        leads.forEach(function(author){
                                                            var resFollow = blast.followUpMessage.toString();
                                                            var term;
                                                            if (used.leadTermArray && used.leadTermArray[0]){
                                                                used.leadTermArray.forEach(function(led){
                                                                    if (led._id.toString() == author._id.toString()){
                                                                        term = led.term;
                                                                    }
                                                                });
                                                            }
                                                            if (term){
                                                                resFollow = resFollow.replace('<span class="SEARCHTERM" contenteditable="false">SEARCHTERM</span>', term);
                                                            }
                                                            resFollow = resFollow.replace('<span class="PAPER" contenteditable="false">PAPER</span>', author.title);
                                                            resFollow = resFollow.replace('<span class="NAME" contenteditable="false">NAME</span>', 'Dr. ' + author.lastname);
                                                            resFollow = resFollow.replace('<span class="INSTITUTION" contenteditable="false">INSTITUTION</span>', author.institution);
                                                            resFollow = resFollow.replace('<span class="DATEPAPER" contenteditable="false">DATEPAPER</span>', moment(author.paperDate).format("dddd, MMMM Do YYYY"));
                                                            resFollow = resFollow.replace('<span class="DATE" contenteditable="false">DATE</span>', moment(blast.date).format("dddd, MMMM Do YYYY"));
                                                            var finalMessageHTMLFollow;
                                                            var finalMessagePlaintextFollow;

                                                            var resSub = blast.subject.toString();
                                                            if (term){
                                                                resSub = resSub.replace('<span class="SEARCHTERM" contenteditable="false">SEARCHTERM</span>', term);
                                                            }
                                                            resSub = resSub.replace('<span class="PAPER" contenteditable="false">PAPER</span>', author.title);
                                                            resSub = resSub.replace('<span class="NAME" contenteditable="false">NAME</span>', 'Dr. ' + author.lastname);
                                                            resSub = resSub.replace('<span class="INSTITUTION" contenteditable="false">INSTITUTION</span>', author.institution);
                                                            resSub = resSub.replace('<span class="DATEPAPER" contenteditable="false">DATEPAPER</span>', moment(author.paperDate).format("dddd, MMMM Do YYYY"));
                                                            resSub = resSub.replace('<span class="DATE" contenteditable="false">DATE</span>', moment(blast.date).format("dddd, MMMM Do YYYY"));

                                                            if (typeof blast.signature == 'string'){
                                                                finalMessageHTMLFollow = $(resFollow + "<br><br>" + blast.signature).text();
                                                                finalMessagePlaintextFollow = $(resFollow + ' ' + blast.signature).text();
                                                            }
                                                            else {
                                                                finalMessageHTMLFollow = $(resFollow).text();
                                                                finalMessagePlaintextFollow = $(resFollow).text();
                                                            }

                                                            finalMessageHTMLFollow += '<br><br><br><a target="_blank" href=' + config.domain + '/doNotTalk?userId=' + used._id.toString() + '&emailId=' + blast._id.toString() + '&leadId=' + author._id.toString() + ' style="color: #cccccc;">Do not hear from me again.</a>';

                                                            sendEmail(author.email, finalMessagePlaintextFollow, finalMessageHTMLFollow, resSub, blast.emailFrom, blast.nameFrom, '');
                                                        });

                                                        blast.followUpDate = new Date();
                                                        used.save(function(err,finalUsed){

                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }, function () {
                                    },
                                    true,
                                    'UTC'
                                );
                                jobOb.job = job;
                                jobOb.type = 'followUp';
                                jobOb.id = emailBlast._id;
                                jobArray.push(jobOb);
                            }
                        }
                    });
                }
                else if (msg.add.type == 'emailMain'){
                    User.findById(msg.add.userId, '_id emailBlasts').exec(function(err,used){
                        if (!isEmpty(used)){
                            var jobOb = {};
                            var emailBlast = used.emailBlasts.id(msg.add.id);
                            if (emailBlast.dateSend){
                                var jobMain = new cronJob(emailBlast.dateSend, function(){
                                        User.findById(msg.add.userId).select('_id emailBlasts').exec(function(err,used){

                                            if (!isEmpty(used)){
                                                var blast = used.emailBlasts.id(emailBlast._id);

                                                Lead.find({_id: { $in: blast.leads}}).batchSize(100000).exec(function(err,leads){
                                                    if (!isEmpty(leads)){
                                                        leads.forEach(function(author){
                                                            var resed = blast.templateMessage.toString();
                                                            var term;
                                                            if (used.leadTermArray && used.leadTermArray[0]){
                                                                used.leadTermArray.forEach(function(led){
                                                                    if (led._id.toString() == author._id.toString()){
                                                                        term = led.term;
                                                                    }
                                                                });
                                                            }
                                                            if (term){
                                                                resed = resed.replace('<span class="SEARCHTERM" contenteditable="false">SEARCHTERM</span>', term);
                                                            }
                                                            resed = resed.replace('<span class="PAPER" contenteditable="false">PAPER</span>', author.title);
                                                            resed = resed.replace('<span class="NAME" contenteditable="false">NAME</span>', 'Dr. ' + author.lastname);
                                                            resed = resed.replace('<span class="INSTITUTION" contenteditable="false">INSTITUTION</span>', author.institution);
                                                            resed = resed.replace('<span class="DATEPAPER" contenteditable="false">DATEPAPER</span>', moment(author.paperDate).format("dddd, MMMM Do YYYY"));
                                                            resed = resed.replace('<span class="DATE" contenteditable="false">DATE</span>', moment(blast.date).format("dddd, MMMM Do YYYY"));

                                                            var resSub = blast.subject.toString();
                                                            if (term){
                                                                resSub = resSub.replace('<span class="SEARCHTERM" contenteditable="false">SEARCHTERM</span>', term);
                                                            }
                                                            resSub = resSub.replace('<span class="PAPER" contenteditable="false">PAPER</span>', author.title);
                                                            resSub = resSub.replace('<span class="NAME" contenteditable="false">NAME</span>', 'Dr. ' + author.lastname);
                                                            resSub = resSub.replace('<span class="INSTITUTION" contenteditable="false">INSTITUTION</span>', author.institution);
                                                            resSub = resSub.replace('<span class="DATEPAPER" contenteditable="false">DATEPAPER</span>', moment(author.paperDate).format("dddd, MMMM Do YYYY"));
                                                            resSub = resSub.replace('<span class="DATE" contenteditable="false">DATE</span>', moment(blast.date).format("dddd, MMMM Do YYYY"));
                                                            var finalMessageHTML;
                                                            var finalMessagePlaintext;
                                                            if (typeof blast.signature == 'string'){
                                                                finalMessageHTML = resed + "<br><br>" + blast.signature;
                                                                finalMessagePlaintext = $(resed + ' ' + blast.signature).text();
                                                            }
                                                            else {
                                                                finalMessageHTML = resed;
                                                                finalMessagePlaintext = $(resed).text();
                                                            }

                                                            finalMessageHTML += '<br><br><br><a target="_blank" href=' + config.domain + '/doNotTalk?userId=' + used._id.toString() + '&emailId=' + blast._id.toString() + '&leadId=' + author._id.toString() + ' style="color: #cccccc;">Do not hear from me again.</a>';

                                                            sendEmail(author.email, finalMessagePlaintext, finalMessageHTML, resSub, blast.emailFrom, blast.nameFrom, blast.attachments);
                                                        });

                                                        blast.sentDate = new Date();

                                                        used.save(function(err,finalUsed){

                                                        });
                                                    }
                                                });
                                            }

                                        });
                                    }, function () {
                                    },
                                    true,
                                    'UTC'
                                );
                                jobOb.job = jobMain;
                                jobOb.type = 'emailMain';
                                jobOb.id = emailBlast._id;
                                jobArray.push(jobOb);
                            }
                            else {
                                Lead.find({_id: { $in: emailBlast.leads}}).batchSize(100000).exec(function(err,leads){
                                    if (!isEmpty(leads)){
                                        leads.forEach(function(author){
                                            var resed = emailBlast.templateMessage.toString();
                                            var term;
                                            if (used.leadTermArray && used.leadTermArray[0]){
                                                used.leadTermArray.forEach(function(led){
                                                    if (led._id.toString() == author._id.toString()){
                                                        term = led.term;
                                                    }
                                                });
                                            }
                                            if (term){
                                                resed = resed.replace('<span class="SEARCHTERM" contenteditable="false">SEARCHTERM</span>', term);
                                            }
                                            resed = resed.replace('<span class="PAPER" contenteditable="false">PAPER</span>', author.title);
                                            resed = resed.replace('<span class="NAME" contenteditable="false">NAME</span>', 'Dr. ' + author.lastname);
                                            resed = resed.replace('<span class="INSTITUTION" contenteditable="false">INSTITUTION</span>', author.institution);
                                            resed = resed.replace('<span class="DATEPAPER" contenteditable="false">DATEPAPER</span>', moment(author.paperDate).format("dddd, MMMM Do YYYY"));
                                            resed = resed.replace('<span class="DATE" contenteditable="false">DATE</span>', moment(emailBlast.date).format("dddd, MMMM Do YYYY"));

                                            var resSub = emailBlast.subject.toString();
                                            if (term){
                                                resSub = resSub.replace('<span class="SEARCHTERM" contenteditable="false">SEARCHTERM</span>', term);
                                            }
                                            resSub = resSub.replace('<span class="PAPER" contenteditable="false">PAPER</span>', author.title);
                                            resSub = resSub.replace('<span class="NAME" contenteditable="false">NAME</span>', 'Dr. ' + author.lastname);
                                            resSub = resSub.replace('<span class="INSTITUTION" contenteditable="false">INSTITUTION</span>', author.institution);
                                            resSub = resSub.replace('<span class="DATEPAPER" contenteditable="false">DATEPAPER</span>', moment(author.paperDate).format("dddd, MMMM Do YYYY"));
                                            resSub = resSub.replace('<span class="DATE" contenteditable="false">DATE</span>', moment(emailBlast.date).format("dddd, MMMM Do YYYY"));
                                            var finalMessageHTML;
                                            var finalMessagePlaintext;
                                            if (typeof emailBlast.signature == 'string'){
                                                finalMessageHTML = resed + "<br><br>" + emailBlast.signature;
                                                finalMessagePlaintext = $(resed + ' ' + emailBlast.signature).text();
                                            }
                                            else {
                                                finalMessageHTML = resed;
                                                finalMessagePlaintext = $(resed).text();
                                            }

                                            finalMessageHTML += '<br><br><br><a target="_blank" href=' + config.domain + '/doNotTalk?userId=' + used._id.toString() + '&emailId=' + emailBlast._id.toString() + '&leadId=' + author._id.toString() + ' style="color: #cccccc;">Do not hear from me again.</a>';

                                            sendEmail(author.email, finalMessagePlaintext, finalMessageHTML, resSub, emailBlast.emailFrom, emailBlast.nameFrom, emailBlast.attachments);
                                        });

                                        emailBlast.sentDate = new Date();

                                        used.save(function(err,finalUsed){

                                        });
                                    }
                                });
                            }
                        }
                    });
                }
                else if (msg.add.type == 'refresh'){
                    User.findById(msg.add.userId).select('_id blastHistories').where('blastHistories.autoRefresh').equals(true).exec(function(err,used){
                        if (!isEmpty(used)){
                            var blast = used.blastHistories.id(msg.add.id);
                            if (!isEmpty(blast)){
                                var jobOb = {};
                                var jobMain = new cronJob('00 30 11 * * 1-5', function(){
                                        runRefresh(msg.add.userId, msg.add.id);
                                    }, function () {
                                    },
                                    true,
                                    'UTC'
                                );
                                runRefresh(msg.add.userId, msg.add.id);
                                jobOb.job = jobMain;
                                jobOb.type = 'refresh';
                                jobOb.id = msg.add.id;
                                jobArray.push(jobOb);
                            }
                        }
                    });
                }
            }
        });
    }

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    var globalBlastEmailValidationQueue = async.queue(function (task, callback) {

        var email = task.email;
        var emailParse = email.split('@');

        EmailValidated.findOne({$and: [
            { pubMedId : task.item.id },
            { firstname : task.author.firstname },
            { lastname : task.author.lastname }
        ]},function(err,emVal){
            if (!isEmpty(emVal)){
                task.author.email = emVal.email;
                callback(task.email, task.author);
            }
            else {
                exec('nslookup -q=mx ' + emailParse[1], function (error, stdout, stderr) {
                    var resed;
                    if (stdout && typeof stdout.toString() == 'string'){
                        resed = stdout.match(/mail addr = .*\n/);

                        if (resed && resed[0]){
                            resed = resed[0].match(/([a-z0-9]+\.)*[a-z0-9-]+\.[a-z]+/i);
                        }
                        else {
                            resed = stdout.match(/mail exchanger = .*\n/);
                        }
                        if (resed && resed[0]){
                            resed = resed[0].match(/([a-z0-9]+\.)*[a-z0-9-]+\.[a-z]+/i);
                        }
                    }
                    if (resed && resed[0]){
                        var valid;

                        var conn = net.connect({port: 25},resed[0],
                            function() {
                                var counter = 0;
                                var before;
                                conn.on("data", function (c) {
                                    counter += 1;
                                    c = c.toString();
                                    if (c.match(/250/) && c.match(/250/)[0] && !before){
                                        before = true;
                                        counter = 1;
                                    }
                                    if (counter == 1 && before){
                                        conn.write("mail from: <" + adminEmail + ">\n");
                                    }
                                    else if (counter == 2){
                                        conn.write("rcpt to: <" + email + ">\n");
                                    }
                                    else if (counter == 3){
                                        if (c.match(/250/) && c.match(/250/)[0]){
                                            valid = true;
                                            task.author.email = email;
                                        }
                                        else {
                                            valid = false;
                                        }
                                        conn.end();
                                    }
                                });
                                conn.on("end", function () {
                                    var validatedObject = new EmailValidated({date : new Date(), email : email, firstname : task.author.firstname, lastname : task.author.lastname, valid : valid, initials : task.author.initials, pubMedId : task.item.id, paper : task.item.title, userSearched : task.user});
                                    validatedObject.save(function(err,validObj){
                                        callback(task.email, task.author);
                                    });
                                });
                                conn.write("helo hi\n");
                            });
                        conn.on('error',function(e){
                            var validatedObject = new EmailValidated({date : new Date(), email : email, firstname : task.author.firstname, lastname : task.author.lastname, valid : valid, initials : task.author.initials, pubMedId : task.item.id, paper : task.item.title, userSearched : task.user});
                            validatedObject.save(function(err,validObj){
                                callback(task.email, task.author);
                            });
                        });
                    }
                    else {
                        callback(task.email, task.author);
                    }
                });
            }
        });
    }, queueLimit);

    Array.prototype.getUnique = function(){
        var u = {}, a = [];
        for(var i = 0, l = this.length; i < l; ++i){
            if(u.hasOwnProperty(this[i])) {
                continue;
            }
            a.push(this[i]);
            u[this[i]] = 1;
        }
        return a;
    }

    function sendRefreshEmail(email, firstname, lastname, title, organization, credits, newLeadCount, chargeLeads, amount){
        var toEmail = firstname + lastname + " <" + email + ">"
            , subject = (newLeadCount + chargeLeads).toString() + ' new leads on LeadDynamo'
            , fromed = "LeadDynamo <" + config.adminEmail + ">"
            , plainText = (newLeadCount + chargeLeads).toString() + ' new leads on LeadDynamo'
            , html;

        if (chargeLeads && chargeLeads[0]){
            html = "Dear " + title + ' ' + firstname + ' ' + lastname + ', <br><br>You now have ' + (newLeadCount + chargeLeads).toString() + ' new leads on <a href="https://leaddynamo.info" target="_blank">LeadDynamo</a>. LeadDynamo charged your credit card ' + amount + '. <br><br>Regards,<br><br>LeadDynamo';
        }
        else {
            html = "Dear " + title + ' ' + firstname + ' ' + lastname + ', <br><br>You now have ' + (newLeadCount + chargeLeads).toString() + ' new leads on <a href="https://leaddynamo.info" target="_blank">LeadDynamo</a>.<br><br>Regards,<br><br>LeadDynamo';
        }
        sendEmail(toEmail, plainText, html, subject, config.adminEmail, "LeadDynamo", []);
    }

    function parseIds(bodied){
        var ids = [];
        if (bodied && bodied.eSearchResult && bodied.eSearchResult.IdList && bodied.eSearchResult.IdList[0] && bodied.eSearchResult.IdList[0].Id){
            ids = bodied.eSearchResult.IdList[0].Id;
        }
        return ids;
    }

    var saveQueue = async.queue(function (task, callback) {
        if (task.lead){
            Lead.findOne({$and: [
                { pubMedId : task.nr.id },
                { firstname : task.author.firstname },
                { lastname : task.author.lastname },
                { email : task.author.email }
            ]},function(err,leaded){
                if (!isEmpty(leaded)){
                    callback(null, null);
                }
                else {
                    var newLer = new Lead({affiliation : task.author.affiliation, country : task.author.country, paperDate : task.nr.date, searchTerm : task.nr.searchTerm, title : task.nr.title, pubMedId : task.nr.id, email : task.author.email, phone : task.author.phone, firstname : task.author.firstname, lastname : task.author.lastname, initials : task.author.initials});
                    newLer.save(function(err,bun){
                        callback(null, null);
                    });
                }
            });
        }
        else {
            Paper.findOne({pubMedId : task.nr.id},function(err,pubbed){
                if (isEmpty(pubbed)){
                    var paped = new Paper({pubMedId : task.nr.id, dateAdded : new Date(), type : 'remote', database : 'PubMed', json : JSON.stringify(task.nr.json), leadsParsed : true});
                    paped.save(function(err,paped){
                        callback(null, null);
                    });
                }
            });
        }
    }, 5);

    function runRefresh(userId, blastId){
        User.findById(userId).select('_id blastHistories emails').where('blastHistories.autoRefresh').equals(true).exec(function(err, used){
            if (!isEmpty(used)){
                var blast = used.blastHistories.id(blastId);
                if (!isEmpty(blast)){
                    var finalIdArray = [];
                    var searchArray = blast.searchArray;
                    var andSearchString = '';
                    var global = $.inArray("Global", searchArray);
                    var startDate = moment().subtract(2, 'month').format('YYYY/MM/DD');
                    var endDate = moment().format('YYYY/MM/DD');
                    var searchLinker = 'OR';

                    if (blast.isAnd){
                        searchLinker = 'AND';
                    }

                    var andCounter = 0;
                    blast.searchTerms.forEach(function(item){

                        var searchString = '';

                        if (searchArray.length > 1){
                            if (global >= 0){
                                searchString = item;
                            }
                            else {
                                var counter = 0;
                                searchArray.forEach(function(search){
                                    if (counter > 0){
                                        searchString = '(' + searchString;
                                    }
                                    if (counter < (searchArray.length - 1)){
                                        searchString += item + '[' + search + '])';
                                    }
                                    else {
                                        searchString += item + '[' + search + ']';
                                    }
                                    counter += 1;
                                });
                            }
                        }
                        else {
                            if (global >= 0){
                                searchString = item;
                            }
                            else {
                                searchString = item + '[' + searchArray[0] + ']';
                            }
                        }

                        if (blast.searchTerms.length > 1){
                            if (andCounter == (blast.searchTerms.length - 1)){
                                searchString = '(' + searchString + ')';
                            }
                            else {
                                searchString = '(' + searchString + ') ' + searchLinker;
                            }
                        }

                        andSearchString += searchString;
                        andCounter +=1;
                    });

                    request.get({
                        url: "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&datetype=edat&retmax=100000&term=" + encodeURI(andSearchString) + + '&mindate=' + encodeURI(startDate) + '&maxdate=' + encodeURI(endDate)
                    }, function (error, response, xmlDoc) {
                        if (xmlDoc){
                            parseString(xmlDoc, function (err, bodied) {
                                if (bodied){
                                    var finalIdArray = parseIds(bodied);

                                    if (finalIdArray && finalIdArray[0]){
                                        var newItems = [];
                                        finalIdArray.forEach(function(findItem){
                                            if ($.inArray(findItem.id, blast.pubMedIds) < 0){
                                                newItems.push(findItem);
                                            }
                                        });
                                        if (newItems && newItems[0]){
                                            var ids = [];

                                            newItems.forEach(function(finalIds){
                                                ids.push(finalIds.id);
                                            });

                                            var finalIded = ids;

                                            var finalResults = [];

                                            var totalFound = 0;
                                            Lead.find({'pubMedId': { $in: ids}}, function(err,loded){
                                                Paper.find({'pubMedId': { $in: ids}}, 'pubMedId', function(err,papers){
                                                    if (!isEmpty(papers)){
                                                        papers.forEach(function(pap){
                                                            var paperLead = [];
                                                            var newResults;
                                                            if (loded && loded[0]){
                                                                loded.forEach(function(led){
                                                                    if (led.pubMedId == pap.pubMedId){
                                                                        paperLead.push(led);
                                                                    }
                                                                });
                                                            }

                                                            var paper = {};
                                                            paper.authors = paperLead;
                                                            paper.id = pap.pubMedId;
                                                            paper.date = pap.date;
                                                            paper.title = pap.title;

                                                            newResults = [paper];

                                                            if (newResults && newResults[0]){
                                                                newResults.forEach(function(nr){
                                                                    finalResults.push(nr);
                                                                });
                                                                totalFound += newResults.length;
                                                            }
                                                            ids.splice(ids.indexOf(pap.pubMedId), 1);
                                                        });
                                                    }
                                                    if (ids && ids[0]){
                                                        var arrayArray = [];

                                                        var i,j,temparray,chunk = 500;
                                                        for (i=0,j=ids.length; i<j; i+=chunk) {
                                                            temparray = ids.slice(i,i+chunk);
                                                            arrayArray.push(temparray);
                                                        }

                                                        async.map(arrayArray, function (item, complete) {
                                                            request.post({
                                                                headers: {
                                                                    "content-type" : "application/json",
                                                                    "Accept": "*/*"
                                                                },
                                                                url: "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&retmode=text&rettype=xml&id=" + item.toString(),
                                                                body: JSON.stringify({id : item.toString()})
                                                            }, function (error, response, body) {
                                                                if (body){
                                                                    parseString(data, function (err, bodied) {
                                                                        if (bodied){
                                                                            var newResults = parseResults(bodied, ids);
                                                                            var itemNum = 1;
                                                                            if (newResults && newResults[0]){
                                                                                newResults.forEach(function(nr){
                                                                                    finalResults.push(nr);
                                                                                    if (nr.authors){
                                                                                        nr.authors = _.map(nr.authors, function (author) {
                                                                                            if (author.affiliation){
                                                                                                if (!author.email){
                                                                                                    var authorEmailMatch = author.affiliation.match(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/);
                                                                                                    if (authorEmailMatch && authorEmailMatch[0]){
                                                                                                        author.email = authorEmailMatch[0];
                                                                                                    }
                                                                                                }
                                                                                                if (!author.country){
                                                                                                    author = parseCountry(author);
                                                                                                }
                                                                                            }
                                                                                            return author;
                                                                                        });
                                                                                        nr.authors.forEach(function(author){
                                                                                            saveQueue.push({nr : nr, author : author, lead : true}, function(email, authored){

                                                                                            });
                                                                                        });
                                                                                    }
                                                                                    saveQueue.push({nr : nr}, function(email, authored){

                                                                                    });
                                                                                    itemNum += 2;
                                                                                });
                                                                                totalFound +=  newResults.length;
                                                                            }
                                                                            complete(null, item);
                                                                        }
                                                                        else {
                                                                            complete(null, item);
                                                                        }
                                                                    });
                                                                }
                                                                else {
                                                                    complete(null, item);
                                                                }
                                                            });
                                                        }, function (err, results) {

                                                        });
                                                    }
                                                    async.map(finalResults, function (item, complete) {
                                                        if (item.authors && item.authors[0]){
                                                            async.map(item.authors, function (author, complete3) {
                                                                if (author.affiliation){
                                                                    if (!author.email){
                                                                        var authorEmailMatch = author.affiliation.match(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/);
                                                                        if (authorEmailMatch && authorEmailMatch[0]){
                                                                            author.email = authorEmailMatch[0];
                                                                        }
                                                                    }
                                                                    if (!author.country){
                                                                        author = parseCountry(author);
                                                                    }
                                                                    if (author.email){
                                                                        complete3(null, author);
                                                                    }
                                                                    else if (author.emails && author.emails[0]){
                                                                        async.map(author.emails, function (email, complete2) {
                                                                            globalBlastEmailValidationQueue.push({email : email, item : item, user : req.session.passport.user, author : author}, function(email, authored){
                                                                                author.email = authored.email;
                                                                                complete2(null, email);
                                                                            });

                                                                        }, function (err, results) {
                                                                            complete3(null, author);
                                                                        });
                                                                    }
                                                                    else {
                                                                        complete3(null, author);
                                                                    }
                                                                }
                                                                else {
                                                                    complete3(null, author);
                                                                }
                                                            }, function (err, results) {
                                                                complete(null, item);
                                                            });
                                                        }
                                                        else {
                                                            complete(null, item);
                                                        }
                                                    }, function (err, results) {
                                                        if (results){
                                                            var currentAuthorCount = 0;
                                                            var totalAuthorCount = 0;

                                                            results.forEach(function(paper){
                                                                if (paper.authors && paper.authors[0]){
                                                                    paper.authors.forEach(function(author){
                                                                        if (author.email){
                                                                            totalAuthorCount += 1;
                                                                        }
                                                                    });
                                                                }
                                                            });

                                                            blast.totalLeads = totalAuthorCount;
                                                            blast.dateUpdated = new Date();

                                                            if (blast.pubMedIds && blast.pubMedIds[0]){
                                                                newItems.forEach(function(idOr){
                                                                    if ($.inArray(idOr.id, blast.pubMedIds) < 0){
                                                                        blast.pubMedIds.push(idOr.id);
                                                                    }
                                                                });
                                                            }
                                                            else {
                                                                blast.pubMedIds = finalIded;
                                                            }
                                                            blast.inactiveLeads = [];
                                                            if (blast && !isEmpty(blast)){

                                                                var newLeadz = [];

                                                                results.forEach(function(paper){
                                                                    paper.authors.forEach(function(author){
                                                                        var notInLeads = true;
                                                                        if (used.blastHistories && used.blastHistories[0]){
                                                                            used.blastHistories.forEach(function(hist){
                                                                                if (hist.activated && hist.leads && hist.leads[0]){
                                                                                    used.emails.forEach(function(led){
                                                                                        if (author.email == led){
                                                                                            notInLeads = false;
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                        if (author.email && notInLeads){
                                                                            var inInactive = false;
                                                                            if (blast.inactiveLeads && blast.inactiveLeads[0]){
                                                                                blast.inactiveLeads.forEach(function(emo){
                                                                                    if (author._id && emo.toString() == author._id.toString()){
                                                                                        inInactive = true;
                                                                                    }
                                                                                });
                                                                            }
                                                                            if (blast.leads && blast.leads[0]){
                                                                                used.emails.forEach(function(emo){
                                                                                    if (author.email && emo == author.email){
                                                                                        inInactive = true;
                                                                                    }
                                                                                });
                                                                            }
                                                                            if (newLeadz && newLeadz[0]){
                                                                                newLeadz.forEach(function(emo){
                                                                                    if (author.email && emo.author.email == author.email){
                                                                                        inInactive = true;
                                                                                    }
                                                                                });
                                                                            }
                                                                            if (!inInactive && author.email){
                                                                                if (author._id){
                                                                                    newLeadz.push({type : 'in', author : author, pubMedId : paper.id});
                                                                                }
                                                                                else{
                                                                                    newLeadz.push({type : 'out', author : {affiliation : author.affiliation, country : author.country, paperDate : paper.date, searchTerm : paper.searchTerm, title : paper.title, pubMedId : paper.id, email : author.email, phone : author.phone, firstname : author.firstname, lastname : author.lastname, initials : author.initials}});
                                                                                }
                                                                            }
                                                                        }
                                                                    });
                                                                });

                                                                if (newLeadz && newLeadz[0]){
                                                                    async.map(newLeadz, function (item, complete4) {
                                                                        if (item.type == 'in'){
                                                                            blast.inactiveLeads.push(item.author._id);
                                                                            complete4(null,item);
                                                                        }
                                                                        else {
                                                                            Lead.findOne({$and: [
                                                                                { pubMedId : item.pubMedId },
                                                                                { firstname : item.author.firstname },
                                                                                { lastname : item.author.lastname },
                                                                                { email : item.author.email }
                                                                            ]},function(err,leaded){
                                                                                if (!isEmpty(leaded)){
                                                                                    blast.inactiveLeads.push(leaded._id);
                                                                                    complete4(null,leaded);
                                                                                }
                                                                                else {
                                                                                    var newLer = new Lead(item.author);
                                                                                    newLer.save(function(err,bun){
                                                                                        if (!isEmpty(bun)){
                                                                                            blast.inactiveLeads.push(bun._id);
                                                                                            complete4(null,bun);
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    }, function(err,results){
                                                                        var newLeadCount = 0;
                                                                        var chargeLeads = [];
                                                                        var messageLeads = [];
                                                                        var newLeadsArray = [];

                                                                        var removeList = [];
                                                                        var emailRemoveList = [];

                                                                        results.forEach(function(lo){
                                                                            if (used.credits <= newLeadCount){
                                                                                newLeadCount += 1;
                                                                                blast.leads.push(lo._id);
                                                                                removeList.push(lo._id);
                                                                                emailRemoveList.push(lo.email);
                                                                                blast.newLeads.push(lo._id.toString());
                                                                                newLeadsArray.push(lo.toObject());
                                                                                used.emails.push(lo.email);
                                                                                //checkLinkedIn(lo);
                                                                                messageLeads.push(lo);
                                                                            }
                                                                            else {
                                                                                chargeLeads.push(lo);
                                                                            }
                                                                        });

                                                                        Lead.find({_id: { $in: blast.inactiveLeads}}).batchSize(100000).exec(function(err,inactiveLeads){
                                                                            if (!isEmpty(inactiveLeads)){

                                                                                if (chargeLeads && chargeLeads[0] && used.stripeCustomerId){

                                                                                    var amounted = (((Number(config.price) * chargeLeads.length) + Number(config.price)) * 100).toFixed();

                                                                                    stripe.charges.create({
                                                                                        amount: amounted,
                                                                                        currency: "usd",
                                                                                        customer: used.stripeCustomerId
                                                                                    }).then(function(charge) {
                                                                                            if (charge && charge.id && charge.paid){

                                                                                                var amount = (Number(amounted) / 100).toFixed(2);
                                                                                                var credits = Math.floor((Number(amounted) / 100 / Number(config.price)));
                                                                                                if (used.credits){
                                                                                                    used.credits = used.credits + credits;
                                                                                                }
                                                                                                else {
                                                                                                    used.credits = credits;
                                                                                                }

                                                                                                chargeLeads.forEach(function(led){
                                                                                                    blast.leads.push(led._id);
                                                                                                    removeList.push(led._id);
                                                                                                    emailRemoveList.push(led.email);
                                                                                                    used.emails.push(led.email);
                                                                                                    blast.newLeads.push(led._id.toString());
                                                                                                    //checkLinkedIn(led);
                                                                                                    messageLeads.push(led);
                                                                                                });

                                                                                                var finalInactive = [];
                                                                                                blast = blast.blastHistories.id(blast._id);
                                                                                                if (removeList && removeList[0] && blast.inactiveLeads && blast.inactiveLeads[0]){
                                                                                                    inactiveLeads.forEach(function(led2){
                                                                                                        if ($.inArray(led2.email, used.emails) < 0){
                                                                                                            finalInactive.push(led2._id.toString());
                                                                                                        }
                                                                                                    });
                                                                                                    blast.inactiveLeads = finalInactive;
                                                                                                }

                                                                                                used.creditHistories.push(new CreditHistory({date : new Date(), price : amount, amount : credits, payMethod : 'Stripe', paymentId : charge.id, type : 'refresh', typeId : blast._id.toString()}));

                                                                                                used.credits = used.credits - newLeadCount - chargeLeads.length;

                                                                                                async.map(newLeadsArray, function (item, complete) {
                                                                                                    var term;
                                                                                                    var totalMatchTerm;
                                                                                                    Paper.findOne({pubMedId : item.pubMedId}, function(err,paped){
                                                                                                        if (!isEmpty(paped)){
                                                                                                            async.map(blast.searchTerms, function (itemed, complete2) {
                                                                                                                if (paped.type == 'remote' && paped.json){
                                                                                                                    var checkString = '';
                                                                                                                    if (item.firstname){
                                                                                                                        checkString += item.firstname;
                                                                                                                    }
                                                                                                                    if (item.lastname){
                                                                                                                        checkString += ' ' + item.lastname;
                                                                                                                    }
                                                                                                                    if (item.institution){
                                                                                                                        checkString += ' ' + item.institution;
                                                                                                                    }
                                                                                                                    if (item.department){
                                                                                                                        checkString += ' ' + item.department;
                                                                                                                    }
                                                                                                                    if (item.affiliation){
                                                                                                                        checkString += ' ' + item.affiliation;
                                                                                                                    }
                                                                                                                    var re = new RegExp(itemed,"ig");
                                                                                                                    var matches = checkString.match(re);
                                                                                                                    if (matches && matches[0]){

                                                                                                                    }
                                                                                                                    else {
                                                                                                                        var ree = new RegExp(itemed,"ig");
                                                                                                                        var matched = paped.json.match(ree);
                                                                                                                        if (matched && matched[0]){
                                                                                                                            totalMatchTerm = itemed;
                                                                                                                        }
                                                                                                                        else {
                                                                                                                            term = itemed;
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                                else if (paped.type == 'local' && paped.filename){
                                                                                                                    var data = fs.readFileSync(paped.filename);
                                                                                                                    data = data.toString();
                                                                                                                    data = data.substring(data.indexOf('\n')+1);
                                                                                                                    var matched = data.match(/\<\!DOCTYPE/i);
                                                                                                                    if (matched && matched[0]){
                                                                                                                        data = data.substring(data.indexOf('\n')+1);
                                                                                                                    }
                                                                                                                    var checkString = '';
                                                                                                                    if (item.firstname){
                                                                                                                        checkString += item.firstname;
                                                                                                                    }
                                                                                                                    if (item.lastname){
                                                                                                                        checkString += ' ' + item.lastname;
                                                                                                                    }
                                                                                                                    if (item.institution){
                                                                                                                        checkString += ' ' + item.institution;
                                                                                                                    }
                                                                                                                    if (item.department){
                                                                                                                        checkString += ' ' + item.department;
                                                                                                                    }
                                                                                                                    if (item.affiliation){
                                                                                                                        checkString += ' ' + item.affiliation;
                                                                                                                    }
                                                                                                                    var re = new RegExp(itemed,"ig");
                                                                                                                    var matches = checkString.match(re);
                                                                                                                    if (matches && matches[0]){

                                                                                                                    }
                                                                                                                    else {
                                                                                                                        var ree = new RegExp(itemed,"ig");
                                                                                                                        var matched = data.match(ree);
                                                                                                                        if (matched && matched[0]){
                                                                                                                            totalMatchTerm = itemed;
                                                                                                                        }
                                                                                                                        else {
                                                                                                                            term = itemed;
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                                complete2(null, itemed);
                                                                                                            }, function(err,results){
                                                                                                                item.term = term;
                                                                                                                item.totalMatchTerm = totalMatchTerm;
                                                                                                                complete(null, item);
                                                                                                            });
                                                                                                        }
                                                                                                        else {
                                                                                                            complete(null, item);
                                                                                                        }
                                                                                                    });
                                                                                                }, function(err,results){
                                                                                                    results.forEach(function(red){
                                                                                                        if (red.term){
                                                                                                            if (used.leadTermArray && used.leadTermArray[0]){

                                                                                                            }
                                                                                                            else {
                                                                                                                used.leadTermArray = [];
                                                                                                            }
                                                                                                            used.leadTermArray.push({_id : red._id.toString(), term : red.term});
                                                                                                        }
                                                                                                        else if (red.totalMatchTerm){
                                                                                                            if (used.leadTermArray && used.leadTermArray[0]){

                                                                                                            }
                                                                                                            else {
                                                                                                                used.leadTermArray = [];
                                                                                                            }
                                                                                                            used.leadTermArray.push({_id : red._id.toString(), term : red.totalMatchTerm});
                                                                                                        }
                                                                                                    });
                                                                                                    used.markModified('leadTermArray');
                                                                                                    used.save(function(err,usedFinal){
                                                                                                        if (!isEmpty(usedFinal)){
                                                                                                            worker.send({ type : 'newLeads', id : blast._id, userId : usedFinal._id, newLeadCount : newLeadCount, chargeLeads : chargeLeads.length, amount : '$' + amount, newLeads : messageLeads });
                                                                                                            sendRefreshEmail(usedFinal.email, usedFinal.firstname, usedFinal.lastname, usedFinal.title, usedFinal.organization, used.credits, newLeadCount, chargeLeads.length, '$' + amount);
                                                                                                        }
                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        });
                                                                                }
                                                                                else {
                                                                                    var finalInactive = [];
                                                                                    blast = blast.blastHistories.id(blast._id);
                                                                                    if (removeList && removeList[0] && blast.inactiveLeads && blast.inactiveLeads[0]){
                                                                                        inactiveLeads.forEach(function(led2){
                                                                                            if ($.inArray(led2.email, used.emails) < 0){
                                                                                                finalInactive.push(led2._id.toString());
                                                                                            }
                                                                                        });
                                                                                        blast.inactiveLeads = finalInactive;
                                                                                    }

                                                                                    used.credits = used.credits - newLeadCount;

                                                                                    async.map(newLeadsArray, function (item, complete) {
                                                                                        var term;
                                                                                        var totalMatchTerm;
                                                                                        Paper.findOne({pubMedId : item.pubMedId}, function(err,paped){
                                                                                            if (!isEmpty(paped)){
                                                                                                async.map(blast.searchTerms, function (itemed, complete2) {
                                                                                                    if (paped.type == 'remote' && paped.json){
                                                                                                        var checkString = '';
                                                                                                        if (item.firstname){
                                                                                                            checkString += item.firstname;
                                                                                                        }
                                                                                                        if (item.lastname){
                                                                                                            checkString += ' ' + item.lastname;
                                                                                                        }
                                                                                                        if (item.institution){
                                                                                                            checkString += ' ' + item.institution;
                                                                                                        }
                                                                                                        if (item.department){
                                                                                                            checkString += ' ' + item.department;
                                                                                                        }
                                                                                                        if (item.affiliation){
                                                                                                            checkString += ' ' + item.affiliation;
                                                                                                        }
                                                                                                        var re = new RegExp(itemed,"ig");
                                                                                                        var matches = checkString.match(re);
                                                                                                        if (matches && matches[0]){

                                                                                                        }
                                                                                                        else {
                                                                                                            var ree = new RegExp(itemed,"ig");
                                                                                                            var matched = paped.json.match(ree);
                                                                                                            if (matched && matched[0]){
                                                                                                                totalMatchTerm = itemed;
                                                                                                            }
                                                                                                            else {
                                                                                                                term = itemed;
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                    else if (paped.type == 'local' && paped.filename){
                                                                                                        var data = fs.readFileSync(paped.filename);
                                                                                                        data = data.toString();
                                                                                                        data = data.substring(data.indexOf('\n')+1);
                                                                                                        var matched = data.match(/\<\!DOCTYPE/i);
                                                                                                        if (matched && matched[0]){
                                                                                                            data = data.substring(data.indexOf('\n')+1);
                                                                                                        }
                                                                                                        var checkString = '';
                                                                                                        if (item.firstname){
                                                                                                            checkString += item.firstname;
                                                                                                        }
                                                                                                        if (item.lastname){
                                                                                                            checkString += ' ' + item.lastname;
                                                                                                        }
                                                                                                        if (item.institution){
                                                                                                            checkString += ' ' + item.institution;
                                                                                                        }
                                                                                                        if (item.department){
                                                                                                            checkString += ' ' + item.department;
                                                                                                        }
                                                                                                        if (item.affiliation){
                                                                                                            checkString += ' ' + item.affiliation;
                                                                                                        }
                                                                                                        var re = new RegExp(itemed,"ig");
                                                                                                        var matches = checkString.match(re);
                                                                                                        if (matches && matches[0]){

                                                                                                        }
                                                                                                        else {
                                                                                                            var ree = new RegExp(itemed,"ig");
                                                                                                            var matched = data.match(ree);
                                                                                                            if (matched && matched[0]){
                                                                                                                totalMatchTerm = itemed;
                                                                                                            }
                                                                                                            else {
                                                                                                                term = itemed;
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                    complete2(null, itemed);
                                                                                                }, function(err,results){
                                                                                                    item.term = term;
                                                                                                    item.totalMatchTerm = totalMatchTerm;
                                                                                                    complete(null, item);
                                                                                                });
                                                                                            }
                                                                                            else {
                                                                                                complete(null, item);
                                                                                            }
                                                                                        });
                                                                                    }, function(err,results){
                                                                                        results.forEach(function(red){
                                                                                            if (red.term){
                                                                                                if (used.leadTermArray && used.leadTermArray[0]){

                                                                                                }
                                                                                                else {
                                                                                                    used.leadTermArray = [];
                                                                                                }
                                                                                                used.leadTermArray.push({_id : red._id.toString(), term : red.term});
                                                                                            }
                                                                                            else if (red.totalMatchTerm){
                                                                                                if (used.leadTermArray && used.leadTermArray[0]){

                                                                                                }
                                                                                                else {
                                                                                                    used.leadTermArray = [];
                                                                                                }
                                                                                                used.leadTermArray.push({_id : red._id.toString(), term : red.totalMatchTerm});
                                                                                            }
                                                                                        });
                                                                                        used.markModified('leadTermArray');
                                                                                        used.save(function(err,usedFinal){
                                                                                            if (!isEmpty(usedFinal)){
                                                                                                worker.send({ type : 'newLeads', id : blast._id, userId : usedFinal._id, newLeadCount : newLeadCount, newLeads : messageLeads });
                                                                                                sendRefreshEmail(usedFinal.email, usedFinal.firstname, usedFinal.lastname, usedFinal.title, usedFinal.organization, used.credits, newLeadCount, chargeLeads.length, '');
                                                                                            }
                                                                                        });
                                                                                    });
                                                                                }
                                                                            }
                                                                        });
                                                                    });
                                                                }
                                                                else {
                                                                    blast.dateUpdated = new Date();
                                                                    used.save(function(err,usedFinal){
                                                                        if (!isEmpty(usedFinal)){

                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        }
                                                    });
                                                });
                                            });
                                        }
                                    }
                                }
                            });
                        }

                    });
                }
            }
        });
    }

    function reInitiateCrons(){
        User.find({$and: [
            { 'emailBlasts.activated' : true },
            { $or: [{$and: [{dateFollowUpSend: {'$ne': null }}, {followUpDate: null }]}, {sentDate: null }] }
        ]}).select('_id emailBlasts').where('emailBlasts.activated').equals(true).exec(function(err, users){
                if (!isEmpty(users)){
                    users.forEach(function(user){
                        if (users.emailBlasts && users.emailBlasts[0]){
                            users.emailBlasts.forEach(function(blasted){
                                if (blasted.activated){
                                    if (blasted.followUpMessage && typeof blasted.followUpMessage == 'string' && !blasted.followUpDate && blasted.dateFollowUpSend){
                                        var jobOb2 = {};

                                        var job = new cronJob(blasted.dateFollowUpSend, function(){
                                                User.findById(user._id).select('_id emailBlasts').where('emailBlasts.activated').equals(true).exec(function(err,used){
                                                    if (!isEmpty(used)){
                                                        var blast = used.emailBlasts.id(blasted._id);
                                                        Lead.find({_id: { $in: blast.leads}}).batchSize(100000).exec(function(err,leads){
                                                            if (!isEmpty(leads)){
                                                                leads.forEach(function(author){
                                                                    var resFollow = blast.followUpMessage.toString();
                                                                    var term;
                                                                    if (used.leadTermArray && used.leadTermArray[0]){
                                                                        used.leadTermArray.forEach(function(led){
                                                                            if (led._id.toString() == author._id.toString()){
                                                                                term = led.term;
                                                                            }
                                                                        });
                                                                    }
                                                                    if (term){
                                                                        resFollow = resFollow.replace('<span class="SEARCHTERM" contenteditable="false">SEARCHTERM</span>', term);
                                                                    }
                                                                    resFollow = resFollow.replace('<span class="PAPER" contenteditable="false">PAPER</span>', author.title);
                                                                    resFollow = resFollow.replace('<span class="NAME" contenteditable="false">NAME</span>', 'Dr. ' + author.lastname);
                                                                    resFollow = resFollow.replace('<span class="INSTITUTION" contenteditable="false">INSTITUTION</span>', author.institution);
                                                                    resFollow = resFollow.replace('<span class="DATEPAPER" contenteditable="false">DATEPAPER</span>', moment(author.paperDate).format("dddd, MMMM Do YYYY"));
                                                                    resFollow = resFollow.replace('<span class="DATE" contenteditable="false">DATE</span>', moment(blast.date).format("dddd, MMMM Do YYYY"));
                                                                    var finalMessageHTMLFollow;
                                                                    var finalMessagePlaintextFollow;

                                                                    var resSub = blast.subject.toString();
                                                                    if (term){
                                                                        resSub = resSub.replace('<span class="SEARCHTERM" contenteditable="false">SEARCHTERM</span>', term);
                                                                    }
                                                                    resSub = resSub.replace('<span class="PAPER" contenteditable="false">PAPER</span>', author.title);
                                                                    resSub = resSub.replace('<span class="NAME" contenteditable="false">NAME</span>', 'Dr. ' + author.lastname);
                                                                    resSub = resSub.replace('<span class="INSTITUTION" contenteditable="false">INSTITUTION</span>', author.institution);
                                                                    resSub = resSub.replace('<span class="DATEPAPER" contenteditable="false">DATEPAPER</span>', moment(author.paperDate).format("dddd, MMMM Do YYYY"));
                                                                    resSub = resSub.replace('<span class="DATE" contenteditable="false">DATE</span>', moment(blast.date).format("dddd, MMMM Do YYYY"));

                                                                    if (typeof blast.signature == 'string'){
                                                                        finalMessageHTMLFollow = $(resFollow + "<br><br>" + blast.signature).text();
                                                                        finalMessagePlaintextFollow = $(resFollow + ' ' + blast.signature).text();
                                                                    }
                                                                    else {
                                                                        finalMessageHTMLFollow = $(resFollow).text();
                                                                        finalMessagePlaintextFollow = $(resFollow).text();
                                                                    }

                                                                    finalMessageHTMLFollow += '<br><br><br><a target="_blank" href=' + config.domain + '/doNotTalk?userId=' + used._id.toString() + '&emailId=' + blast._id.toString() + '&leadId=' + author._id.toString() + ' style="color: #cccccc;">Do not hear from me again.</a>';

                                                                    sendEmail(author.email, finalMessagePlaintextFollow, finalMessageHTMLFollow, resSub, blast.emailFrom, blast.nameFrom, '');
                                                                });

                                                                blast.followUpDate = new Date();
                                                                used.save(function(err,finalUsed){

                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }, function () {
                                            },
                                            true,
                                            'UTC'
                                        );
                                        jobOb2.job = job;
                                        jobOb2.type = 'followUp';
                                        jobOb2.id = blasted._id;
                                        jobArray.push(jobOb2);
                                    }
                                    if (blasted.templateMessage && blasted.dateSend && !blasted.sentDate){
                                        var jobOb = {};
                                        var jobMain = new cronJob(blasted.dateSend, function(){
                                                User.findById(user._id).select('_id emailBlasts').where('emailBlasts.activated').equals(true).exec(function(err,used){

                                                    if (!isEmpty(used)){
                                                        var blast = used.emailBlasts.id(blasted._id);
                                                        Lead.find({_id: { $in: blast.leads}}).batchSize(100000).exec(function(err,leads){
                                                            if (!isEmpty(leads)){
                                                                leads.forEach(function(author){
                                                                    var resed = blast.templateMessage.toString();
                                                                    var term;
                                                                    if (used.leadTermArray && used.leadTermArray[0]){
                                                                        used.leadTermArray.forEach(function(led){
                                                                            if (led._id.toString() == author._id.toString()){
                                                                                term = led.term;
                                                                            }
                                                                        });
                                                                    }
                                                                    if (term){
                                                                        resed = resed.replace('<span class="SEARCHTERM" contenteditable="false">SEARCHTERM</span>', term);
                                                                    }
                                                                    resed = resed.replace('<span class="PAPER" contenteditable="false">PAPER</span>', author.title);
                                                                    resed = resed.replace('<span class="NAME" contenteditable="false">NAME</span>', 'Dr. ' + author.lastname);
                                                                    resed = resed.replace('<span class="INSTITUTION" contenteditable="false">INSTITUTION</span>', author.institution);
                                                                    resed = resed.replace('<span class="DATEPAPER" contenteditable="false">DATEPAPER</span>', moment(author.paperDate).format("dddd, MMMM Do YYYY"));
                                                                    resed = resed.replace('<span class="DATE" contenteditable="false">DATE</span>', moment(blast.date).format("dddd, MMMM Do YYYY"));

                                                                    var resSub = blast.subject.toString();
                                                                    if (term){
                                                                        resSub = resSub.replace('<span class="SEARCHTERM" contenteditable="false">SEARCHTERM</span>', term);
                                                                    }
                                                                    resSub = resSub.replace('<span class="PAPER" contenteditable="false">PAPER</span>', author.title);
                                                                    resSub = resSub.replace('<span class="NAME" contenteditable="false">NAME</span>', 'Dr. ' + author.lastname);
                                                                    resSub = resSub.replace('<span class="INSTITUTION" contenteditable="false">INSTITUTION</span>', author.institution);
                                                                    resSub = resSub.replace('<span class="DATEPAPER" contenteditable="false">DATEPAPER</span>', moment(author.paperDate).format("dddd, MMMM Do YYYY"));
                                                                    resSub = resSub.replace('<span class="DATE" contenteditable="false">DATE</span>', moment(blast.date).format("dddd, MMMM Do YYYY"));
                                                                    var finalMessageHTML;
                                                                    var finalMessagePlaintext;
                                                                    if (typeof blast.signature == 'string'){
                                                                        finalMessageHTML = resed + "<br><br>" + blast.signature;
                                                                        finalMessagePlaintext = $(resed + ' ' + blast.signature).text();
                                                                    }
                                                                    else {
                                                                        finalMessageHTML = resed;
                                                                        finalMessagePlaintext = $(resed).text();
                                                                    }

                                                                    finalMessageHTML += '<br><br><br><a target="_blank" href=' + config.domain + '/doNotTalk?userId=' + used._id.toString() + '&emailId=' + blast._id.toString() + '&leadId=' + author._id.toString() + ' style="color: #cccccc;">Do not hear from me again.</a>';

                                                                    sendEmail(author.email, finalMessagePlaintext, finalMessageHTML, resSub, blast.emailFrom, blast.nameFrom, blast.attachments);
                                                                });

                                                                blast.sentDate = new Date();

                                                                used.save(function(err,finalUsed){

                                                                });
                                                            }
                                                        });
                                                    }

                                                });
                                            }, function () {
                                            },
                                            true,
                                            'UTC'
                                        );
                                        jobOb.job = jobMain;
                                        jobOb.type = 'emailMain';
                                        jobOb.id = blasted._id;
                                        jobArray.push(jobOb);
                                    }
                                }
                            });
                        }
                    });
                }
            });
        User.find({'blastHistories.autoRefresh' : true}).select('_id blastHistories').where('blastHistories.autoRefresh').equals(true).exec(function(err, users){
            if (!isEmpty(users)){
                users.forEach(function(user){
                    if (users.blastHistories && users.blastHistories[0]){
                        users.blastHistories.forEach(function(history){
                            if (history.autoRefresh){
                                var jobOb = {};
                                var jobMain = new cronJob('00 30 11 * * 1-5', function(){
                                        runRefresh(user._id, history._id);
                                    }, function () {
                                    },
                                    true,
                                    'UTC'
                                );
                                runRefresh(user._id, history._id);
                                jobOb.job = jobMain;
                                jobOb.type = 'refresh';
                                jobOb.id = history._id;
                                jobArray.push(jobOb);
                            }
                        });
                    }
                });
            }
        });
    }

    reInitiateCrons();


    function parseEmail(affied){
        affied = affied.replace(/ Email\: (?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])\./, '');
        affied = affied.replace(/ Electronic address\: (?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])\./, '');
        affied = affied.replace(/ (?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])./, '');
        affied = affied.replace(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/, '');
        return affied;
    }

    function parseAffiliation(affied){
        if (affied){
            affied = affied.replace(/ Email\: (?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])\./, '');
            affied = affied.replace(/ Electronic address\: (?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])\./, '');
            affied = affied.replace(/ (?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])./, '');
            affied = affied.replace(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/, '');
            affied = affied.split(';')[0];
            var matcherComma = affied.match(/,/g);
            if (matcherComma && matcherComma.length >= 4){
                var affie = affied.split(',');
                affied = affie[0] + ',' + affie[1] + ',' + affie[2];
            }
            else if (matcherComma && (matcherComma.length == 2 || matcherComma.length == 3)){
                var affie = affied.split(',');
                affied = affie[0] + ',' + affie[1];
            }
            else {
                var affie = affied.split(',');
                affied = affie[0];
            }
        }
        return affied;
    }

    function parseResults(jsonText2, finalIdArray){
        var paperArray = [];

        if (jsonText2.PubmedArticleSet && jsonText2.PubmedArticleSet.PubmedArticle){
            jsonText2.PubmedArticleSet.PubmedArticle.forEach(function(article){
                var paperObject = {};
                paperObject.json = article;
                if (article.PubmedData && article.PubmedData[0].ArticleIdList && article.PubmedData[0].ArticleIdList[0] && article.PubmedData[0].ArticleIdList[0].ArticleId){
                    var pubmedId;

                    article.PubmedData[0].ArticleIdList[0].ArticleId.forEach(function(pub){
                        if (pub['$'].IdType == 'pubmed'){
                            pubmedId = pub['_'];
                        }
                    });
                    paperObject.id = pubmedId;
                    finalIdArray.forEach(function(idded){
                        if (idded.id == paperObject.id){
                            paperObject.searchTerm = idded.term;
                        }
                    });

                    if (article.MedlineCitation[0].Article){

                        if (article.MedlineCitation[0].Article[0].ArticleDate){
                            paperObject.date = new Date(article.MedlineCitation[0].Article[0].ArticleDate[0].Year[0], article.MedlineCitation[0].Article[0].ArticleDate[0].Month[0], article.MedlineCitation[0].Article[0].ArticleDate[0].Day[0]);
                        }
                        paperObject.title = article.MedlineCitation[0].Article[0].ArticleTitle[0];

                        var authorList = [];
                        if (article.MedlineCitation[0].Article && article.MedlineCitation[0].Article[0].AuthorList){

                            if (article.MedlineCitation[0].Article[0].AuthorList[0].Author[0]){
                                article.MedlineCitation[0].Article[0].AuthorList[0].Author.forEach(function(auth){
                                    var authorObject = {};
                                    if (auth.ForeName){
                                        authorObject.firstname = auth.ForeName[0];
                                    }
                                    if (auth.Initials){
                                        authorObject.initials = auth.Initials[0];
                                    }
                                    if (auth.LastName){
                                        authorObject.lastname = auth.LastName[0];
                                    }
                                    if (auth.Affiliation){
                                        authorObject.affiliation = auth.Affiliation[0];
                                        authorList.push(authorObject);
                                    }

                                });
                                if (authorList && authorList[0]){
                                    paperObject.authors = authorList;
                                    paperArray.push(paperObject);
                                }
                            }
                        }
                    }
                }
            });
        }
        return paperArray;
    }

} else {

    // TODO : write function or regexs on data return/display for affiliation section institution/department parsing if no institution and phone number for efetch

    // TODO : prompto /sendBlast route option not connected to list or search

    // TODO : cron to clear out non activated email blasts and non activated searches after session expiry time

    // TODO : separate stringified JSON json property of papers into neat, indexable fields. index with SOLR

    // TODO : websocket and master - process communication to keep temporal emails and lead generations real time

    var express = require('express')
        , http = require('http')
        , config = require('./config')
        , connect = require('connect')
        , request = require('request')
        , hbs = require('hbs')
        , fs = require('fs')
        , $ = require('jQuery')
        , _ = require('underscore')
        , stripe = require('stripe')(config.stripeSecret)
        , async = require('async')
        , crypto = require('crypto-js')
        , cryptoSha = require('crypto')
        , querystring = require('querystring')
        , bcrypt = require('bcrypt')
        , redis = require('redis').createClient()
        , mongoose = require('mongoose')
        , xmlhttp = require('xmlhttprequest')
        , solr = require('solr')
        , solrSearch = solr.createClient()
        , linkedinClient = require('linkedin-js')(config.linkedinKey, config.linkedinSecret, config.domain)
        , s3 = require('aws2js').load('s3', config.s3Key, config.s3Secret)
        , jsdom = require('jsdom')
        , net = require('net')
        , OAuth= require('oauth').OAuth
        , passport = require('passport')
        , LocalStrategy = require('passport-local').Strategy
        , TwitterStrategy = require('passport-twitter').Strategy
        , LinkedInStrategy = require('passport-linkedin').Strategy
        , nodemailer = require('nodemailer')
        , RedisStore = require('connect-redis')(express)
        , moment = require('moment')
        , path = require('path')
        , parseString = require('xml2js').parseString
        , cronJob = require('cron').CronJob
        , twitter = require('twitter')
        , exec = require('child_process').exec;
    var countryList = ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua & Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia & Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Cape Verde","Cayman Islands","Chad","Chile","China","Colombia","Canada","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cruise Ship","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kuwait","Kyrgyz Republic","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Mauritania","Mauritius","Mexico","MÃ©xico","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Namibia","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Norway","Oman","Pakistan","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre & Miquelon","Samoa","San Marino","Satellite","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","South Africa","South Korea","Spain","Sri Lanka","St Kitts & Nevis","St Lucia","St Vincent","St. Lucia","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad &amp; Tobago","Tunisia","Turkey","Turkmenistan","Turks & Caicos","Uganda","Ukraine","United Arab Emirates","United Kingdom","Uruguay","Uzbekistan","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe", "Korea", "UK", "USA", "U.K.", "U.S.A.", "United States of America", "United States", "America", "Chinese"];
    var stateList = [
        {data: "AK", label: "Alaska"},
        {data: "AL", label: "Alabama"},
        {data: "AR", label: "Arkansas"},
        {data: "AZ", label: "Arizona"},
        {data: "CA", label: "California"},
        {data: "CO", label: "Colorado"},
        {data: "CT", label: "Connecticut"},
        {data: "DE", label: "Delaware"},
        {data: "DC", label: "District of Columbia"},
        {data: "DC", label: "Washington D.C."},
        {data: "FL", label: "Florida"},
        {data: "GA", label: "Georgia"},
        {data: "HI", label: "Hawaii"},
        {data: "IA", label: "Iowa"},
        {data: "ID", label: "Idaho"},
        {data: "IL", label: "Illinois"},
        {data: "IN", label: "Indiana"},
        {data: "KS", label: "Kansas"},
        {data: "KY", label: "Kentucky"},
        {data: "LA", label: "Louisiana"},
        {data: "MA", label: "Massachusetts"},
        {data: "MD", label: "Maryland"},
        {data: "ME", label: "Maine"},
        {data: "MI", label: "Michigan"},
        {data: "MN", label: "Minnesota"},
        {data: "MS", label: "Mississippi"},
        {data: "MO", label: "Missouri"},
        {data: "MT", label: "Montana"},
        {data: "NC", label: "North Carolina"},
        {data: "ND", label: "North Dakota"},
        {data: "NE", label: "Nebraska"},
        {data: "NH", label: "New Hampshire"},
        {data: "NJ", label: "New Jersey"},
        {data: "NM", label: "New Mexico"},
        {data: "NV", label: "Nevada"},
        {data: "NY", label: "New York"},
        {data: "OH", label: "Ohio"},
        {data: "OK", label: "Oklahoma"},
        {data: "OR", label: "Oregon"},
        {data: "PA", label: "Pennsylvania"},
        {data: "RI", label: "Rhode Island"},
        {data: "SC", label: "South Carolina"},
        {data: "SD", label: "South Dakota"},
        {data: "TN", label: "Tennessee"},
        {data: "TX", label: "Texas"},
        {data: "UT", label: "Utah"},
        {data: "VA", label: "Virginia"},
        {data: "VT", label: "Vermont"},
        {data: "WA", label: "Washington"},
        {data: "WI", label: "Wisconsin"},
        {data: "WV", label: "West Virginia"},
        {data: "WY", label: "Wyoming"}];
    var domainList = [
        {domain : '.ad', country : 'Andorra'},
        {domain : '.ae', country : 'United Arab Emirates'},
        {domain : '.af', country : 'Afghanistan'},
        {domain : '.ag', country : 'Antigua and Barbuda'},
        {domain : '.ai', country : 'Anguilla'},
        {domain : '.al', country : 'Albania'},
        {domain : '.am', country : 'Armenia'},
        {domain : '.an', country : 'Netherlands Antilles'},
        {domain : '.ao', country : 'Angola'},
        {domain : '.aq', country : 'Antarctica'},
        {domain : '.ar', country : 'Argentina'},
        {domain : '.as', country : 'American Samoa'},
        {domain : '.at', country : 'Austria'},
        {domain : '.au', country : 'Australia'},
        {domain : '.aw', country : 'Aruba'},
        {domain : '.az', country : 'Azerbaijan'},
        {domain : '.ba', country : 'Bosnia/Herzegovinia'},
        {domain : '.bb', country : 'Barbados'},
        {domain : '.bd', country : 'Bangladesh'},
        {domain : '.be', country : 'Belgium'},
        {domain : '.bf', country : 'Burkina Faso'},
        {domain : '.bg', country : 'Bulgaria'},
        {domain : '.bh', country : 'Bahrain'},
        {domain : '.bi', country : 'Burundi'},
        {domain : '.bj', country : 'Benin'},
        {domain : '.bm', country : 'Bermuda'},
        {domain : '.bn', country : 'Brunei Darussalam'},
        {domain : '.bo', country : 'Bolivia'},
        {domain : '.br', country : 'Brazil'},
        {domain : '.bs', country : 'Bahamas'},
        {domain : '.bt', country : 'Bhutan'},
        {domain : '.bv', country : 'Bouvet Island'},
        {domain : '.bw', country : 'Botswana'},
        {domain : '.by', country : 'Belarus'},
        {domain : '.bz', country : 'Belize'},
        {domain : '.ca', country : 'Canada'},
        {domain : '.cc', country : 'Cocos Islands - Keelings'},
        {domain : '.cf', country : 'Central African Republic'},
        {domain : '.cg', country : 'Congo'},
        {domain : '.ch', country : 'Switzerland'},
        {domain : '.ci', country : 'Cote D&#226;â‚¬â„¢Ivoire, or Ivory Coast'},
        {domain : '.ck', country : 'Cook Islands'},
        {domain : '.cl', country : 'Chile'},
        {domain : '.cm', country : 'Cameroon'},
        {domain : '.cn', country : 'China'},
        {domain : '.co', country : 'Colombia'},
        {domain : '.cr', country : 'Costa Rica'},
        {domain : '.cs', country : 'Czechoslovakia (former)'},
        {domain : '.cu', country : 'Cuba'},
        {domain : '.cv', country : 'Cape Verde'},
        {domain : '.cx', country : 'Christmas Island'},
        {domain : '.cy', country : 'Cyprus'},
        {domain : '.cz', country : 'Czech Republic'},
        {domain : '.de', country : 'Germany'},
        {domain : '.dj', country : 'Djibouti'},
        {domain : '.dk', country : 'Denmark'},
        {domain : '.dm', country : 'Dominica'},
        {domain : '.do', country : 'Dominican Republic'},
        {domain : '.dz', country : 'Algeria'},
        {domain : '.ec', country : 'Ecuador'},
        {domain : '.ee', country : 'Estonia'},
        {domain : '.eg', country : 'Egypt'},
        {domain : '.eh', country : 'Western Sahara '},
        {domain : '.er', country : 'Eritrea'},
        {domain : '.es', country : 'Spain'},
        {domain : '.et', country : 'Ethiopia'},
        {domain : '.eu', country : 'European Union'},
        {domain : '.fi', country : 'Finland'},
        {domain : '.fj', country : 'Fiji'},
        {domain : '.fk', country : 'Falkland Islands/Malvinas'},
        {domain : '.fm', country : 'Micronesia'},
        {domain : '.fo', country : 'Faroe Islands'},
        {domain : '.fr', country : 'France'},
        {domain : '.fx', country : 'Metropolitan France'},
        {domain : '.ga', country : 'Gabon'},
        {domain : '.gb', country : 'Great Britain'},
        {domain : '.gd', country : 'Grenada'},
        {domain : '.ge', country : 'Georgia'},
        {domain : '.gf', country : 'French Guiana'},
        {domain : '.gh', country : 'Ghana'},
        {domain : '.gi', country : 'Gibraltar'},
        {domain : '.gl', country : 'Greenland'},
        {domain : '.gm', country : 'Gambia'},
        {domain : '.gn', country : 'Guinea'},
        {domain : '.gp', country : 'Guadeloupe'},
        {domain : '.gq', country : 'Equatorial Guinea'},
        {domain : '.gr', country : 'Greece'},
        {domain : '.gs', country : 'South Georgia and South Sandwich Islands'},
        {domain : '.gt', country : 'Guatemala'},
        {domain : '.gu', country : 'Guam'},
        {domain : '.gw', country : 'Guinea-Bissau'},
        {domain : '.gy', country : 'Guyana'},
        {domain : '.hk', country : 'Hong Kong'},
        {domain : '.hm', country : 'Heard and McDonald Islands'},
        {domain : '.hn', country : 'Honduras'},
        {domain : '.hr', country : 'Croatia/Hrvatska'},
        {domain : '.ht', country : 'Haiti'},
        {domain : '.hu', country : 'Hungary'},
        {domain : '.id', country : 'Indonesia'},
        {domain : '.ie', country : 'Ireland'},
        {domain : '.il', country : 'Israel'},
        {domain : '.in', country : 'India'},
        {domain : '.io', country : 'British Indian Ocean Territory'},
        {domain : '.iq', country : 'Iraq'},
        {domain : '.ir', country : 'Iran'},
        {domain : '.is', country : 'Iceland'},
        {domain : '.it', country : 'Italy'},
        {domain : '.jm', country : 'Jamaica'},
        {domain : '.jo', country : 'Jordan'},
        {domain : '.jp', country : 'Japan'},
        {domain : '.ke', country : 'Kenya'},
        {domain : '.kg', country : 'Kyrgyzstan'},
        {domain : '.kh', country : 'Cambodia'},
        {domain : '.ki', country : 'Kiribati'},
        {domain : '.km', country : 'Comoros'},
        {domain : '.kn', country : 'Saint Kitts and Nevis'},
        {domain : '.kp', country : 'North Korea'},
        {domain : '.kr', country : 'South Korea'},
        {domain : '.kw', country : 'Kuwait'},
        {domain : '.ky', country : 'Cayman Islands'},
        {domain : '.kz', country : 'Kazakhstan'},
        {domain : '.la', country : 'Laos'},
        {domain : '.lb', country : 'Lebanon'},
        {domain : '.lc', country : 'Saint Lucia'},
        {domain : '.li', country : 'Liechtenstein'},
        {domain : '.lk', country : 'Sri Lanka'},
        {domain : '.lr', country : 'Liberia'},
        {domain : '.ls', country : 'Lesotho'},
        {domain : '.lt', country : 'Lithuania'},
        {domain : '.lu', country : 'Luxembourg'},
        {domain : '.lv', country : 'Latvia'},
        {domain : '.ly', country : 'Libya'},
        {domain : '.ma', country : 'Morocco'},
        {domain : '.mc', country : 'Monaco'},
        {domain : '.md', country : 'Moldova'},
        {domain : '.mg', country : 'Madagascar'},
        {domain : '.mh', country : 'Marshall Islands'},
        {domain : '.mk', country : 'Macedonia'},
        {domain : '.ml', country : 'Mali'},
        {domain : '.mm', country : 'Myanmar'},
        {domain : '.mn', country : 'Mongolia'},
        {domain : '.mo', country : 'Macau'},
        {domain : '.mp', country : 'Northern Mariana Islands'},
        {domain : '.mq', country : 'Martinique'},
        {domain : '.mr', country : 'Mauritania'},
        {domain : '.ms', country : 'Montserrat'},
        {domain : '.mt', country : 'Malta'},
        {domain : '.mu', country : 'Mauritius'},
        {domain : '.mv', country : 'Maldives'},
        {domain : '.mw', country : 'Malawi'},
        {domain : '.mx', country : 'Mexico'},
        {domain : '.my', country : 'Malaysia'},
        {domain : '.mz', country : 'Mozambique'},
        {domain : '.na', country : 'Namibia'},
        {domain : '.nc', country : 'New Caledonia'},
        {domain : '.ne', country : 'Niger'},
        {domain : '.nf', country : 'Norfolk Island'},
        {domain : '.ng', country : 'Nigeria'},
        {domain : '.ni', country : 'Nicaragua'},
        {domain : '.nl', country : 'Netherlands'},
        {domain : '.no', country : 'Norway'},
        {domain : '.np', country : 'Nepal'},
        {domain : '.nr', country : 'Nauru'},
        {domain : '.nt', country : 'Neutral Zone'},
        {domain : '.nu', country : 'Niue'},
        {domain : '.nz', country : 'New Zealand (Aotearoa)'},
        {domain : '.om', country : 'Oman'},
        {domain : '.pa', country : 'Panama'},
        {domain : '.pe', country : 'Peru'},
        {domain : '.pf', country : 'French Polynesia'},
        {domain : '.pg', country : 'Papua New Guinea'},
        {domain : '.ph', country : 'Philippines'},
        {domain : '.pk', country : 'Pakistan'},
        {domain : '.pl', country : 'Poland'},
        {domain : '.pm', country : 'St. Pierre and Miquelon'},
        {domain : '.pn', country : 'Pitcairn'},
        {domain : '.pr', country : 'Puerto Rico'},
        {domain : '.pt', country : 'Portugal'},
        {domain : '.pw', country : 'Palau'},
        {domain : '.py', country : 'Paraguay'},
        {domain : '.qa', country : 'Qatar'},
        {domain : '.re', country : 'Reunion'},
        {domain : '.ro', country : 'Romania'},
        {domain : '.ru', country : 'Russian Federation'},
        {domain : '.rw', country : 'Rwanda'},
        {domain : '.sa', country : 'Saudi Arabia'},
        {domain : '.sb', country : 'Solomon Islands'},
        {domain : '.sc', country : 'Seychelles'},
        {domain : '.sd', country : 'Sudan'},
        {domain : '.se', country : 'Sweden'},
        {domain : '.sg', country : 'Singapore'},
        {domain : '.sh', country : 'Saint Helena'},
        {domain : '.si', country : 'Slovenia'},
        {domain : '.sj', country : 'Svalbard and Jan Mayen Islands'},
        {domain : '.sk', country : 'Slovakia'},
        {domain : '.sl', country : 'Sierra Leone'},
        {domain : '.sm', country : 'San Marino'},
        {domain : '.sn', country : 'Senegal'},
        {domain : '.so', country : 'Somalia'},
        {domain : '.sr', country : 'Suriname'},
        {domain : '.st', country : 'Sao Torme and Principe'},
        {domain : '.su', country : 'Former USSR'},
        {domain : '.sv', country : 'El Salvador'},
        {domain : '.sy', country : 'Syria'},
        {domain : '.sz', country : 'Swaziland'},
        {domain : '.tc', country : 'Turks and Caicos Islands'},
        {domain : '.td', country : 'Chad'},
        {domain : '.tf', country : 'French Southern Territory'},
        {domain : '.tg', country : 'Togo'},
        {domain : '.th', country : 'Thailand'},
        {domain : '.tj', country : 'Tajikistan'},
        {domain : '.tk', country : 'Tokelau'},
        {domain : '.tm', country : 'Turkmenistan'},
        {domain : '.tn', country : 'Tunisia'},
        {domain : '.to', country : 'Tonga'},
        {domain : '.tp', country : 'East Timor'},
        {domain : '.tr', country : 'Turkey'},
        {domain : '.tt', country : 'Trinidad and Tobago'},
        {domain : '.tv', country : 'Tuvalu'},
        {domain : '.tw', country : 'Taiwan'},
        {domain : '.tz', country : 'Tanzania'},
        {domain : '.ua', country : 'Ukraine'},
        {domain : '.ug', country : 'Uganda'},
        {domain : '.uk', country : 'United Kingdom'},
        {domain : '.um', country : 'Minor Outlying Islands'},
        {domain : '.us', country : 'USA'},
        {domain : '.uy', country : 'Uruguay'},
        {domain : '.uz', country : 'Uzbekistan'},
        {domain : '.va', country : 'Vatican City State'},
        {domain : '.vc', country : 'Saint Vincent and the Grenadines'},
        {domain : '.ve', country : 'Venezuela'},
        {domain : '.vg', country : 'British Virgin Islands'},
        {domain : '.vi', country : 'Virgin Islands'},
        {domain : '.vn', country : 'Viet Nam'},
        {domain : '.vu', country : 'Vanuatu'},
        {domain : '.wf', country : 'Wallis and Futuna Islands'},
        {domain : '.ws', country : 'Samoa'},
        {domain : '.ye', country : 'Yemen'},
        {domain : '.yt', country : 'Mayotte'},
        {domain : '.yu', country : 'Yugoslavia'},
        {domain : '.za', country : 'South Africa'},
        {domain : '.zm', country : 'Zambia'},
        {domain : '.zr', country : 'Zaire'},
        {domain : '.zw', country : 'Zimbabwe'}
    ];
    var price = Number(config.price)
        , adminEmail = config.adminEmail
        , gmailPassword = config.gmailPassword
        , domain = config.domain
        , issueInvitesCode = config.issueInvitesCode
        , domainName = config.domainName
        , queueLimit = 1000
        , smallTopLevelDomains = [".com",".org",".edu",".gov",".info",".co",".net"]
        , topLevelDomains = [".com",".org",".edu",".gov",".cat",".coop",".info",".int",".jobs",".mil",".mobi",".museum",
            ".name",".net",".travel",".ac",".ad",".ae",".af",".ag",".ai",".al",".am",".an",".ao",".aq",".ar",".as",".at",".au",".aw",
            ".az",".ba",".bb",".bd",".be",".bf",".bg",".bh",".bi",".bj",".bm",".bn",".bo",".br",".bs",".bt",".bv",".bw",".by",".bz",".ca",
            ".cc",".cd",".cf",".cg",".ch",".ci",".ck",".cl",".cm",".cn",".co",".cr",".cs",".cu",".cv",".cx",".cy",".cz",".de",".dj",".dk",".dm",
            ".do",".dz",".ec",".ee",".eg",".eh",".er",".es",".et",".eu",".fi",".fj",".fk",".fm",".fo",".fr",".ga",".gb",".gd",".ge",".gf",".gg",".gh",
            ".gi",".gl",".gm",".gn",".gp",".gq",".gr",".gs",".gt",".gu",".gw",".gy",".hk",".hm",".hn",".hr",".ht",".hu",".id",".ie",".il",".im",
            ".in",".io",".iq",".ir",".is",".it",".je",".jm",".jo",".jp",".ke",".kg",".kh",".ki",".km",".kn",".kp",".kr",".kw",".ky",".kz",".la",".lb",
            ".lc",".li",".lk",".lr",".ls",".lt",".lu",".lv",".ly",".ma",".mc",".md",".mg",".mh",".mk",".ml",".mm",".mn",".mo",".mp",".mq",
            ".mr",".ms",".mt",".mu",".mv",".mw",".mx",".my",".mz",".na",".nc",".ne",".nf",".ng",".ni",".nl",".no",".np",".nr",".nu",
            ".nz",".om",".pa",".pe",".pf",".pg",".ph",".pk",".pl",".pm",".pn",".pr",".ps",".pt",".pw",".py",".qa",".re",".ro",".ru",".rw",
            ".sa",".sb",".sc",".sd",".se",".sg",".sh",".si",".sj",".sk",".sl",".sm",".sn",".so",".sr",".st",".su",".sv",".sy",".sz",".tc",".td",".tf",
            ".tg",".th",".tj",".tk",".tm",".tn",".to",".tp",".tr",".tt",".tv",".tw",".tz",".ua",".ug",".uk",".um",".us",".uy",".uz", ".va",".vc",
            ".ve",".vg",".vi",".vn",".vu",".wf",".ws",".ye",".yt",".yu",".za",".zm",".zr",".zw"];

    mongoose.connect(config.mongoString);

    var Schema = mongoose.Schema,
        ObjectId = Schema.ObjectId;

    var emailBlasts = new Schema({
        id    : ObjectId
        , followUpMessage : String
        , emailFrom : String
        , nameFrom : String
        , templateMessage : String
        , subject : String
        , leads : [{ type: Schema.Types.ObjectId, ref: 'Lead' }]
        , date : Date
        , dateSend : Date
        , sentDate : Date
        , dateFollowUpSend : Date
        , followUpMessage : String
        , attachments : Schema.Types.Mixed
        , followUpDate : Date
        , type : String
        , activated : Boolean
        , blastId : String
        , creditsSpent : Number
    });

    emailBlasts.set('autoIndex', false);

    var leads = new Schema({
        id    : ObjectId
        , pubMedId : String
        , title : String
        , searchTerm : String
        , authorInfo : String
        , email : String
        , country : String
        , affiliation : String
        , firstname : String
        , lastname : String
        , initials : String
        , institution : String
        , department : String
        , phone : String
        , state : String
        , paperDate : Date
        , address : String
        , type : String
        , linkedinUrl : String
        , linkedinId : String
        , linkedinPictureUrl : String
        , linkedinDate : Date
    });

    leads.set('autoIndex', false);

    leads.index({ pubMedId: 1, email: 1, firstname : 1, lastname : 1, type : 1 });

    var papers = new Schema({
        id    : ObjectId
        , pubMedId : String
        , contents : String
        , type : String
        , database : String
        , json : { type: String, index: false }
        , filename : String
        , dateAdded : Date
        , leadsParsed : Boolean
    });

    papers.set('autoIndex', false);

    papers.index({ pubMedId: 1, filename: 1 });

    var blastHistories = new Schema({
        id    : ObjectId
        , searchTerms : [String]
        , searchArray : [String]
        , isAnd : Boolean
        , date : Date
        , startDate : Date
        , endDate : Date
        , dateUpdated : Date
        , activated : Boolean
        , totalLeads : Number
        , leadCount : Number
        , newLeads : [String]
        , pubMedIds : [String]
        , activatedDate : Date
        , autoRefresh: Boolean
        , leads : [{ type: Schema.Types.ObjectId, ref: 'Lead' }]
        , inactiveLeads : [{ type: Schema.Types.ObjectId, ref: 'Lead' }]
        , emailBlasts : [emailBlasts]
    });

    blastHistories.set('autoIndex', false);

    var lists = new Schema({
        id    : ObjectId
        , searchTerms : [String]
        , searchArray : [String]
        , name : String
        , periodic : Boolean
        , period : String
        , date : Date
        , dateUpdated : Date
        , activated : Boolean
        , leadCount : Number
        , activatedDate : Date
        , leads : [{ type: Schema.Types.ObjectId, ref: 'Lead' }]
        , emailBlasts : [emailBlasts]
    });

    lists.set('autoIndex', false);

    var subscriptionHistories = new Schema({
        id    : ObjectId
        , date : Date
        , price : Number
        , currency : String
        , payMethod : String
        , paymentId : String
    });

    subscriptionHistories.set('autoIndex', false);

    var creditGrants = new Schema({
        id    : ObjectId
        , date : Date
        , dateRedeemed : Date
        , amount : Number
        , code : String
        , emailTo : String
        , emailFrom : String
        , redeemed : Boolean
    });

    var creditHistories = new Schema({
        id    : ObjectId
        , date : Date
        , price : Number
        , amount : Number
        , currency : String
        , payMethod : String
        , paymentId : String
        , typeId : String
        , type : String
    });

    creditHistories.set('autoIndex', false);

    var emailsValidated = new Schema({
        id    : ObjectId
        , date : Date
        , email : String
        , valid : Boolean
        , firstname : String
        , lastname : String
        , initials : String
        , organization : String
        , ending : String
        , institutionParsed : String
        , topLevelDomain : String
        , paper : String
        , pubMedId : String
        , userSearched : String
    });

    emailsValidated.set('autoIndex', false);

    emailsValidated.index({ pubMedId: 1, email: 1, firstname : 1, lastname : 1, valid : 1, organization : 1, ending : 1 });

    var inviteCodes = new Schema({
        id    : ObjectId
        , code : String
        , user : String
        , used : Boolean
        , usedDate: Date
        , expirationDate : Date
        , issuedDate: Date
        , dateRangeStart : Date
        , dateRangeEnd : Date
        , type : String
        , duration : Schema.Types.Mixed
        , email : String
    });

    inviteCodes.set('autoIndex', false);

    inviteCodes.index({ user: 1, used: 1, code : 1, email : 1 });

    var users = new Schema({
        id    : ObjectId
        , email     : String // used for login confirmation
        , username  : String // optional CRUD on profile view // if added display on all headings
        , password  : String // U on profile view
        , name : String
        , salutation : String
        , firstname : String
        , lastname : String
        , organization : String
        , stripeCustomerId : String
        , title : String
        , credits : Number
        , alreadyEmailed : [String]
        , joinDate  : Date // display on profile view
        , activeDate : Date // display on profile view
        , verified: Boolean
        , subscribed: Boolean
        , lists : [lists]
        , monthFreeStart: Date
        , paidStart: Date
        , verifyCode : String
        , emailsValidated : [{ type: Schema.Types.ObjectId, ref: 'EmailValidated' }]
        , leadTermArray : [Schema.Types.Mixed]
        , creditHistories : [creditHistories]
        , subscriptionHistories: [subscriptionHistories]
        , blastHistories: [blastHistories]
        , signature: String
        , credits : Number
        , tweeted: Boolean
        , linkedin: Boolean
        , emails: [String]
        , dontTalk : [String]
        , emailBlasts : [emailBlasts]
    });

    users.set('autoIndex', false);

    users.index({ email: 1, username: 1, verifyCode : 1 });

    var searchEmails = new Schema({
        id    : ObjectId
        , searchTerms     : [String] // used for login confirmation
        , searchArray : [String]
        , ids  : [String] // optional CRUD on profile view // if added display on all headings
        , titles  : [String] // U on profile view
        , authorInfo  : String // display on profile view
        , emails : [String] // display on profile view
        , customMessage : String
        , from : String
        , date : Date
    });

    searchEmails.set('autoIndex', false);

    var Lead = mongoose.model('Lead', leads)
        , EmailBlast = mongoose.model('EmailBlast', emailBlasts)
        , BlastHistory = mongoose.model('BlastHistory', blastHistories)
        , List = mongoose.model('List', lists)
        , Paper = mongoose.model('Paper', papers)
        , CreditGrant = mongoose.model('CreditGrant', creditGrants)
        , SubscriptionHistory = mongoose.model('SubscriptionHistory', subscriptionHistories)
        , CreditHistory = mongoose.model('CreditHistory', creditHistories)
        , InviteCode = mongoose.model('InviteCode', inviteCodes)
        , EmailValidated = mongoose.model('EmailValidated', emailsValidated)
        , User = mongoose.model('User', users)
        , SearchEmail = mongoose.model('SearchEmail', searchEmails);

    function checkLinkedIn(leadz){
        if (!isEmpty(leadz) && leadz.country && leadz.lastname && leadz.institution && !leadz.linkedinUrl){
            var passThrough = true;

            if (leadz.linkedinDate){
                var then = leadz.linkedinDate.getTime(),
                    now  = new Date().getTime(),
                    thirtyDaysInMilliseconds = 1592000000;
                if (now - then < thirtyDaysInMilliseconds) {
                    passThrough = false;
                }
            }

            if (passThrough){
                domainList.forEach(function(dou){
                    if (dou.country == leadz.country){
                        var countryCode = dou.domain.substring(1);
                        linkedinClient.apiCall('GET', '/people-search?keywords=' + encodeURIComponent(leadz.institution) + '&last-name=' + leadz.lastname + '&country-code=' + countryCode,
                            {
                                token: {
                                    oauth_token_secret: config.linkedinOAuthSecret
                                    , oauth_token: config.linkedinOAuth
                                }
                            }
                            , function (error, result) {
                                if (result && result[0]){
                                    linkedinClient.apiCall('GET', '/people/id=' + result[0].id + ':(public-profile-url,picture-url)',
                                        {
                                            token: {
                                                oauth_token_secret: config.linkedinOAuthSecret
                                                , oauth_token: config.linkedinOAuth
                                            }
                                        }
                                        , function (error, resulted) {
                                            if (resulted){
                                                leadz.linkedinUrl = resulted.publicProfileUrl;
                                                leadz.linkedinId = result[0].id;
                                                leadz.linkedinPictureUrl = resulted.pictureUrl;
                                                leadz.linkedinDate = new Date();
                                                leadz.save(function(err,lead){});
                                            }
                                        }
                                    );
                                }
                                else {
                                    leadz.linkedinDate = new Date();
                                    leadz.save(function(err,lead){});
                                }
                            }
                        );
                    }
                });
            }
        }
    }

    function linkedinTest(){
        linkedinClient.apiCall('GET', '/people-search?last-name=Mimms&country-code=us',
            {
                token: {
                    oauth_token_secret: config.linkedinOAuthSecret
                    , oauth_token: config.linkedinOAuth
                }
            }
            , function (error, result) {
                console.log(error);
                console.log(result);
                if (result && result[0]){
                    linkedinClient.apiCall('GET', '/people/id=' + result[0].id + ':(public-profile-url,picture-url)',
                        {
                            token: {
                                oauth_token_secret: config.linkedinOAuthSecret
                                , oauth_token: config.linkedinOAuth
                            }
                        }
                        , function (error, resulted) {
                            console.log(error);
                            console.log(resulted);
                        }
                    );
                }
            }
        );
    }

    function isEmpty(ob){
        for(var i in ob){ if(ob.hasOwnProperty(i)){return false;}}
        return true;
    }

    function escapeRegExp(str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }

    function parseCountry(led){
        var countryParse = parseEmail(led.affiliation);
        countryList.some(function(count){
            var countMatch = new RegExp(count,"g");
            var countried = countryParse.match(countMatch);
            if (countried && countried[0]){
                led.country = countried[countried.length - 1];
                if (led.country == "America"){
                    led.country = "United States";
                }
                if (led.country == "USA"){
                    led.country = "United States";
                }
                if (led.country == "U.S.A."){
                    led.country = "United States";
                }
                if (led.country == "UK"){
                    led.country = "United Kingdom";
                }
                if (led.country == "U.K."){
                    led.country = "United Kingdom";
                }
                if (led.country == "Chinese"){
                    led.country = "China";
                }
                if (led.country == "Mexico"){
                    led.country = "MÃ©xico";
                }
                return count === 1;
            }
        });
        if (!led.country || led.country == 'United States'){
            stateList.some(function(count){
                var countMatch = new RegExp(count.label + '|' + count.data,"g");
                var countried = countryParse.match(countMatch);
                if (countried && countried[0]){
                    led.country = 'United States';
                    led.state = count.label;
                    return count === 1;
                }
            });
        }
        if (!led.country && led.email){
            domainList.some(function(count){
                var countMatch = new RegExp(escapeRegExp(count.domain),"g");
                var countried = led.email.match(countMatch);
                if (countried && countried[0]){
                    led.country = count.country;
                    return count === 1;
                }
            });
        }
        if (led.affiliation && !led.institution){
            var affilly2 = led.affiliation.split(',');
            var afilCount = 0;
            var finalCount;
            affilly2.forEach(function(affil){

                var countMatch = new RegExp("College","g");
                var countried = affil.match(countMatch);
                if (countried && countried[0]){
                    led.institution = affil;
                    finalCount = afilCount;
                }
                var countMatch = new RegExp("University","g");
                var countried = affil.match(countMatch);
                if (countried && countried[0]){
                    led.institution = affil;
                    finalCount = afilCount;
                }
                var countMatch = new RegExp("Institute","g");
                var countried = affil.match(countMatch);
                if (countried && countried[0]){
                    led.institution = affil;
                    finalCount = afilCount;
                }
                var countMatch = new RegExp("School","g");
                var countried = affil.match(countMatch);
                if (countried && countried[0]){
                    led.institution = affil;
                    finalCount = afilCount;
                }
                afilCount += 1;
            });
            if (!led.institution){
                if (affilly2.length > 1){
                    led.institution = affilly2[1];
                }
                else {
                    led.institution = affilly2[0];
                }
            }
        }
        if (led.affiliation && !led.department){
            var affilly = led.affiliation.split(',');
            if (finalCount && finalCount !== 0){
                if (affilly.length > 1){
                    led.department = affilly[0];
                }
            }
        }
        return led;
    }

    function sendEmail(toEmail, plainText, html, subject, fromed, name, attachments) {
        var smtpTransport = nodemailer.createTransport("SMTP",{
            service: "Gmail",
            auth: {
                user: config.adminEmail,
                pass: config.gmailPassword
            }
        });
        var from = name + " <" + fromed + ">";

        if (attachments){
            var mailOptions = {
                from: from,
                to: toEmail,
                subject: subject,
                text: plainText,
                html: html,
                replyTo: from,
                attachments: attachments
            };
        }
        else {
            var mailOptions = {
                from: from,
                to: toEmail,
                subject: subject,
                text: plainText,
                replyTo: from,
                html: html
            };
        }

        smtpTransport.sendMail(mailOptions, function(error, response){
            if (!isEmpty(error)){

            }else{
            }
            smtpTransport.close();
        });
    }

    function shaValue(valueFixed){
        return require('crypto').createHash('sha512').update(valueFixed).digest('hex');
    }

    passport.use(new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        function(req, email, password, done) {
            var email_found = false, password_match = false;
            password = shaValue(password);
            User.findOne({email:email}, 'email password _id', function (err, user) {
                if (!isEmpty(user)) {
                    bcrypt.compare(password, user.password, function(err, result) {
                        if (result){
                            email_found = true;
                            password_match = true;
                            if (email == 'jared@leaddynamo.info'){
                                user.credits = 10000;
                                user.save(function(err,ure){
                                    if (!isEmpty(ure)){
                                        return done(null, ure, {message : "Login successful."});
                                    }
                                });
                            }
                            else {
                                return done(null, user, {message : "Login successful."});
                            }
                        }
                        else {
                            return done(null, false, { message: 'Password mismatch.' });
                        }
                    });
                }
                else {
                    return done(null, false, { message: 'No email login found.' });
                }
            });
        }
    ));

    passport.use(new TwitterStrategy({
            consumerKey: config.twitterKey,
            consumerSecret: config.twitterSecret,
            callbackURL : '/auth/twitter/callback',
            passReqToCallback: true
        },
        function(req, token, tokenSecret, profile, done) {
            if (req.session && req.session.passport && req.session.passport.user){
                User.findById(req.session.passport.user, 'credits tweeted', function (err, user) {
                    if (!isEmpty(user) && !user.tweeted){
                        var twit = new twitter({
                            consumer_key: config.twitterKey,
                            consumer_secret: config.twitterSecret,
                            access_token_key: token,
                            access_token_secret: tokenSecret
                        });
                        twit
                            .verifyCredentials(function(data) {

                            })
                            .updateStatus('Generating leads with LeadDynamo! ' + config.domain,
                            function(data) {
                                if (user.credits){
                                    user.credits = user.credits + 25;
                                }
                                else {
                                    user.credits = 25;
                                }
                                user.tweeted = true;
                                user.save(function(err,used){
                                    if (!isEmpty(used)){
                                        req.session.tweeted = true;
                                        return done(err, used);
                                    }
                                });
                            }
                        );
                    }
                });
            }
        }
    ));

    passport.use(new LinkedInStrategy({
            consumerKey: config.linkedinKey,
            consumerSecret: config.linkedinSecret,
            callbackURL : '/auth/linkedin/callback',
            passReqToCallback: true
        },
        function(req, token, tokenSecret, profile, done) {
            if (req.session && req.session.passport && req.session.passport.user){
                User.findById(req.session.passport.user, 'credits linkedin', function (err, user) {
                    if (!isEmpty(user) && !user.linkedin){

                        var post_data = "<?xml version='1.0' encoding='UTF-8'?><share><comment>Generating leads with " + config.domainName + "!</comment><content><title>LeadDynamo - Lead Generation Made Easy</title><description>Generate scientifically targeted leads as simple as a search.</description><submitted-url>" + config.domain + "</submitted-url><submitted-image-url>" + config.domain + "/images/LeadDynamo.png</submitted-image-url></content><visibility><code>anyone</code></visibility></share>";

                        var oa= new OAuth("https://api.linkedin.com/uas/oauth/requestToken",
                            "https://api.linkedin.com/uas/oauth/accessToken",
                            config.linkedinKey,  config.linkedinSecret, "1.0", "/", "HMAC-SHA1");
                        oa.post("http://api.linkedin.com/v1/people/~/shares", token, tokenSecret, post_data, 'text/xml', function(error, data){
                            if (data){
                                user.credits = user.credits + 25;
                                user.linkedin = true;
                                user.save(function(err,used){
                                    if (!isEmpty(used)){
                                        req.session.linkedin = true;
                                        return done(err, used);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }
    ));

    passport.serializeUser(function(user, done) {
        done(null, user._id);
    });
    passport.deserializeUser(function(id, done) {
        User.findById(id, '_id', function(err, user) {
            done(err, user);
        });
    });

    var expressIo = require('express.io');

    var app = expressIo();

    app.http().io();

    app.io.set('match origin protocol', true);

    var redisCli = require('redis');

    app.io.set('store', new expressIo.io.RedisStore({
        redisPub: redisCli.createClient(),
        redisSub: redisCli.createClient(),
        redisClient: redisCli.createClient()
    }));

    app.set('port', 8080);

    app.engine('hbs', hbs.__express);
    app.set('view engine', 'hbs');
    app.set('view options', {
        layout: false
    });
    app.set('views', __dirname + '/views');

    app.use(express.compress());

    app.use(express.favicon());
    app.use(express.cookieParser());
    app.use(express.bodyParser({limit: '100mb'}));
    app.use(express.json({limit: '100mb'}));
    app.use(express.urlencoded({limit: '100mb'}));

    var allowCrossDomain = function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    };

    app.use(express.logger('dev'));
    app.use(express.session({ secret: 'fsdv8dfh98refhFHUFDHf98dfh9shfiksdef343', key: 'express.sid', store: new RedisStore({ host: 'localhost', port: 6379, client: redis }) }));

    app.use(express.static(path.join(__dirname, 'public')));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(allowCrossDomain);
    app.use(app.router);

    if ('development' == app.get('env')) {
        app.use(express.errorHandler());
    }

    app.listen(app.get('port'));

    app.all('*',function(req,res,next) {
        if ('production' == app.get('env') && req.headers['x-forwarded-proto'] != 'https') {
            res.redirect(config.domain + req.url);
        }
        else {
            next();
        }
    });

    process.on('message', function(msg) {
        if (msg.type == 'newLeads'){
            if (msg.newLeads && msg.newLeads[0]){
                msg.newLeads = _.map(msg.newLeads, function(history){
                    if (history.activated == true){
                        var historied = history.toObject();
                        if (historied.leads && historied.leads[0]){
                            historied.leads.forEach(function(led){
                                if (led.paperDate){
                                    led.paperDateFormatted = moment(led.paperDate).format("dddd, MMMM Do YYYY");
                                }
                                if (led.affiliation){
                                    if (!led.email){
                                        var authorEmailMatch = led.affiliation.match(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/);
                                        if (authorEmailMatch && authorEmailMatch[0]){
                                            led.email = authorEmailMatch[0];
                                        }
                                    }
                                    led = parseCountry(led);

                                    var affy = parseAffiliation(led.affiliation);

                                    led.affiliation = affy;
                                }
                            });
                        }
                        if (historied.leadCount && historied.totalLeads){
                            historied.totalLeadsLeft = historied.totalLeads - historied.leadCount;
                        }
                        else {
                            historied.totalLeadsLeft = 0;
                        }

                        historied.totalLeadsBoolean;
                        if (historied.totalLeadsLeft > 0){
                            historied.totalLeadsBoolean = true;
                        }
                        else {
                            historied.totalLeadsBoolean = false;
                        }
                        if (historied.searchArray && historied.searchArray[0]){
                            var global;
                            historied.searchArray.forEach(function(search){
                                if (search == 'Global'){
                                    global = 'Global';
                                }
                            });
                            if (global){
                                historied.searchArray = [global];
                            }
                        }
                        historied.formattedDate = moment(historied.date).format("dddd, MMMM Do YYYY");
                        historied.formattedUpdateDate = moment(historied.updateDate).format("dddd, MMMM Do YYYY");
                        historied._id = historied._id.toString();
                        return historied;
                    }
                });
            }
            app.io.room(msg.userId.toString()).broadcast('newLeads', msg);
        }
    });

    function randomString(length, chars) {
        var result = '';
        for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
        return result;
    }

    var globalValidationQueue = async.queue(function (task, callback) {

        var email = task.email;
        var emailParse = email.split('@');
        var organization = task.organization;
        var ending = task.ending;
        var validEmail;
        var validEmail2;

        exec('nslookup -q=mx ' + emailParse[1], function (error, stdout, stderr) {
            var resed;
            if (stdout && typeof stdout.toString() == 'string'){
                resed = stdout.match(/mail addr = .*\n/);

                if (resed && resed[0]){
                    resed = resed[0].match(/([a-z0-9]+\.)*[a-z0-9\-\.]+\.[a-z]+/i);
                }
                else {
                    resed = stdout.match(/mail exchanger = .*\n/);
                }
                if (resed && resed[0]){
                    resed = resed[0].match(/([a-z0-9]+\.)*[a-z0-9\-\.]+\.[a-z]+/i);
                }
            }
            if (resed && resed[0]){
                var valid = false;
                var mailSent = false;
                var rcptSent = false;
                var helo = false;
                var ended = false;
                var conn = net.connect(25,resed[0],
                    function() {
                        var counter = 0;
                        var before;
                        conn.on("data", function (c) {
                            c = c.toString();
                            if (c.match(/250/) && c.match(/250/)[0]){
                                counter += 1;
                            }
                            if ((c.match(/550/) && c.match(/550/)[0]) || (c.match(/450/) && c.match(/450/)[0])){
                                conn.end();
                            }
                            else if (c.match(/220/) && c.match(/220/)[0] && !helo){
                                conn.write("helo hi\r\n");
                                helo = true;
                            }
                            else if (counter == 1 && !mailSent && !rcptSent){
                                conn.write("mail from: <" + adminEmail + ">\r\n");
                                mailSent = true;
                            }
                            else if (counter == 2 && !rcptSent && mailSent){
                                conn.write("rcpt to: <" + randomString(20, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') + '@' + email.split('@')[1] + ">\r\n");
                                rcptSent = true;
                            }
                            else if (counter >= 3 && !ended && mailSent && rcptSent){
                                if (c.match(/250/) && c.match(/250/)[0]){
                                    valid = true;
                                    validEmail2 = email;
                                    ended = true;
                                    conn.end();
                                }
                                else if ((c.match(/450/) && c.match(/450/)[0]) || (c.match(/550/) && c.match(/550/)[0])){
                                    valid = false;
                                    ended = true;
                                    conn.end();
                                }
                            }
                        });
                        conn.on("end", function (e) {
                            var valid1 = false;
                            mailSent = false;
                            rcptSent = false;
                            helo = false;
                            ended = false;
                            if (!validEmail2){
                                var conned = net.connect(25,resed[0],
                                    function() {
                                        var counter = 0;
                                        var before;
                                        conned.on("data", function (c) {
                                            c = c.toString();
                                            if (c.match(/250/) && c.match(/250/)[0]){
                                                counter += 1;
                                            }
                                            if ((c.match(/550/) && c.match(/550/)[0]) || (c.match(/450/) && c.match(/450/)[0])){
                                                conned.end();
                                            }
                                            else if (c.match(/220/) && c.match(/220/)[0] && !helo){
                                                conned.write("helo hi\r\n");
                                                helo = true;
                                            }
                                            else if (counter == 1 && !mailSent && !rcptSent){
                                                conned.write("mail from: <" + adminEmail + ">\r\n");
                                                mailSent = true;
                                            }
                                            else if (counter == 2 && !rcptSent && mailSent){
                                                conned.write("rcpt to: <" + email + ">\r\n");
                                                rcptSent = true;
                                            }
                                            else if (counter >= 3 && !ended && mailSent && rcptSent){
                                                if (c.match(/250/) && c.match(/250/)[0]){
                                                    valid = true;
                                                    validEmail = email;
                                                    ended = true;
                                                    conned.end();
                                                }
                                                else if ((c.match(/450/) && c.match(/450/)[0]) || (c.match(/550/) && c.match(/550/)[0])){
                                                    valid = false;
                                                    ended = true;
                                                    conned.end();
                                                }
                                            }
                                        });
                                        conned.on("end", function (e) {
                                            if (validEmail){
                                                var validatedObject = new EmailValidated({date : new Date(), email : email.toLowerCase(), firstname : task.firstname, lastname : task.lastname, valid : valid});
                                                if (task.organization){
                                                    validatedObject.organization = task.organization.toLowerCase();
                                                }
                                                if (task.ending){
                                                    validatedObject.ending = task.ending.toLowerCase();
                                                }
                                                validatedObject.save(function(err,validObj){
                                                    callback(validEmail, validObj._id);
                                                });
                                            }
                                            else {
                                                callback(validEmail, '');
                                            }
                                        });
                                    });
                                conned.on('error',function(e){
                                    callback(validEmail, '');
                                });
                            }
                            else {
                                callback(validEmail, '');
                            }
                        });
                    });
                conn.on('error',function(e){
                    callback(validEmail, '');
                });
            }
            else {
                callback(validEmail, '');
            }
        });
    }, queueLimit);

    app.post('/validation', function(req, res){
        if (typeof req.body.fullname == 'string' && (typeof req.body.ending == 'string' || typeof req.body.organization == 'string') && ((req.session && req.session.passport && req.session.passport.user) || (req.session && (!req.session.searches || !isNaN(Number(req.session.searches)) && Number(req.session.searches) <= 5)))){

            var nameArray = [];
            var emailCombos = [];

            var fulled = req.body.fullname.split(' ');

            req.body.firstname = fulled[0];
            if (fulled[1] && !fulled[2]){
                req.body.lastname = fulled[1];
            }
            else if (fulled[1] && fulled[2]){
                req.body.middlename = fulled[1];
                req.body.lastname = fulled[2];
            }

            if (req.body.firstname){
                nameArray.push(req.body.firstname);
            }
            if (req.body.lastname){
                nameArray.push(req.body.lastname);
            }
            if (req.body.firstname && req.body.lastname){
                nameArray.push(req.body.firstname.charAt(0) + req.body.lastname);
                nameArray.push(req.body.firstname.charAt(0) + req.body.lastname.charAt(0));
                nameArray.push(req.body.firstname.charAt(0) + req.body.firstname.charAt(1) + req.body.lastname);
                nameArray.push(req.body.firstname + req.body.lastname);
                nameArray.push(req.body.firstname + req.body.lastname.charAt(0));
                nameArray.push(req.body.lastname + req.body.firstname);
                nameArray.push(req.body.firstname + '.' + req.body.lastname);
                nameArray.push(req.body.lastname + '.' + req.body.firstname);
                nameArray.push(req.body.lastname + '.' + req.body.firstname.charAt(0));
                nameArray.push(req.body.lastname.charAt(0) + '.' + req.body.firstname);
                nameArray.push(req.body.firstname.charAt(0) + '.' + req.body.lastname.charAt(0));
                nameArray.push(req.body.firstname.charAt(0) + '.' + req.body.lastname);
                nameArray.push(req.body.firstname + '.' + req.body.lastname.charAt(0));
                nameArray.push(req.body.firstname + '_' + req.body.lastname);
                nameArray.push(req.body.lastname + '_' + req.body.firstname);
                nameArray.push(req.body.lastname + '_' + req.body.firstname.charAt(0));
                nameArray.push(req.body.lastname.charAt(0) + '_' + req.body.firstname);
                nameArray.push(req.body.firstname.charAt(0) + '_' + req.body.lastname.charAt(0));
                nameArray.push(req.body.firstname.charAt(0) + '_' + req.body.lastname);
                nameArray.push(req.body.firstname + '_' + req.body.lastname.charAt(0));
                nameArray.push(req.body.firstname + '-' + req.body.lastname);
                nameArray.push(req.body.lastname + '-' + req.body.firstname);
                nameArray.push(req.body.lastname + '-' + req.body.firstname.charAt(0));
                nameArray.push(req.body.lastname.charAt(0) + '-' + req.body.firstname);
                nameArray.push(req.body.firstname.charAt(0) + '-' + req.body.lastname.charAt(0));
                nameArray.push(req.body.firstname.charAt(0) + '-' + req.body.lastname);
                nameArray.push(req.body.firstname + '-' + req.body.lastname.charAt(0));
            }
            if (req.body.middlename){
                nameArray.push(req.body.firstname.charAt(0) + req.body.middlename + req.body.lastname);
                nameArray.push(req.body.firstname.charAt(0) + req.body.middlename.charAt(0) + req.body.lastname);
                nameArray.push(req.body.firstname.charAt(0) + req.body.middlename.charAt(0) + req.body.lastname.charAt(0));
                nameArray.push(req.body.firstname.charAt(0) + req.body.firstname.charAt(1) + req.body.middlename.charAt(0) + req.body.lastname);
                nameArray.push(req.body.firstname.charAt(0) + req.body.firstname.charAt(1) + req.body.middlename + req.body.lastname);
                nameArray.push(req.body.firstname + req.body.lastname);
                nameArray.push(req.body.firstname + req.body.middlename + req.body.lastname);
                nameArray.push(req.body.lastname + req.body.middlename + req.body.firstname);
                nameArray.push(req.body.firstname + '.' + req.body.middlename + '.' + req.body.lastname);
                nameArray.push(req.body.lastname + '.' + req.body.middlename + '.' + req.body.firstname);
                nameArray.push(req.body.lastname + '.' + req.body.middlename + '.' + req.body.firstname.charAt(0));
                nameArray.push(req.body.lastname + '.' + req.body.middlename.charAt(0) + '.' + req.body.firstname.charAt(0));
                nameArray.push(req.body.lastname.charAt(0) + '.' + req.body.middlename.charAt(0) + '.' + req.body.firstname);
                nameArray.push(req.body.lastname.charAt(0) + '.' + req.body.middlename + '.' + req.body.firstname);
                nameArray.push(req.body.firstname.charAt(0) + '.' + req.body.middlename.charAt(0) + '.' + req.body.lastname.charAt(0));
                nameArray.push(req.body.firstname.charAt(0) + '.' + req.body.middlename.charAt(0) + '.' + req.body.lastname);
                nameArray.push(req.body.firstname.charAt(0) + '.' + req.body.middlename + '.' + req.body.lastname);
                nameArray.push(req.body.firstname + '.' + req.body.middlename + '.' + req.body.lastname.charAt(0));
                nameArray.push(req.body.firstname + '.' + req.body.middlename.charAt(0) + '.' + req.body.lastname.charAt(0));
                nameArray.push(req.body.firstname + '_' + req.body.middlename + '_' + req.body.lastname);
                nameArray.push(req.body.lastname + '_' + req.body.middlename + '_' + req.body.firstname);
                nameArray.push(req.body.lastname + '_' + req.body.middlename + '_' + req.body.firstname.charAt(0));
                nameArray.push(req.body.lastname + '_' + req.body.middlename.charAt(0) + '_' + req.body.firstname.charAt(0));
                nameArray.push(req.body.lastname.charAt(0) + '_' + req.body.middlename.charAt(0) + '_' + req.body.firstname);
                nameArray.push(req.body.lastname.charAt(0) + '_' + req.body.middlename + '_' + req.body.firstname);
                nameArray.push(req.body.firstname.charAt(0) + '_' + req.body.middlename.charAt(0) + '_' + req.body.lastname.charAt(0));
                nameArray.push(req.body.firstname.charAt(0) + '_' + req.body.middlename.charAt(0) + '_' + req.body.lastname);
                nameArray.push(req.body.firstname.charAt(0) + '_' + req.body.middlename + '_' + req.body.lastname);
                nameArray.push(req.body.firstname + '_' + req.body.middlename + '_' + req.body.lastname.charAt(0));
                nameArray.push(req.body.firstname + '_' + req.body.middlename.charAt(0) + '_' + req.body.lastname.charAt(0));
            }

            if (req.body.ending){
                nameArray.forEach(function(name){
                    if (validateEmail(name + '@' + req.body.ending)){
                        emailCombos.push(name + '@' + req.body.ending);
                    }
                });
            }
            else {
                var institutionArray = [];

                var affyArray = req.body.organization.split(' ');
                if (affyArray && affyArray[0] && affyArray.length <= 2){
                    institutionArray.push(affyArray[0]);
                    if (affyArray[1]){
                        institutionArray.push(affyArray[0] + affyArray[1]);
                        institutionArray.push(affyArray[0] + '.' + affyArray[1]);
                        institutionArray.push(affyArray[0].charAt(0) + affyArray[1].charAt(0));
                    }
                }
                else if (affyArray && affyArray[0]) {
                    var threeMore = '';
                    var threeMoreDot = '';
                    var abbrev = '';
                    var number = 0;
                    affyArray.forEach(function(oneWord){
                        number += 1;
                        if (number == affyArray.length){
                            threeMore = threeMore + oneWord;
                            abbrev = abbrev + oneWord.charAt(0);
                            threeMoreDot = threeMoreDot + oneWord;
                        }
                        else {
                            threeMore = threeMore + oneWord;
                            abbrev = abbrev + oneWord.charAt(0);
                            threeMoreDot = threeMoreDot + oneWord + '.';
                        }
                    });
                    institutionArray.push(threeMore);
                    institutionArray.push(threeMoreDot);
                    institutionArray.push(abbrev);
                }
                else {
                    institutionArray.push(req.body.organization);
                }
                smallTopLevelDomains.forEach(function(topDomain){
                    institutionArray.forEach(function(institute){
                        nameArray.forEach(function(name){
                            if (validateEmail(name + '@' + institute + topDomain)){
                                emailCombos.push(name + '@' + institute + topDomain);
                            }
                        });
                    });
                });
            }

            var emailed;
            var valided;

            async.each(emailCombos, function (email, complete2) {
                if (req.body.organization){
                    EmailValidated.findOne({$and: [
                        {email : email.toLowerCase()},
                        {organization : req.body.organization.toLowerCase()},
                        {valid : true}
                    ]},function(err,emVal){
                        if (!isEmpty(emVal)){
                            emailed = email;
                            valided = emVal._id;
                            complete2('Email found.');
                        }
                        else {
                            globalValidationQueue.push({email : email, firstname : req.body.firstname, lastname : req.body.lastname, organization : req.body.organization, ending : req.body.ending}, function(email, validObj){
                                if (email){
                                    emailed = email;
                                    valided = validObj;
                                    complete2('Email found.');
                                }
                                else {
                                    complete2();
                                }
                            });
                        }
                    });
                }
                else if (req.body.ending){
                    EmailValidated.findOne({$and: [
                        {email : email.toLowerCase()},
                        {ending : req.body.ending.toLowerCase()},
                        {valid : true}
                    ]},function(err,emVal){
                        if (!isEmpty(emVal)){
                            emailed = email;
                            valided = emVal._id;
                            complete2('Email found.');
                        }
                        else {
                            globalValidationQueue.push({email : email, firstname : req.body.firstname, lastname : req.body.lastname, organization : req.body.organization, ending : req.body.ending}, function(email, validObj){
                                if (email){
                                    emailed = email;
                                    valided = validObj;
                                    complete2('Email found.');
                                }
                                else {
                                    complete2();
                                }
                            });
                        }
                    });
                }
            }, function (err) {
                var searches;
                if (!req.session || !req.session.passport || !req.session.passport.user){
                    searches = 5 - req.session.searches;
                    if (!isNaN(Number(req.session.searches))){
                        req.session.searches += 1;
                    }
                    else {
                        req.session.searches = 1;
                    }
                }
                if (emailed){
                    if (req.session && req.session.passport && req.session.passport.user){
                        User.findById(req.session.passport.user, function(err,user){
                            if (!isEmpty(user)){
                                if (valided){
                                    if (!user.emailsValidated || !user.emailsValidated[0]){
                                        user.emailsValidated.push(valided);
                                    }
                                    else if ($.inArray(valided,user.emailsValidated) < 0){
                                        user.emailsValidated.push(valided);
                                    }
                                }
                                user.save(function(err,used){
                                    res.send({ email : emailed.toLowerCase(), searches : searches});
                                });
                            }
                        });
                    }
                    else {
                        res.send({ email : emailed.toLowerCase(), searches : searches});
                    }
                }
                else {
                    res.send({ error: 'No email found.', searches : searches});
                }
            });
        }
        else {
            res.send({ errored: 'Five free email generations exceeded.'});
        }
    });

    app.io.route('auth', function(req) {
        if (req.session && req.session.passport && req.session.passport.user){
            User.findById(req.session.passport.user, "_id", function (err,doc) {
                if (!isEmpty(doc)){
                    req.io.join(doc._id.toString());
                }
            });
        }
    });

    app.post('/login', function(req, res, next) {passport.authenticate('local', {failureFlash: true}, function(err, user, info) {
        if (err) { return next(err) }
        if (!user) {
            res.send({message : info.message});
        }
        else {
            if (req.session.code){
                InviteCode.findOne({code : req.session.code}, function(err,inv){
                    if (!isEmpty(inv) && !inv.used){
                        User.findOne({email : req.session.email}, '_id subscribed monthFreeStart subscriptionHistories', function(err, used){
                            if (!isEmpty(used) && user._id.toString() == used._id.toString()){
                                inv.used = true;
                                inv.usedDate = new Date();
                                inv.save(function(err,invSave){
                                    used.subscribed = true;
                                    used.monthFreeStart = new Date();
                                    var history = new SubscriptionHistory({date : new Date(), price : 0, currency : 'USD', payMethod : "One Month Free", paymentId : "0000"});
                                    used.subscriptionHistories.push(history);
                                    req.logIn(user, function(err) {
                                        if (err) { return next(err); }
                                        res.send({success : 'success'});
                                    });
                                });
                            }
                            else {
                                req.logIn(user, function(err) {
                                    if (err) { return next(err); }
                                    res.send({success : 'success'});
                                });
                            }
                        });
                    }
                    else {
                        req.logIn(user, function(err) {
                            if (err) { return next(err); }
                            res.send({success : 'success'});
                        });
                    }
                });
            }
            else {
                req.logIn(user, function(err) {
                    if (err) { return next(err); }
                    res.send({success : 'success'});
                });
            }
        }

    })(req, res, next);});

    app.get('/register', function(req, res){
        if (req.session && req.session.passport && req.session.passport.user){
            res.redirect('/');
        }
        else {
            res.render('register');
        }
    });

    app.post('/register', function(req, res){
        if (typeof req.body.email == 'string' && typeof req.body.password == 'string' && validateEmail(req.body.email)){
            var password = shaValue(req.body.password);
            bcrypt.hash(password, 8, function(err, hash) {
                password = hash;
                var email = req.body.email;
                var query = User.find({email:email}, '_id email', function(err, users) {
                    if (!isEmpty(users)){
                        res.send({ error: 'Email already used.' });
                    }
                    else {
                        var verifyCode = crypto.lib.WordArray.random(64);
                        verifyCode = crypto.enc.Base64.stringify(verifyCode);
                        var userAdd = new User({ credits : 10, email: email, password: password, verified: false, verifyCode: verifyCode, joinDate : new Date(), updateDate : new Date(), activeDate : new Date(), organization : req.body.organization, title : req.body.title, salutation : req.body.salutation, firstname : req.body.firstname, lastname : req.body.lastname});
                        if (req.body.email == 'jared@leaddynamo.info'){
                            userAdd.credits = 10000;
                        }
                        userAdd.save(function (errored,useradded) {
                            if (!isEmpty(errored)) {
                                res.send({ error: 'Please forgive the database save error.'});
                            }
                            else {
                                var plainText = domain + '/verifyCode?verifyCode=' + encodeURIComponent(verifyCode);
                                var html = "<a href='" + domain + "/verifyCode?verifyCode=" + encodeURIComponent(verifyCode) + "'>Click here</a> to verify account.";
                                var subject = "Verify Account ✔";
                                sendEmail(req.body.email, plainText, html, subject, adminEmail, domainName, '');
                                if (req.session && req.session.creditCode){
                                    CreditGrant.findOne({code : req.session.creditCode}, function(err,inv){
                                        if (!isEmpty(inv) && !inv.redeemed){
                                            inv.redeemed = true;
                                            inv.dateRedeemed = new Date();
                                            useradded.credits = useradded.credits + inv.amount;
                                            inv.save(function(err,goeth){
                                                useradded.save(function(err,finalAdded){
                                                    req.login(finalAdded, function(err){
                                                        if (!isEmpty(err)){
                                                        }
                                                        else {
                                                            res.redirect(200, '/');
                                                        }
                                                    });
                                                });
                                            });
                                        }
                                        else {
                                            req.login(useradded, function(err){
                                                if (!isEmpty(err)){
                                                }
                                                else {
                                                    res.redirect(200, '/');
                                                }
                                            });
                                        }
                                    });
                                }
                                else {
                                    req.login(useradded, function(err){
                                        if (!isEmpty(err)){
                                        }
                                        else {
                                            res.redirect(200, '/');
                                        }
                                    });
                                }
                            }
                        });
                    }

                });
            });
        }
        else {
            res.send({ error: 'No inputs.'});
        }
    });

    app.get('/doNotTalk',function(req, res){
        User.findById(req.query.userId,function(err,used){
            if (!isEmpty(used)){
                var blast = used.emailBlasts.id(req.query.emailId);
                if (!isEmpty(blast)){
                    if (blast.leads && blast.leads[0] && $.inArray(req.query.leadId, blast.leads) >= 0){
                        if (!used.dontTalk){
                            used.dontTalk = [];
                        }
                        if (used.dontTalk && $.inArray(req.query.leadId, used.dontTalk) < 0){
                            used.dontTalk.push(req.query.leadId);
                            used.save(function(err,use){
                                res.send('Unsubscribed successfully.');
                            });
                        }
                        else {
                            res.send('Already unsubscribed.');
                        }
                    }
                }
            }
        });
    });

    app.get('/verifyCode',function(req, res){
        var verifyQuery = req.query.verifyCode;
        if (req.session.passport && verifyQuery && req.session.passport.user){
            User.findOne({ 'verifyCode': verifyQuery }, '_id verified', function(err, user) {
                if (!isEmpty(user) && req.session.passport.user == user._id){
                    user.verified = true;
                    user.save(function(err, data){
                        res.redirect('/');
                    });
                }
                else {
                    res.redirect('/');
                }
            });
        }
        else {
            res.redirect('/');
        }
    });

    app.get('/creditCode',function(req, res){
        if (req.session.passport && req.query.creditCode && req.session.passport.user){
            CreditGrant.findOne({code : req.query.creditCode},function(err,grant){
                if (!isEmpty(grant) && grant.redeemed){
                    User.findOne({email : grant.emailFrom}, function(err,used){
                        if (!isEmpty(used)){
                            used.credits = used.credits + grant.amount;
                            grant.redeemed = true;
                            grant.dateRedeemed = new Date();
                            grant.save(function(err,granted){
                                used.save(function(err,usedr){
                                    res.redirect('/');
                                });
                            });
                        }
                    });
                }
            });
        }
        else {
            if (req.session){
                req.session.creditCode = req.query.creditCode;
            }
            res.redirect('/register');
        }
    });

    app.get('/logout', function(req, res) {
        if (req.session && req.session.passport && req.session.passport.user){
            User.findById(req.session.passport.user, function(err,user){
                if (!isEmpty(user) && user.blastHistories){
                    user.blastHistories.forEach(function(bla){
                        if (!bla.activated){
                            bla.remove();
                        }
                    });
                    user.save(function(err,used){

                    });
                }
            });
            req.logout();
            res.redirect('/');
        }
    });

    app.get('/twitter',
        passport.authenticate('twitter', { failureRedirect: '/' }));

    app.get('/auth/linkedin/callback', function(req, res, next) {
        passport.authenticate('linkedin', { scope: ['rw_nus'] }, function(err, user, info) {
            res.redirect('/');
        })(req, res, next);
    });

    app.get('/auth/twitter/callback', function(req, res, next) {
        passport.authenticate('twitter', function(err, user, info) {
            res.redirect('/');
        })(req, res, next);
    });

    app.get('/linkedin',
        passport.authenticate('linkedin', { scope: ['rw_nus'] }));

    app.get('/', function(req, res){
        if (req.session && req.session.passport && req.session.passport.user){
            User.findById(req.session.passport.user, '_id blastHistories._id blastHistories.searchArray blastHistories.searchTerms blastHistories.isAnd tweeted linkedin credits name firstname lastname signature email organization title verified', function(err,logged){
                if (!isEmpty(logged)){

                    var flashSearch;
                    var searchTerms;
                    var isAnd;
                    var titles;
                    var abstracts;
                    var affiliations;
                    var authors;
                    var textWords;
                    var global;
                    if (req.session.flashSearch){
                        flashSearch = req.session.flashSearch;

                        if (!isEmpty(logged.blastHistories.id(flashSearch))){
                            var blast = logged.blastHistories.id(flashSearch);

                            searchTerms = blast.searchTerms.toString();

                            if (blast.searchArray){
                                blast.searchArray.forEach(function(search){
                                    if (search == 'Global'){
                                        global = 'Global';
                                    }
                                    if (search == 'Title'){
                                        titles = 'Title';
                                    }
                                    if (search == 'Title/Abstract'){
                                        abstracts = 'Title/Abstract';
                                    }
                                    if (search == 'Affiliation'){
                                        affiliations = 'Affiliation';
                                    }
                                    if (search == 'Author'){
                                        authors = 'Author';
                                    }
                                    if (search == 'Text Word'){
                                        textWords = 'Text Word';
                                    }
                                });

                                isAnd = blast.isAnd;
                            }
                            req.session.flashSearch = '';
                        }
                    }

                    var tweeted = req.session.tweeted;
                    req.session.tweeted = false;
                    var linked = req.session.linkedin;
                    req.session.linkedin = false;
                    res.render('index', {price : price, flashSearch : flashSearch, global : global, textWords : textWords, authors : authors, searchTerms : searchTerms, isAnd : isAnd, titles : titles, abstracts : abstracts, affiliations : affiliations, twitter : logged.tweeted, linkedin : logged.linkedin, tweeted : tweeted, linked : linked, stripeKey : config.stripePublic, credits : logged.credits, name : logged.name, firstname : logged.firstname, lastname : logged.lastname, signature : logged.signature, organization : logged.organization, title : logged.title, id : logged._id.toString(), loggedIn: true, verified: logged.verified, email: logged.email, userId: logged._id.toString()});
                }
            });
        }
        else {
            res.render('index');
        }
    });

    app.post('/stripe', function(req, res){
        if (req.session && req.session.passport && req.session.passport.user){
            User.findById(req.session.passport.user, '_id stripeCustomerId creditHistories credits', function(err,used){
                if (!isEmpty(used)){
                    var stripeToken = req.body.stripeToken;
                    var customered;
                    stripe.customers.create({
                        card: stripeToken,
                        description: used._id.toString()
                    }).then(function(customer) {
                            customered = customer.id;
                            return stripe.charges.create({
                                amount: req.body.amount,
                                currency: "usd",
                                customer: customer.id
                            });
                        }).then(function(charge) {
                            if (charge && charge.id && charge.paid){
                                used.stripeCustomerId = customered;
                                var amount = (Number(req.body.amount) / 100).toFixed(2);
                                var credits = Math.floor((Number(req.body.amount) / 100 / Number(price)));
                                if (used.credits){
                                    used.credits = used.credits + credits;
                                }
                                else {
                                    used.credits = credits;
                                }

                                used.creditHistories.push(new CreditHistory({date : new Date(), price : amount, amount : credits, payMethod : 'Stripe', paymentId : charge.id}));
                                used.save(function(err,used2){
                                    res.redirect('/credits');
                                });
                            }
                        });
                }
            });
        }
    });

    app.get('/login', function(req, res){
        if (req.session && req.session.passport && req.session.passport.user){
            User.findById(req.session.passport.user, '_id', function(err,logged){
                if (!isEmpty(logged)){
                    if (req.session && req.session.creditCode){
                        CreditGrant.findOne({code : req.session.creditCode}, function(err,inv){
                            if (!isEmpty(inv) && !inv.redeemed){
                                inv.redeemed = true;
                                inv.dateRedeemed = new Date();
                                logged.credits = logged.credits + inv.amount;
                                inv.save(function(err,goeth){
                                    logged.save(function(err,finalAdded){
                                        res.redirect('/');
                                    });
                                });
                            }
                            else {
                                res.redirect('/');
                            }
                        });
                    }
                    else {
                        res.redirect('/');
                    }
                }
            });
        }
        else {
            if (req.session){
                var recover = req.session.recover;
                req.session.recover = false;
            }
            res.render('login', {recover : recover});
        }
    });

    app.get('/credits', function(req, res){
        if (req.session && req.session.passport && req.session.passport.user){
            User.findById(req.session.passport.user, '_id tweeted linkedin credits name firstname lastname signature email organization title verified creditHistories', function(err, used){
                if (!isEmpty(used)){
                    var creditHistories = used.creditHistories;
                    if (creditHistories && creditHistories[0]){
                        creditHistories.sort(function(a,b){
                            return new Date(b.date) - new Date(a.date);
                        });
                        creditHistories.forEach(function(aa){
                            aa.formattedDate = moment(aa.date).format("dddd, MMMM Do YYYY");
                            if (aa.price.toFixed(2).length == 3){
                                aa.formattedPrice = '$0' + aa.price.toFixed(2);
                            }
                            else {
                                aa.formattedPrice = '$' + aa.price.toFixed(2);
                            }
                            aa.id = aa._id.toString();
                        });
                    }
                    var finalGrants = [];
                    if (used.email == 'jared@leaddynamo.info'){
                        CreditGrant.find({}, function(err,creditGrants){
                            if (!isEmpty(creditGrants)){
                                creditGrants.forEach(function(grant){
                                    grant = grant.toObject();
                                    grant.date = moment(grant.date).format("dddd, MMMM Do YYYY");
                                    if (grant.dateRedeemed){
                                        grant.dateRedeemed = moment(grant.dateRedeemed).format("dddd, MMMM Do YYYY");
                                    }
                                    else {
                                        grant.dateRedeemed = '';
                                    }
                                    finalGrants.push(grant);
                                });
                                res.render('subscriptions', {creditGrants : finalGrants, admin : true, stripeKey : config.stripePublic, credits : used.credits, creditHistories : creditHistories, twitter : used.tweeted, linkedin : used.linkedin, firstname : used.firstname, lastname : used.lastname, organization : used.organization, title : used.title, id : used._id.toString(), usedIn: true, verified: used.verified, email: used.email, price: config.price, userId: used._id.toString()});
                            }
                        });
                    }
                    else {
                        res.render('subscriptions', {stripeKey : config.stripePublic, credits : used.credits, creditHistories : creditHistories, twitter : used.tweeted, linkedin : used.linkedin, firstname : used.firstname, lastname : used.lastname, organization : used.organization, title : used.title, id : used._id.toString(), usedIn: true, verified: used.verified, email: used.email, price: config.price, userId: used._id.toString()});
                    }
                }
            });
        }
    });

    app.get('/leads', function(req, res){
        if (req.session && req.session.passport && req.session.passport.user){
            User.findById(req.session.passport.user).select('-blastHistories.inactiveLeads').exec(function(err, used){
                if (!isEmpty(used)){
                    var countries = [];
                    var cunto = [];
                    var loded = [];

                    if (used.blastHistories && used.blastHistories[0]){
                        used = used.toObject();
                        used.blastHistories = _.filter(used.blastHistories,function(blast){
                            if (blast.activated){
                                return blast;
                            }
                            else {
                                return false;
                            }
                        });
                        async.map(used.blastHistories, function (item, completor) {
                            if (item.leads && item.leads[0]){
                                var leadStream = Lead.find({_id: { $in: item.leads}}).batchSize(100000).stream();

                                leadStream.on('data', function (doc) {
                                    loded.push(doc.toObject());
                                }).on('error', function (err) {

                                    }).on('close', function () {
                                        completor(null, item);
                                    });
                            }
                            else {
                                completor(null, item);
                            }
                        }, function (err, results) {
                            if (loded && loded[0]){
                                used.blastHistories = _.map(used.blastHistories, function(historied){
                                    if (historied.leadCount && historied.totalLeads){
                                        historied.totalLeadsLeft = historied.totalLeads - historied.leadCount;
                                    }
                                    else {
                                        historied.totalLeadsLeft = 0;
                                    }

                                    historied.totalLeadsBoolean;
                                    if (historied.totalLeadsLeft > 0){
                                        historied.totalLeadsBoolean = true;
                                    }
                                    else {
                                        historied.totalLeadsBoolean = false;
                                    }
                                    if (historied.searchArray && historied.searchArray[0]){
                                        var global;
                                        historied.searchArray.forEach(function(search){
                                            if (search == 'Global'){
                                                global = 'Global';
                                            }
                                        });
                                        if (global){
                                            historied.searchArray = [global];
                                        }
                                    }
                                    historied.formattedDate = moment(historied.date).format("dddd, MMMM Do YYYY");
                                    historied.formattedUpdateDate = moment(historied.updateDate).format("dddd, MMMM Do YYYY");
                                    historied._id = historied._id.toString();
                                    return historied;
                                });

                                if (used.lists && used.lists[0]){
                                    used.lists = _.map(used.lists, function(histObj){
                                        histObj.formattedDate = moment(histObj.date).format("dddd, MMMM Do YYYY");
                                        histObj.formattedUpdateDate = moment(histObj.updateDate).format("dddd, MMMM Do YYYY");
                                        histObj._id = histObj._id.toString();
                                        return histObj;
                                    });
                                }

                                loded = _.map(loded, function(lod){
                                    if (lod.paperDate){
                                        lod.paperDateFormatted = moment(lod.paperDate).format("dddd, MMMM Do YYYY");
                                    }
                                    if (lod.affiliation){
                                        if (!lod.email){
                                            var authorEmailMatch = lod.affiliation.match(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/);
                                            if (authorEmailMatch && authorEmailMatch[0]){
                                                lod.email = authorEmailMatch[0];
                                            }
                                        }
                                        lod = parseCountry(lod);

                                        lod.affiliation = parseAffiliation(lod.affiliation);
                                    }
                                    if (lod.country && cunto.indexOf(lod.country) < 0){
                                        var cco = {};
                                        cco.country = lod.country;
                                        countries.push(cco);
                                        cunto.push(lod.country);
                                    }
                                    return lod;
                                });
                            }
                            res.render('blasts', {countries : countries, twitter : used.tweeted, linkedin : used.linkedin, credits : used.credits, loggedIn : true, verified : used.verified, name: used.name, email : used.email, blasts : JSON.stringify(used.emailBlasts), blastHistoried : JSON.stringify(used.blastHistories), listed : JSON.stringify(used.lists), userId: used._id.toString(), leads: JSON.stringify(loded)});
                        });
                    }
                    else {
                        res.render('blasts', {countries : countries, twitter : used.tweeted, linkedin : used.linkedin, credits : used.credits, loggedIn : true, verified : used.verified, name: used.name, email : used.email, blasts : JSON.stringify(used.emailBlasts), blastHistoried : JSON.stringify(used.blastHistories), listed : JSON.stringify(used.lists), userId: used._id.toString(), leads: JSON.stringify(loded)});
                    }
                }
            });
        }
    });

    app.post('/refresh', function(req, res){
        if (req.session && req.session.passport && req.session.passport.user && req.body.id){
            User.findById(req.session.passport.user, '_id blastHistories._id blastHistories.searchArray blastHistories.searchTerms blastHistories.isAnd', function(err, used){
                if (!isEmpty(used)){
                    if (used.blastHistories && used.blastHistories[0]){
                        if (!isEmpty(used.blastHistories.id(req.body.id))){
                            req.session.flashSearch = req.body.id;
                            res.send({success : 'success'});
                        }
                    }
                }
            });
        }
    });

    app.post('/addToList', function(req, res){
        if (req.session && req.session.passport && req.session.passport.user && req.body.leadId && req.body.listId){
            User.findById(req.session.passport.user).select('_id lists').exec(function(err, used){
                if (!isEmpty(used)){
                    var listed = used.lists.id(req.body.listId);
                    if (isEmpty(listed.leads(req.body.leadId))){
                        listed.leads.push(req.body.leadId);
                    }
                    listed.leadCount = listed.leads.length;
                    used.save(function(err,finUsed){
                        if (!isEmpty(finUsed)){
                            res.send({success : 'success'});
                        }
                    });
                }
            });
        }
    });

    app.post('/removeFromList', function(req, res){
        if (req.session && req.session.passport && req.session.passport.user && req.body.leadId && req.body.listId){
            User.findById(req.session.passport.user).select('_id lists').exec(function(err, used){
                if (!isEmpty(used)){
                    var listed = used.lists.id(req.body.listId);
                    var finalLeads = [];
                    if (listed.leads && listed.leads[0]){
                        listed.leads.forEach(function(ledded){
                            if (ledded != req.body.leadId){
                                finalLeads.push(ledded);
                            }
                        });
                    }
                    listed.leads = finalLeads;
                    listed.leadCount = listed.leads.length;
                    used.save(function(err,finUsed){
                        if (!isEmpty(finUsed)){
                            res.send({success : 'success'});
                        }
                    });
                }
            });
        }
    });

    app.post('/addLists', function(req, res){
        if (req.session && req.session.passport && req.session.passport.user && req.body.name && req.body.leads && req.body.leads[0]){
            User.findById(req.session.passport.user).select('_id lists blastHistories').exec(function(err, used){
                if (!isEmpty(used)){
                    var alreadyIn = [];
                    if (used.blastHistories && used.blastHistories[0]){
                        var includeList = [];
                        used.blastHistories.forEach(function(hist){
                            if (hist.activated && hist.leads && hist.leads[0]){
                                hist.leads.forEach(function(lead){
                                    includeList.push(lead.toString());
                                });
                            }
                        });
                        req.body.leads.forEach(function(lead2){
                            if ($.inArray(lead2.toString(), includeList) >= 0 && $.inArray(lead2.toString(), alreadyIn) < 0){
                                alreadyIn.push(lead2.toString());
                            }
                        });
                    }
                    User.findById(req.session.passport.user, function(err,used){
                        if (!isEmpty(used)){
                            var list = new List({date : new Date(), dateUpdated : new Date(), name : req.body.name});
                            if (alreadyIn && alreadyIn[0]){
                                if (list.leads){

                                }
                                else {
                                    list.leads = [];
                                }
                                alreadyIn.forEach(function(hust){
                                    list.leads.push(hust);
                                });
                            }
                            list.leadCount = list.leads.length;
                            if (list.leads && list.leads[0]){
                                used.lists.push(list);
                                used.save(function(err,useo){
                                    if (!isEmpty(useo)){
                                        var lists3 = useo.lists.id(list._id);
                                        if (!isEmpty(lists3)){
                                            var histObj = lists3.toObject();
                                            Lead.find({_id: { $in: histObj.leads}}).batchSize(100000).exec(function(err,leads){
                                                if (!isEmpty(leads)){
                                                    histObj.leads = leads;
                                                    if (histObj.leads && histObj.leads[0]){
                                                        histObj.leads.forEach(function(led){
                                                            if (led.paperDate){
                                                                led.paperDateFormatted = moment(led.paperDate).format("dddd, MMMM Do YYYY");
                                                            }
                                                            if (led.affiliation){
                                                                if (!led.email){
                                                                    var authorEmailMatch = led.affiliation.match(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/);
                                                                    if (authorEmailMatch && authorEmailMatch[0]){
                                                                        led.email = authorEmailMatch[0];
                                                                    }
                                                                }
                                                                led = parseCountry(led);
                                                                led.affiliation = parseAffiliation(led.affiliation);
                                                            }
                                                        });
                                                    }
                                                    histObj.formattedDate = moment(histObj.date).format("dddd, MMMM Do YYYY");
                                                    histObj.formattedUpdateDate = moment(histObj.updateDate).format("dddd, MMMM Do YYYY");
                                                    histObj._id = histObj._id.toString();
                                                }
                                            });
                                        }

                                        res.send({success : 'success', list : histObj});
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }
    });

    app.post('/editLists', function(req, res){
        if (req.session && req.session.passport && req.session.passport.user && req.body.id && req.body.name && req.body.leads && req.body.leads[0]){
            User.findById(req.session.passport.user).select('_id lists blastHistories').exec(function(err, used){
                if (!isEmpty(used)){
                    if (used.lists && used.lists[0]){
                        if (!isEmpty(used.lists.id(req.body.id))){

                            var lists = used.lists.id(req.body.id);

                            if (!isEmpty(lists)){
                                lists.name = req.body.name;
                                lists.leads = [];
                                used.save(function(err,use2){
                                    if (!isEmpty(use2)){
                                        var lists2 = use2.lists.id(req.body.id);
                                        if (use2.blastHistories && use2.blastHistories[0]){
                                            use2.blastHistories.forEach(function(hist){
                                                if (hist.leads && hist.leads[0] && hist.activated){
                                                    hist.leads.forEach(function(lead){
                                                        req.body.leads.forEach(function(lead2){
                                                            if (lead2 == lead){
                                                                var inAlready = false;
                                                                lists2.leads.forEach(function(led){
                                                                    if (led == lead2){
                                                                        inAlready = true;
                                                                    }
                                                                });
                                                                if (!inAlready){
                                                                    lists2.leads.push(lead);
                                                                }
                                                            }
                                                        });
                                                    });
                                                }
                                            });
                                        }
                                        lists2.leadCount = lists2.leads.length;
                                        use2.save(function(err,finUse){
                                            if (!isEmpty(finUse)){
                                                var lists3 = finUse.lists.id(req.body.id);
                                                if (!isEmpty(lists3)){
                                                    var histObj = lists3.toObject();

                                                    Lead.find({_id: { $in: histObj.leads}}).batchSize(100000).exec(function(err,leads){
                                                        if (!isEmpty(leads)){
                                                            histObj.leads = leads;

                                                            if (histObj.leads && histObj.leads[0]){
                                                                histObj.leads.forEach(function(led){
                                                                    if (led.paperDate){
                                                                        led.paperDateFormatted = moment(led.paperDate).format("dddd, MMMM Do YYYY");
                                                                    }
                                                                    if (led.affiliation){

                                                                        if (!led.email){
                                                                            var authorEmailMatch = led.affiliation.match(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/);
                                                                            if (authorEmailMatch && authorEmailMatch[0]){
                                                                                led.email = authorEmailMatch[0];
                                                                            }
                                                                        }

                                                                        led = parseCountry(led);

                                                                        var affy = parseAffiliation(led.affiliation);

                                                                        led.affiliation = affy;
                                                                    }
                                                                });
                                                            }
                                                            histObj.formattedDate = moment(histObj.date).format("dddd, MMMM Do YYYY");
                                                            histObj.formattedUpdateDate = moment(histObj.updateDate).format("dddd, MMMM Do YYYY");
                                                            histObj._id = histObj._id.toString();
                                                            res.send({success : 'success', list : histObj});
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }
                }
            });
        }
    });

    app.post('/deleteLists', function(req, res){
        if (req.session && req.session.passport && req.session.passport.user && req.body.id){
            User.findById(req.session.passport.user).select('_id lists blastHistories').exec(function(err, used){
                if (!isEmpty(used)){
                    if (used.lists && used.lists[0]){
                        if (!isEmpty(used.lists.id(req.body.id))){
                            used.lists.id(req.body.id).remove();
                            used.save(function(err,finUsed){
                                if (!isEmpty(finUsed)){
                                    res.send({success : 'success', identifier : req.body.id});
                                }
                            });
                        }
                    }
                }
            });
        }
    });

    app.get('/team', function(req, res){
        if (req.session && req.session.passport && req.session.passport.user){
            User.findById(req.session.passport.user, '_id tweeted linkedin credits name firstname lastname signature email organization title verified', function(err, used){
                if (!isEmpty(used)){
                    res.render('about', {twitter : used.tweeted, linkedin : used.linkedin, loggedIn: true, email: used.email, name: used.name, userId: used._id.toString(), credits: used.credits});
                }
            });
        }
        else {
            res.render('about', {loggedIn: false});
        }
    });

    app.get('/terms', function(req, res){
        if (req.session && req.session.passport && req.session.passport.user){
            User.findById(req.session.passport.user, '_id tweeted linkedin credits name firstname lastname signature email organization title verified', function(err, used){
                if (!isEmpty(used)){
                    res.render('terms', {twitter : used.tweeted, linkedin : used.linkedin, loggedIn: true, email: used.email, name: used.name, userId: used._id.toString(), credits: used.credits});
                }
            });
        }
        else {
            res.render('terms');
        }
    });

    app.get('/acceptableUse', function(req, res){
        if (req.session && req.session.passport && req.session.passport.user){
            User.findById(req.session.passport.user, '_id tweeted linkedin credits name firstname lastname signature email organization title verified', function(err, used){
                if (!isEmpty(used)){
                    res.render('acceptableUse', {twitter : used.tweeted, linkedin : used.linkedin, loggedIn: true, email: used.email, name: used.name, userId: used._id.toString(), credits: used.credits});
                }
            });
        }
        else {
            res.render('acceptableUse');
        }
    });

    app.get('/privacy', function(req, res){
        if (req.session && req.session.passport && req.session.passport.user){
            User.findById(req.session.passport.user, '_id tweeted linkedin credits name firstname lastname signature email organization title verified', function(err, used){
                if (!isEmpty(used)){
                    res.render('privacy', {twitter : used.tweeted, linkedin : used.linkedin, loggedIn: true, email: used.email, name: used.name, userId: used._id.toString(), credits: used.credits});
                }
            });
        }
        else {
            res.render('privacy');
        }
    });

    app.post('/checkSubscription', function(req, res){
        if (req.session.passport && req.session.passport.user){
            User.findById(req.session.passport.user, 'paidStart subscribed monthFreeStart', function(err,used){
                if (!isEmpty(used)){
                    var subscribed;
                    if ((((new Date() - used.paidStart) < 2628000000) && used.subscribed) || (((new Date() - used.monthFreeStart) < 2628000000) && used.subscribed)){
                        subscribed = true;
                    }
                    else {
                        subscribed = false;
                    }
                    res.send({subscribed : subscribed});
                }
            });
        }
    });

    app.post('/recovery', function(req, res){
        if (req.body.email){
            User.findOne({email : req.body.email}, '_id email recoverCode', function(err, used){
                if (!isEmpty(used)){
                    var recoverCode = crypto.lib.WordArray.random(64);
                    recoverCode = crypto.enc.Base64.stringify(recoverCode);
                    used.update({'recoverCode' : recoverCode},
                        function(err, updateObject){
                            var plainText = domain + '/recovery?recoverCode="' + recoverCode;
                            var html = '<br><a href="' + domain + '/recovery?recoverCode="' + recoverCode + '>Click here</a> to change password.';
                            var subject = "Change Password âœ”";
                            sendEmail(used, plainText, html, subject, adminEmail, domainName, '');
                            res.send({success : 'success'});
                        }
                    );
                }
                else {
                    res.send({error : 'error'});
                }
            });
        }
        else {
            res.send({error : 'error'});
        }
    });

    app.post('/creditSend', function(req, res){
        if (req.session.passport && req.session.passport.user && req.body.email && !isNaN(Number(req.body.amount))){
            User.findById(req.session.passport.user, '_id email', function(err,used){
                if (!isEmpty(used) && used.email == 'jared@leaddynamo.info'){

                    var newCredit = new CreditGrant({date : new Date(), amount : req.body.amount, emailTo : req.body.email, emailFrom : used.email});

                    var verifyCode = crypto.lib.WordArray.random(64);
                    verifyCode = crypto.enc.Base64.stringify(verifyCode);

                    newCredit.code = verifyCode;

                    newCredit.save(function(err,manned){
                        if (!isEmpty(manned)){
                            var plainText = domain + '/creditCode?creditCode=' + encodeURIComponent(verifyCode);
                            var html = "<a href='" + domain + "/creditCode?creditCode=" + encodeURIComponent(verifyCode) + "'>Click here</a> to get " + req.body.amount.toString() + " bonus credits.";
                            var subject = req.body.amount.toString() + " Bonus LeadDynamo Credits âœ”";
                            sendEmail(req.body.email, plainText, html, subject, adminEmail, domainName, '');
                            res.send({success : 'success'});
                        }
                    });
                }
                else {
                    res.send({success : 'success'});
                }
            });
        }
    });

    app.post('/changePassword', function(req, res){
        if (req.body.password && req.session.recoverCode){
            User.findOne({recoverCode : req.session.recoverCode}, '_id email recoverCode password', function(err, used){
                if (!isEmpty(used)){
                    var password = shaValue(req.body.password);
                    bcrypt.hash(password, 8, function(err, hash) {
                        password = hash;
                        used.password = password;
                        used.save(function(err, usedfinal){
                            req.session.recover = true;
                            res.redirect(200, '/login');
                        });
                    });
                }
            });
        }
    });

    app.get('/recovery', function(req, res){
        if (req.query.recoverCode){
            req.session.recoverCode = req.query.recoverCode;
            res.render('recovery');
        }
    });

    app.get('/profile', function(req, res){
        if (req.session && req.session.passport && req.session.passport.user){
            User.findById(req.session.passport.user, '_id tweeted salutation linkedin credits name firstname lastname signature email organization title verified', function(err, used){
                if (!isEmpty(used)){
                    if (used.signature){
                        used.signature = used.signature.replace(/\r?\n|\r/g, '');
                    }
                    res.render('profile', {twitter : used.tweeted, linkedin : used.linkedin, signature : used.signature, firstname : used.firstname, lastname : used.lastname, salutation : used.salutation, organization : used.organization, title : used.title, email : used.email, credits : used.credits, userId : used._id.toString()});
                }
            });
        }
    });

    app.post('/profile', function(req,res){
        if (req.session && req.session.passport && req.session.passport.user){
            User.findById(req.session.passport.user, '_id password tweeted salutation linkedin credits name firstname lastname signature email organization title verified', function(err, used){
                if (!isEmpty(used)){
                    used.firstname = req.body.firstname;
                    used.lastname = req.body.lastname;
                    used.salutation = req.body.salutation;
                    used.signature = req.body.signature;
                    used.organization = req.body.organization;
                    used.title = req.body.title;
                    if (req.body.password){
                        var password = shaValue(req.body.password);
                        bcrypt.hash(password, 8, function(err, hash) {
                            password = hash;
                            used.password = password;
                            used.save(function(err,uso){
                                res.send({success : 'success'});
                            });
                        });
                    }
                    else {
                        used.save(function(err,uso){
                            res.send({success : 'success'});
                        });
                    }
                }
            });
        }
    });

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    var globalBlastEmailValidationQueue = async.queue(function (task, callback) {

        var email = task.email;
        var emailParse = email.split('@');

        EmailValidated.findOne({$and: [
            { pubMedId : task.item.id },
            { firstname : task.author.firstname },
            { lastname : task.author.lastname },
            { valid : true }
        ]},function(err,emVal){
            if (!isEmpty(emVal)){
                task.author.email = emVal.email;
                callback(task.email, task.author);
            }
            else {
                exec('nslookup -q=mx ' + emailParse[1], function (error, stdout, stderr) {
                    var resed;
                    if (stdout && typeof stdout.toString() == 'string'){
                        resed = stdout.match(/mail addr = .*\n/);

                        if (resed && resed[0]){
                            resed = resed[0].match(/([a-z0-9]+\.)*[a-z0-9-]+\.[a-z]+/i);
                        }
                        else {
                            resed = stdout.match(/mail exchanger = .*\n/);
                        }
                        if (resed && resed[0]){
                            resed = resed[0].match(/([a-z0-9]+\.)*[a-z0-9-]+\.[a-z]+/i);
                        }
                    }
                    if (resed && resed[0]){
                        var valid;

                        var conn = net.connect(25,resed[0],
                            function() {
                                var counter = 0;
                                var before;
                                conn.on("data", function (c) {
                                    counter += 1;
                                    c = c.toString();
                                    if (c.match(/250/) && c.match(/250/)[0] && !before){
                                        before = true;
                                        counter = 1;
                                    }
                                    if (counter == 1 && before){
                                        conn.write("mail from: <" + adminEmail + ">\n");
                                    }
                                    else if (counter == 2){
                                        conn.write("rcpt to: <" + email + ">\n");
                                    }
                                    else if (counter == 3){
                                        if (c.match(/250/) && c.match(/250/)[0]){
                                            valid = true;
                                            task.author.email = email;
                                        }
                                        else {
                                            valid = false;
                                        }
                                        conn.end();
                                    }
                                });
                                conn.on("end", function () {
                                    var validatedObject = new EmailValidated({date : new Date(), email : email, firstname : task.author.firstname, lastname : task.author.lastname, valid : valid, initials : task.author.initials, pubMedId : task.item.id, paper : task.item.title, userSearched : task.user});
                                    validatedObject.save(function(err,validObj){
                                        callback(task.email, task.author);
                                    });
                                });
                                conn.write("helo hi\n");
                            });
                        conn.on('error',function(e){
                            var validatedObject = new EmailValidated({date : new Date(), email : email, firstname : task.author.firstname, lastname : task.author.lastname, valid : valid, initials : task.author.initials, pubMedId : task.item.id, paper : task.item.title, userSearched : task.user});
                            validatedObject.save(function(err,validObj){
                                callback(task.email, task.author);
                            });
                        });
                    }
                    else {
                        callback(task.email, task.author);
                    }
                });
            }
        });
    }, queueLimit);

    Array.prototype.getUnique = function(){
        var u = {}, a = [];
        for(var i = 0, l = this.length; i < l; ++i){
            if(u.hasOwnProperty(this[i])) {
                continue;
            }
            a.push(this[i]);
            u[this[i]] = 1;
        }
        return a;
    }

    var saveQueue = async.queue(function (task, callback) {
        if (task.lead){
            Lead.findOne({$and: [
                { pubMedId : task.nr.id },
                { firstname : task.author.firstname },
                { lastname : task.author.lastname },
                { email : task.author.email }
            ]},function(err,leaded){
                if (!isEmpty(leaded)){
                    callback(null, null);
                }
                else {
                    var newLer = new Lead({affiliation : task.author.affiliation, country : task.author.country, paperDate : task.nr.date, searchTerm : task.nr.searchTerm, title : task.nr.title, pubMedId : task.nr.id, email : task.author.email, phone : task.author.phone, firstname : task.author.firstname, lastname : task.author.lastname, initials : task.author.initials});
                    newLer.save(function(err,bun){
                        callback(null, null);
                    });
                }
            });
        }
        else {
            Paper.findOne({pubMedId : task.nr.id},function(err,pubbed){
                if (isEmpty(pubbed)){
                    var paped = new Paper({pubMedId : task.nr.id, dateAdded : new Date(), type : 'remote', database : 'PubMed', json : JSON.stringify(task.nr.json), leadsParsed : true});
                    paped.save(function(err,paped){
                        callback(null, null);
                    });
                }
            });
        }
    }, 5);

    app.post('/submitBlast', function(req, res){
        User.findById(req.body.userId).select('_id credits blastHistories emails').exec(function(err,logged){
            if (!isEmpty(logged) && req.body.idArray && req.body.idArray[0]){
                if (req.body.searchArray){
                    req.body.searchArray.getUnique();
                }
//                var allEmailCombinations = [];
//                req.body.results.forEach(function(paper){
//                    if (paper.authors && paper.authors[0]){
//                        paper.authors.forEach(function(author){
//                            if (author.affiliation){
//                                var authorEmailMatch = author.affiliation.match(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/);
//                                if (authorEmailMatch && authorEmailMatch[0]){
//                                    author.email = authorEmailMatch[0];
//                                }
//                                else {
////                                        var institutionArray = [];
////
////                                        var nameArray = [];
////
////                                        if (author.firstname && author.lastname){
////                                            nameArray.push(author.firstname.charAt(0) + author.lastname);
////                                            nameArray.push(author.firstname.charAt(0) + author.firstname.charAt(1) + author.lastname);
////                                        }
////                                        if (author.firstname){
////                                            nameArray.push(author.firstname);
////                                        }
////                                        if (author.lastname){
////                                            nameArray.push(author.lastname);
////                                        }
////
////                                        var affiliationArray = author.affiliation.split(', ');
////
////                                        if (affiliationArray && affiliationArray[0]){
////                                            affiliationArray.forEach(function(affy){
////                                                var affyArray = affy.split(' ');
////                                                if (affyArray && affyArray[0] && affyArray.length <= 2){
////                                                    institutionArray.push(affyArray[0]);
////                                                    if (affyArray[1]){
////                                                        institutionArray.push(affyArray[0] + affyArray[1]);
////                                                        institutionArray.push(affyArray[0] + '.' + affyArray[1]);
////                                                        institutionArray.push(affyArray[0].charAt(0) + affyArray[1].charAt(0));
////                                                    }
////                                                }
////                                                else if (affyArray && affyArray[0]) {
////                                                    var threeMore = '';
////                                                    var threeMoreDot = '';
////                                                    var abbrev = '';
////                                                    var number = 0;
////                                                    affyArray.forEach(function(oneWord){
////                                                        number += 1;
////                                                        if (number == affyArray.length){
////                                                            threeMore = threeMore + oneWord;
////                                                            abbrev = abbrev + oneWord.charAt(0);
////                                                            threeMoreDot = threeMoreDot + oneWord;
////                                                        }
////                                                        else {
////                                                            threeMore = threeMore + oneWord;
////                                                            abbrev = abbrev + oneWord.charAt(0);
////                                                            threeMoreDot = threeMoreDot + oneWord + '.';
////                                                        }
////                                                    });
////                                                    institutionArray.push(threeMore);
////                                                    institutionArray.push(threeMoreDot);
////                                                    institutionArray.push(abbrev);
////                                                }
////                                            });
////                                        }
////                                        else if (affiliationArray){
////                                            var affyArray = affiliationArray.split(' ');
////                                            if (affyArray && affyArray[0] && affyArray.length <= 2){
////                                                institutionArray.push(affyArray[0]);
////                                                if (affyArray[1]){
////                                                    institutionArray.push(affyArray[0] + affyArray[1]);
////                                                    institutionArray.push(affyArray[0] + '.' + affyArray[1]);
////                                                    institutionArray.push(affyArray[0].charAt(0) + affyArray[1].charAt(0));
////                                                }
////                                            }
////                                            else if (affyArray && affyArray[0]) {
////                                                var threeMore = '';
////                                                var threeMoreDot = '';
////                                                var abbrev = '';
////                                                var number = 0;
////                                                affyArray.forEach(function(oneWord){
////                                                    number += 1;
////                                                    if (number == affyArray.length){
////                                                        threeMore = threeMore + oneWord;
////                                                        abbrev = abbrev + oneWord.charAt(0);
////                                                        threeMoreDot = threeMoreDot + oneWord;
////                                                    }
////                                                    else {
////                                                        threeMore = threeMore + oneWord;
////                                                        abbrev = abbrev + oneWord.charAt(0);
////                                                        threeMoreDot = threeMoreDot + oneWord + '.';
////                                                    }
////                                                });
////                                                institutionArray.push(threeMore);
////                                                institutionArray.push(threeMoreDot);
////                                                institutionArray.push(abbrev);
////                                            }
////                                            else if (affyArray){
////                                                institutionArray.push(affyArray);
////                                            }
////                                        }
////
////                                        author.emails = [];
////                                        author.validatedEmails = [];
////
////                                        institutionArray.forEach(function(institute){
////                                            nameArray.forEach(function(name){
////                                                topLevelDomains.forEach(function(topDomain){
////                                                    if (validateEmail(name + '@' + institute + topDomain)){
////                                                        author.emails.push(name + '@' + institute + topDomain);
////                                                        allEmailCombinations.push(name + '@' + institute + topDomain);
////                                                    }
////                                                });
////                                            });
////                                        });
//                                }
//                            }
//                            else {
//
//                            }
//                        });
//                    }
//                });
//
//                EmailValidated.find({'email': { $in: allEmailCombinations}},function(err, validatedEmails){
//                    if (!isEmpty(validatedEmails) && validatedEmails[0]){
//                        validatedEmails.forEach(function(email){
//                            req.body.results.forEach(function(paper){
//                                if (paper.authors){
//                                    paper.authors.forEach(function(author){
//                                        if (author.emails && author.emails[0]){
//                                            author.emails.some(function(authorEmail){
//                                                if (authorEmail == email){
//                                                    // TODO : if checked within 3 months valid
//                                                    if (email.valid){
//                                                        for (var i=author.emails.length-1; i>=0; i--) {
//                                                            if (author.emails[i] === email) {
//                                                                author.emails.splice(i, 1);
//                                                            }
//                                                        }
//                                                        author.email = email;
//                                                        return authorEmail === 1;
//                                                    }
//                                                    else {
//                                                        for (var i=author.emails.length-1; i>=0; i--) {
//                                                            if (author.emails[i] === email) {
//                                                                author.emails.splice(i, 1);
//                                                            }
//                                                        }
//                                                    }
//                                                }
//                                            });
//                                        }
//                                    });
//                                }
//                            });
//                        });
//                    }

                var ids = req.body.idArray;

                var finalIded = ids;

                var finalResults = [];

                var totalFound = 0;

                var arrayArray1 = [];

                var i,j,temparray,chunk = 500;
                for (i=0,j=ids.length; i<j; i+=chunk) {
                    temparray = ids.slice(i,i+chunk);
                    arrayArray1.push(temparray);
                }

                var loded = [];
                var papers = [];

                async.map(arrayArray1, function (item, completor) {
                    var leadStream = Lead.find({'pubMedId': { $in: item}, 'email': { $exists: true }}).batchSize(100000).stream();

                    leadStream.on('data', function (doc) {
                        loded.push(doc.toObject());
                    }).on('error', function (err) {

                    }).on('close', function () {
                        completor(null, item);
                    });
                }, function (err, results) {
                    async.map(arrayArray1, function (item, completor2) {
                        var paperStream = Paper.find({'pubMedId': { $in: item}}).select('pubMedId').batchSize(100000).stream();

                        paperStream.on('data', function (doc) {
                            papers.push(doc.toObject());
                        }).on('error', function (err) {

                        }).on('close', function () {
                            completor2(null, item);
                        });
                    }, function (err, results) {
                        if (!isEmpty(papers)){
                            papers.forEach(function(pap){
                                var paperLead = [];
                                var newResults;
                                if (loded && loded[0]){
                                    loded.forEach(function(led){
                                        if (led.pubMedId == pap.pubMedId){
                                            paperLead.push(led);
                                        }
                                    });
                                }

                                var paper = {};
                                paper.authors = paperLead;
                                paper.id = pap.pubMedId;
                                paper.date = pap.date;
                                paper.title = pap.title;

                                finalResults.push(paper);
                                totalFound += 1;
                                ids.splice(ids.indexOf(pap.pubMedId), 1);
                            });
                        }
                        if (ids && ids[0]){
                            var arrayArray = [];

                            var i,j,temparray,chunk = 500;
                            for (i=0,j=ids.length; i<j; i+=chunk) {
                                temparray = ids.slice(i,i+chunk);
                                arrayArray.push(temparray);
                            }

                            async.map(arrayArray, function (item, complete) {
                                request.post({
                                    headers: {
                                        "content-type" : "application/json",
                                        "Accept": "*/*"
                                    },
                                    url: "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&retmode=text&rettype=xml&id=" + item.toString(),
                                    body: JSON.stringify({id : item.toString()})
                                }, function (error, response, body) {
                                    if (body){
                                        parseString(body, function (err, bodied) {
                                            if (bodied){
                                                var newResults = parseResults(bodied, ids);
                                                var itemNum = 1;
                                                if (newResults && newResults[0]){
                                                    newResults.forEach(function(nr){
                                                        finalResults.push(nr);
                                                        if (nr.authors){
                                                            nr.authors = _.map(nr.authors, function (author) {
                                                                if (author.affiliation){
                                                                    if (!author.email){
                                                                        var authorEmailMatch = author.affiliation.match(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/);
                                                                        if (authorEmailMatch && authorEmailMatch[0]){
                                                                            author.email = authorEmailMatch[0];
                                                                        }
                                                                    }
                                                                    if (!author.country){
                                                                        author = parseCountry(author);
                                                                    }
                                                                }
                                                                return author;
                                                            });
                                                            nr.authors.forEach(function(author){
                                                                saveQueue.push({nr : nr, author : author, lead : true}, function(email, authored){

                                                                });
                                                            });
                                                        }
                                                        saveQueue.push({nr : nr}, function(email, authored){

                                                        });
                                                        itemNum += 2;
                                                    });
                                                    totalFound +=  newResults.length;
                                                }
                                                complete(null, item);
                                            }
                                            else {
                                                complete(null, item);
                                            }
                                        });
                                    }
                                    else {
                                        complete(null, item);
                                    }
                                });
                            }, function (err, results) {

                            });
                            async.map(finalResults, function (item, complete) {
                                if (item.authors && item.authors[0]){
                                    async.map(item.authors, function (author, complete3) {
                                        if (author.affiliation){
                                            if (!author.email){
                                                var authorEmailMatch = author.affiliation.match(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/);
                                                if (authorEmailMatch && authorEmailMatch[0]){
                                                    author.email = authorEmailMatch[0];
                                                }
                                            }
                                            if (!author.country){
                                                author = parseCountry(author);
                                            }
                                            if (author.email){
                                                complete3(null, author);
                                            }
                                            else if (author.emails && author.emails[0]){
                                                async.map(author.emails, function (email, complete2) {
                                                    globalBlastEmailValidationQueue.push({email : email, item : item, user : req.session.passport.user, author : author}, function(email, authored){
                                                        author.email = authored.email;
                                                        complete2(null, email);
                                                    });
                                                }, function (err, results) {
                                                    complete3(null, author);
                                                });
                                            }
                                            else {
                                                complete3(null, author);
                                            }
                                        }
                                        else {
                                            complete3(null, author);
                                        }
                                    }, function (err, results) {
                                        complete(null, item);
                                    });
                                }
                                else {
                                    complete(null, item);
                                }
                            }, function (err, results) {
                                if (results){
                                    var currentAuthorCount = 0;
                                    var totalAuthorCount = 0;
                                    results.forEach(function(paper){
                                        if (paper.authors && paper.authors[0]){
                                            paper.authors.forEach(function(author){
                                                if (author.email){
                                                    totalAuthorCount += 1;
                                                }
                                            });
                                        }
                                    });

                                    var history;
                                    var alreadyHistory;
                                    if (logged.blastHistories && logged.blastHistories[0]){
                                        logged.blastHistories.forEach(function(historied){
                                            if ((historied.searchTerms.sort().toString() == req.body.searchTerms.sort().toString()) && (historied.searchArray.sort().toString() == req.body.searchArray.sort().toString()) && (req.body.isAnd.toString() == historied.isAnd.toString())){
                                                historied.totalLeads = totalAuthorCount;
                                                historied.dateUpdated = new Date();
                                                alreadyHistory = historied;
                                            }
                                        });
                                    }

                                    if (req.body.id){
                                        if (logged.blastHistories && logged.blastHistories[0]){
                                            history = logged.blastHistories.id(req.body.id);
                                            history.totalLeads = totalAuthorCount;
                                            history.dateUpdated = new Date();
                                        }
                                    }
                                    else if (alreadyHistory){
                                        alreadyHistory.inactiveLeads = [];
                                        history = alreadyHistory;
                                    }
                                    else {
                                        history = new BlastHistory({totalLeads : totalAuthorCount, date : new Date(), leadsParsed : true, dateUpdated : new Date(), searchTerms : req.body.searchTerms, searchArray : req.body.searchArray, isAnd : req.body.isAnd});
                                    }
                                    history.endDate = req.body.endDate;
                                    history.startDate = req.body.startDate;
                                    if (history.pubMedIds && history.pubMedIds[0]){
                                        req.body.idArray.forEach(function(idOr){
                                            if ($.inArray(idOr, history.pubMedIds) < 0){
                                                history.pubMedIds.push(idOr);
                                            }
                                        });
                                    }
                                    else {
                                        history.pubMedIds = finalIded;
                                    }
                                    if (history && !isEmpty(history)){
                                        var newLeadz = [];

                                        results.forEach(function(paper){
                                            paper.authors.forEach(function(author){
                                                var notInLeads = true;
                                                if (logged.blastHistories && logged.blastHistories[0]){
                                                    logged.blastHistories.forEach(function(hist){
                                                        if (hist.activated && hist.leads && hist.leads[0]){
                                                            logged.emails.forEach(function(led){
                                                                if (author.email == led){
                                                                    notInLeads = false;
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                                if (author.email && notInLeads){
                                                    var inInactive = false;
                                                    if (history.inactiveLeads && history.inactiveLeads[0]){
                                                        history.inactiveLeads.forEach(function(emo){
                                                            if (author._id && emo.toString() == author._id.toString()){
                                                                inInactive = true;
                                                            }
                                                        });
                                                    }
                                                    if (logged.emails){
                                                        logged.emails.forEach(function(emo){
                                                            if (author.email && emo == author.email){
                                                                inInactive = true;
                                                            }
                                                        });
                                                    }
                                                    if (newLeadz && newLeadz[0]){
                                                        newLeadz.forEach(function(emo){
                                                            if (author.email && emo.author.email == author.email){
                                                                inInactive = true;
                                                            }
                                                        });
                                                    }
                                                    if (!inInactive && author.email){
                                                        if (author._id){
                                                            newLeadz.push({type : 'in', author : author, pubMedId : paper.id});
                                                        }
                                                        else{
                                                            newLeadz.push({type : 'out', author : {affiliation : author.affiliation, country : author.country, paperDate : paper.date, searchTerm : paper.searchTerm, title : paper.title, pubMedId : paper.id, email : author.email, phone : author.phone, firstname : author.firstname, lastname : author.lastname, initials : author.initials}});
                                                        }
                                                    }
                                                }
                                            });
                                        });

                                        if (newLeadz && newLeadz[0]){
                                            async.map(newLeadz, function (item, complete4) {
                                                if (item.type == 'in'){
                                                    history.inactiveLeads.push(item.author._id);
                                                    complete4(null,item);
                                                }
                                                else {
                                                    Lead.findOne({$and: [
                                                        { pubMedId : item.pubMedId },
                                                        { firstname : item.author.firstname },
                                                        { lastname : item.author.lastname },
                                                        { email : item.author.email }
                                                    ]},function(err,leaded){
                                                        if (!isEmpty(leaded)){
                                                            history.inactiveLeads.push(leaded._id);
                                                            complete4(null,leaded);
                                                        }
                                                        else {
                                                            var newLer = new Lead(item.author);
                                                            newLer.save(function(err,bun){
                                                                if (!isEmpty(bun)){
                                                                    history.inactiveLeads.push(bun._id);
                                                                    complete4(null,bun);
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            }, function(err,results){

                                                if (isEmpty(logged.blastHistories.id(history._id))){
                                                    logged.blastHistories.push(history);
                                                }

                                                logged.save(function(err,loggedFinal){
                                                    if (!isEmpty(loggedFinal)){

                                                        var inactiveLeadArray = [];
                                                        var historied = loggedFinal.blastHistories.id(history._id);

                                                        var lod = _.filter(loded, function(num){
                                                            if ($.inArray(num._id, historied.inactiveLeads) >= 0){
                                                                return num;
                                                            }
                                                            else {
                                                                return false;
                                                            }
                                                        });

                                                        if (lod && lod[0]){
                                                            if (!historied.leads || !historied.leads[0]){
                                                                historied.leads = [];
                                                            }

                                                            var emailArray = [];

                                                            lod.forEach(function(inactiveLead){
                                                                if (inactiveLead.affiliation){
                                                                    currentAuthorCount += 1;
                                                                    var leadPack = {};
                                                                    leadPack._id = inactiveLead._id.toString();
                                                                    leadPack.firstname = inactiveLead.firstname;
                                                                    leadPack.lastname = inactiveLead.lastname;
                                                                    leadPack.country = inactiveLead.country;
                                                                    var pushIn = false;
                                                                    if ($.inArray(inactiveLead.email, emailArray) >= 0){
                                                                        pushIn = true;
                                                                    }
                                                                    emailArray.push(inactiveLead.email);
                                                                    leadPack.affiliation = inactiveLead.affiliation;
                                                                    if (inactiveLead.institution && inactiveLead.department){
                                                                        leadPack.affiliation = inactiveLead.department + ', ' + inactiveLead.institution;
                                                                    }
                                                                    else if (inactiveLead.institution){
                                                                        leadPack.affiliation = inactiveLead.institution;
                                                                    }
                                                                    else if (inactiveLead.affiliation && inactiveLead.affiliation.length <= 6){
                                                                        leadPack.affiliation = '-';
                                                                    }
                                                                    leadPack.affiliation = parseAffiliation(leadPack.affiliation);
                                                                    if (!pushIn){
                                                                        inactiveLeadArray.push(leadPack);
                                                                    }
                                                                }
                                                            });
                                                        }
                                                        if (inactiveLeadArray.length == 0){
                                                            res.send({message : 'No leads found.', papers : finalResults.length});
                                                        }
                                                        else if (inactiveLeadArray.length > loggedFinal.credits){
                                                            res.send({success : 'Need more credits for full leads.', leads : inactiveLeadArray, identifier: historied._id.toString(), total : inactiveLeadArray.length, credits : loggedFinal.credits, difference : (inactiveLeadArray.length - loggedFinal.credits), price : Number(config.price), priceDifference : ((Number(config.price) * (inactiveLeadArray.length - loggedFinal.credits)) + Number(config.price))});
                                                        }
                                                        else {
                                                            res.send({success : 'Credits available for all leads.', leads : inactiveLeadArray, credits: loggedFinal.credits, identifier: historied._id.toString(), total : inactiveLeadArray.length});
                                                        }
                                                    }
                                                });
                                            });
                                        }
                                        else {

                                            if (isEmpty(logged.blastHistories.id(history._id))){
                                                logged.blastHistories.push(history);
                                            }

                                            logged.save(function(err,loggedFinal){
                                                if (!isEmpty(loggedFinal)){

                                                    var inactiveLeadArray = [];
                                                    var historied = loggedFinal.blastHistories.id(history._id);

                                                    var lod = _.filter(loded, function(num){
                                                        if ($.inArray(num._id, historied.inactiveLeads) >= 0){
                                                            return num;
                                                        }
                                                        else {
                                                            return false;
                                                        }
                                                    });

                                                    if (lod && lod[0]){
                                                        if (!historied.leads || !historied.leads[0]){
                                                            historied.leads = [];
                                                        }

                                                        var emailArray = [];

                                                        lod.forEach(function(inactiveLead){
                                                            if (inactiveLead.affiliation){
                                                                currentAuthorCount += 1;
                                                                var leadPack = {};
                                                                leadPack._id = inactiveLead._id.toString();
                                                                leadPack.firstname = inactiveLead.firstname;
                                                                leadPack.lastname = inactiveLead.lastname;
                                                                leadPack.country = inactiveLead.country;
                                                                var pushIn = false;
                                                                if ($.inArray(inactiveLead.email, emailArray) >= 0){
                                                                    pushIn = true;
                                                                }
                                                                emailArray.push(inactiveLead.email);
                                                                leadPack.affiliation = inactiveLead.affiliation;
                                                                if (inactiveLead.institution && inactiveLead.department){
                                                                    leadPack.affiliation = inactiveLead.department + ', ' + inactiveLead.institution;
                                                                }
                                                                else if (inactiveLead.institution){
                                                                    leadPack.affiliation = inactiveLead.institution;
                                                                }
                                                                else if (inactiveLead.affiliation && inactiveLead.affiliation.length <= 6){
                                                                    leadPack.affiliation = '-';
                                                                }
                                                                leadPack.affiliation = parseAffiliation(leadPack.affiliation);
                                                                if (!pushIn){
                                                                    inactiveLeadArray.push(leadPack);
                                                                }
                                                            }
                                                        });
                                                    }
                                                    if (inactiveLeadArray.length == 0){
                                                        res.send({message : 'No leads found.', papers : finalResults.length});
                                                    }
                                                    else if (inactiveLeadArray.length > loggedFinal.credits){
                                                        res.send({success : 'Need more credits for full leads.', leads : inactiveLeadArray, identifier: historied._id.toString(), total : inactiveLeadArray.length, credits : loggedFinal.credits, difference : (inactiveLeadArray.length - loggedFinal.credits), price : Number(config.price), priceDifference : ((Number(config.price) * (inactiveLeadArray.length - loggedFinal.credits)) + Number(config.price))});
                                                    }
                                                    else {
                                                        res.send({success : 'Credits available for all leads.', leads : inactiveLeadArray, credits: loggedFinal.credits, identifier: historied._id.toString(), total : inactiveLeadArray.length});
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            });
                        }
                        else {
                            async.map(finalResults, function (item, complete) {
                                if (item.authors && item.authors[0]){
                                    async.map(item.authors, function (author, complete3) {
                                        if (author.affiliation){
                                            if (!author.email){
                                                var authorEmailMatch = author.affiliation.match(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/);
                                                if (authorEmailMatch && authorEmailMatch[0]){
                                                    author.email = authorEmailMatch[0];
                                                }
                                            }

                                            if (!author.country){
                                                author = parseCountry(author);
                                            }
                                            if (author.email){
                                                complete3(null, author);
                                            }
                                            else if (author.emails && author.emails[0]){
                                                async.map(author.emails, function (email, complete2) {
                                                    globalBlastEmailValidationQueue.push({email : email, item : item, user : req.session.passport.user, author : author}, function(email, authored){
                                                        author.email = authored.email;
                                                        complete2(null, email);
                                                    });

                                                }, function (err, results) {
                                                    complete3(null, author);
                                                });
                                            }
                                            else {
                                                complete3(null, author);
                                            }
                                        }
                                        else {
                                            complete3(null, author);
                                        }
                                    }, function (err, results) {
                                        complete(null, item);
                                    });
                                }
                                else {
                                    complete(null, item);
                                }
                            }, function (err, results) {
                                if (results){
                                    var currentAuthorCount = 0;
                                    var totalAuthorCount = 0;
                                    results.forEach(function(paper){
                                        if (paper.authors && paper.authors[0]){
                                            paper.authors.forEach(function(author){
                                                if (author.email){
                                                    totalAuthorCount += 1;
                                                }
                                            });
                                        }
                                    });

                                    var history;
                                    var alreadyHistory;
                                    if (logged.blastHistories && logged.blastHistories[0] && req.body.searchTerms){
                                        logged.blastHistories.forEach(function(historied){
                                            if ((historied.searchTerms.sort().toString() == req.body.searchTerms.sort().toString()) && (historied.searchArray.sort().toString() == req.body.searchArray.sort().toString()) && (req.body.isAnd.toString() == historied.isAnd.toString())){
                                                historied.totalLeads = totalAuthorCount;
                                                historied.dateUpdated = new Date();
                                                alreadyHistory = historied;
                                            }
                                        });
                                    }

                                    if (req.body.id){
                                        if (logged.blastHistories && logged.blastHistories[0]){
                                            history = logged.blastHistories.id(req.body.id);
                                            history.totalLeads = totalAuthorCount;
                                            history.dateUpdated = new Date();
                                        }
                                    }
                                    else if (alreadyHistory){
                                        alreadyHistory.inactiveLeads = [];
                                        history = alreadyHistory;
                                    }
                                    else {
                                        history = new BlastHistory({totalLeads : totalAuthorCount, date : new Date(), leadsParsed : true, dateUpdated : new Date(), searchTerms : req.body.searchTerms, searchArray : req.body.searchArray, isAnd : req.body.isAnd});
                                    }
                                    history.endDate = req.body.endDate;
                                    history.startDate = req.body.startDate;
                                    if (history.pubMedIds && history.pubMedIds[0]){
                                        req.body.idArray.forEach(function(idOr){
                                            if ($.inArray(idOr, history.pubMedIds) < 0){
                                                history.pubMedIds.push(idOr);
                                            }
                                        });
                                    }
                                    else {
                                        history.pubMedIds = finalIded;
                                    }
                                    if (history && !isEmpty(history)){
                                        var newLeadz = [];

                                        results.forEach(function(paper){
                                            paper.authors.forEach(function(author){
                                                var notInLeads = true;
                                                if (logged.blastHistories && logged.blastHistories[0]){
                                                    logged.blastHistories.forEach(function(hist){
                                                        if (hist.activated && hist.leads && hist.leads[0]){
                                                            logged.emails.forEach(function(led){
                                                                if (author.email == led){
                                                                    notInLeads = false;
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                                if (author.email && notInLeads){
                                                    var inInactive = false;
                                                    if (history.inactiveLeads && history.inactiveLeads[0]){
                                                        history.inactiveLeads.forEach(function(emo){
                                                            if (author._id && emo.toString() == author._id.toString()){
                                                                inInactive = true;
                                                            }
                                                        });
                                                    }
                                                    if (logged.emails){
                                                        logged.emails.forEach(function(emo){
                                                            if (author.email && emo == author.email){
                                                                inInactive = true;
                                                            }
                                                        });
                                                    }
                                                    if (newLeadz && newLeadz[0]){
                                                        newLeadz.forEach(function(emo){
                                                            if (author.email && emo.author.email == author.email){
                                                                inInactive = true;
                                                            }
                                                        });
                                                    }
                                                    if (!inInactive && author.email){
                                                        if (author._id){
                                                            newLeadz.push({type : 'in', author : author, pubMedId : paper.id});
                                                        }
                                                        else{
                                                            newLeadz.push({type : 'out', author : {affiliation : author.affiliation, country : author.country, paperDate : paper.date, searchTerm : paper.searchTerm, title : paper.title, pubMedId : paper.id, email : author.email, phone : author.phone, firstname : author.firstname, lastname : author.lastname, initials : author.initials}});
                                                        }
                                                    }
                                                }
                                            });
                                        });

                                        if (newLeadz && newLeadz[0]){
                                            async.map(newLeadz, function (item, complete4) {
                                                if (item.type == 'in'){
                                                    history.inactiveLeads.push(item.author._id);
                                                    complete4(null,item);
                                                }
                                                else {
                                                    Lead.findOne({$and: [
                                                        { pubMedId : item.pubMedId },
                                                        { firstname : item.author.firstname },
                                                        { lastname : item.author.lastname },
                                                        { email : item.author.email }
                                                    ]},function(err,leaded){
                                                        if (!isEmpty(leaded)){
                                                            history.inactiveLeads.push(leaded._id);
                                                            complete4(null,leaded);
                                                        }
                                                        else {
                                                            var newLer = new Lead(item.author);
                                                            newLer.save(function(err,bun){
                                                                if (!isEmpty(bun)){
                                                                    history.inactiveLeads.push(bun._id);
                                                                    complete4(null,bun);
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            }, function(err,results){

                                                if (isEmpty(logged.blastHistories.id(history._id))){
                                                    logged.blastHistories.push(history);
                                                }

                                                logged.save(function(err,loggedFinal){
                                                    if (!isEmpty(loggedFinal)){

                                                        var inactiveLeadArray = [];
                                                        var historied = loggedFinal.blastHistories.id(history._id);

                                                        var lod = _.filter(loded, function(num){
                                                            if ($.inArray(num._id, historied.inactiveLeads) >= 0){
                                                                return num;
                                                            }
                                                            else {
                                                                return false;
                                                            }
                                                        });

                                                        if (lod && lod[0]){
                                                            if (!historied.leads || !historied.leads[0]){
                                                                historied.leads = [];
                                                            }

                                                            var emailArray = [];

                                                            lod.forEach(function(inactiveLead){
                                                                if (inactiveLead.affiliation){
                                                                    currentAuthorCount += 1;
                                                                    var leadPack = {};
                                                                    leadPack._id = inactiveLead._id.toString();
                                                                    leadPack.firstname = inactiveLead.firstname;
                                                                    leadPack.lastname = inactiveLead.lastname;
                                                                    leadPack.country = inactiveLead.country;
                                                                    var pushIn = false;
                                                                    if ($.inArray(inactiveLead.email, emailArray) >= 0){
                                                                        pushIn = true;
                                                                    }
                                                                    emailArray.push(inactiveLead.email);
                                                                    leadPack.affiliation = inactiveLead.affiliation;
                                                                    if (inactiveLead.institution && inactiveLead.department){
                                                                        leadPack.affiliation = inactiveLead.department + ', ' + inactiveLead.institution;
                                                                    }
                                                                    else if (inactiveLead.institution){
                                                                        leadPack.affiliation = inactiveLead.institution;
                                                                    }
                                                                    else if (inactiveLead.affiliation && inactiveLead.affiliation.length <= 6){
                                                                        leadPack.affiliation = '-';
                                                                    }
                                                                    leadPack.affiliation = parseAffiliation(leadPack.affiliation);
                                                                    if (!pushIn){
                                                                        inactiveLeadArray.push(leadPack);
                                                                    }
                                                                }
                                                            });
                                                        }
                                                        if (inactiveLeadArray.length == 0){
                                                            res.send({message : 'No leads found.', papers : finalResults.length});
                                                        }
                                                        else if (inactiveLeadArray.length > loggedFinal.credits){
                                                            res.send({success : 'Need more credits for full leads.', leads : inactiveLeadArray, identifier: historied._id.toString(), total : inactiveLeadArray.length, credits : loggedFinal.credits, difference : (inactiveLeadArray.length - loggedFinal.credits), price : Number(config.price), priceDifference : ((Number(config.price) * (inactiveLeadArray.length - loggedFinal.credits)) + Number(config.price))});
                                                        }
                                                        else {
                                                            res.send({success : 'Credits available for all leads.', leads : inactiveLeadArray, credits: loggedFinal.credits, identifier: historied._id.toString(), total : inactiveLeadArray.length});
                                                        }
                                                    }
                                                });
                                            });
                                        }
                                        else {
                                            if (isEmpty(logged.blastHistories.id(history._id))){
                                                logged.blastHistories.push(history);
                                            }
                                            logged.save(function(err,loggedFinal){
                                                if (!isEmpty(loggedFinal)){

                                                    var inactiveLeadArray = [];
                                                    var historied = loggedFinal.blastHistories.id(history._id);

                                                    var lod = _.filter(loded, function(num){
                                                        if ($.inArray(num._id, historied.inactiveLeads) >= 0){
                                                            return num;
                                                        }
                                                        else {
                                                            return false;
                                                        }
                                                    });

                                                    if (lod && lod[0]){
                                                        if (!historied.leads || !historied.leads[0]){
                                                            historied.leads = [];
                                                        }

                                                        var emailArray = [];

                                                        lod.forEach(function(inactiveLead){
                                                            if (inactiveLead.affiliation){
                                                                currentAuthorCount += 1;
                                                                var leadPack = {};
                                                                leadPack._id = inactiveLead._id.toString();
                                                                leadPack.firstname = inactiveLead.firstname;
                                                                leadPack.lastname = inactiveLead.lastname;
                                                                leadPack.country = inactiveLead.country;
                                                                var pushIn = false;
                                                                if ($.inArray(inactiveLead.email, emailArray) >= 0){
                                                                    pushIn = true;
                                                                }
                                                                emailArray.push(inactiveLead.email);
                                                                leadPack.affiliation = inactiveLead.affiliation;
                                                                if (inactiveLead.institution && inactiveLead.department){
                                                                    leadPack.affiliation = inactiveLead.department + ', ' + inactiveLead.institution;
                                                                }
                                                                else if (inactiveLead.institution){
                                                                    leadPack.affiliation = inactiveLead.institution;
                                                                }
                                                                else if (inactiveLead.affiliation && inactiveLead.affiliation.length <= 6){
                                                                    leadPack.affiliation = '-';
                                                                }
                                                                leadPack.affiliation = parseAffiliation(leadPack.affiliation);
                                                                if (!pushIn){
                                                                    inactiveLeadArray.push(leadPack);
                                                                }
                                                            }
                                                        });
                                                    }
                                                    if (inactiveLeadArray.length == 0){
                                                        res.send({message : 'No leads found.', papers : finalResults.length});
                                                    }
                                                    else if (inactiveLeadArray.length > loggedFinal.credits){
                                                        res.send({success : 'Need more credits for full leads.', leads : inactiveLeadArray, identifier: historied._id.toString(), total : inactiveLeadArray.length, credits : loggedFinal.credits, difference : (inactiveLeadArray.length - loggedFinal.credits), price : Number(config.price), priceDifference : ((Number(config.price) * (inactiveLeadArray.length - loggedFinal.credits)) + Number(config.price))});
                                                    }
                                                    else {
                                                        res.send({success : 'Credits available for all leads.', leads : inactiveLeadArray, credits: loggedFinal.credits, identifier: historied._id.toString(), total : inactiveLeadArray.length});
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            });
                        }
                    });
                });
            }
        });
    });

    app.post('/stripeBlast', function(req, res){
        if (req.session && req.session.passport && req.session.passport.user && req.body.ids && req.body.ids[0] && !isNaN(Number(req.body.amount))){
            var identifier = req.body.identifier; 
            User.findById(req.session.passport.user).select('_id blastHistories emails creditHistories credits stripeCustomerId').exec(function(err,logged){
                if (!isEmpty(logged)){
                    var stripeToken = req.body.stripeToken;
                    var customered;
                    stripe.customers.create({
                        card: stripeToken,
                        description: logged._id.toString()
                    }).then(function(customer) {
                            console.log(customer);
                            customered = customer;
                            return stripe.charges.create({
                                amount: req.body.amount,
                                currency: "usd",
                                customer: customer.id
                            });
                        }).then(function(charge) {
                            if (charge && charge.id && req.body.ids && req.body.ids[0]){
                                req.body.ids = req.body.ids.getUnique();
                                logged.stripeCustomerId = customered.id;
                                var amount = (Number(req.body.amount) / 100).toFixed(2);
                                var credits = Math.floor((Number(req.body.amount) / 100 / Number(price)));
                                if (logged.credits){
                                    logged.credits = logged.credits + credits;
                                }
                                else {
                                    logged.credits = credits;
                                }
                                var history;
                                var currentAuthorCount = 0;
                                if (identifier && logged.blastHistories && logged.blastHistories[0]){
                                    history = logged.blastHistories.id(identifier);
                                    history.dateUpdated = new Date();
                                }
                                if (!isEmpty(history)){
                                    logged.creditHistories.push(new CreditHistory({date : new Date(), price : amount, amount : credits, payMethod : 'Stripe', paymentId : charge.id, type : 'leads', typeId : identifier.toString()}));
                                    Lead.find({_id: { $in: history.inactiveLeads}}).batchSize(100000).exec(function(err,inactiveLeads){
                                        if (!isEmpty(inactiveLeads)){
                                            if (!isEmpty(history) && history.inactiveLeads && history.inactiveLeads[0]){
                                                var newLead = false;
                                                var newLeads = 0;
                                                var newLeadsArray = [];
                                                inactiveLeads.forEach(function(author){
                                                    currentAuthorCount += 1;
                                                    if ((newLeads < logged.credits) && (req.body.ids.indexOf(author._id.toString()) > -1)){
                                                        newLeads += 1;
                                                        newLead = true;
                                                        history.leads.push(author._id);
                                                        logged.emails.push(author.email);
                                                        newLeadsArray.push(author.toObject());
                                                        // checkLinkedIn(author);
                                                    }
                                                });
                                                if (logged.emails && history.inactiveLeads && history.inactiveLeads[0]){
                                                    var finalList = [];
                                                    var finedList = [];
                                                    var finedEmail = [];
                                                    inactiveLeads.forEach(function(led2){
                                                        if ($.inArray(led2.email, logged.emails) < 0){
                                                            finedList.push(led2._id.toString());
                                                        }
                                                    });
                                                    history.inactiveLeads = finedList.getUnique();
                                                }
                                                if (!isEmpty(history) && history.leads && history.leads[0]){
                                                    if (newLead){
                                                        logged.credits = logged.credits - newLeads;
                                                        history.activated = true;
                                                        history.activatedDate = new Date();
                                                        history.leadCount = history.leads.length;
                                                        history.totalLeads = history.leads.length + history.inactiveLeads.length;

                                                        async.map(newLeadsArray, function (item, complete) {
                                                            var term;
                                                            var totalMatchTerm;
                                                            Paper.findOne({pubMedId : item.pubMedId}, function(err,paped){
                                                                if (!isEmpty(paped)){
                                                                    async.map(history.searchTerms, function (itemed, complete2) {
                                                                        if (paped.type == 'remote' && paped.json){
                                                                            var checkString = '';
                                                                            if (item.firstname){
                                                                                checkString += item.firstname;
                                                                            }
                                                                            if (item.lastname){
                                                                                checkString += ' ' + item.lastname;
                                                                            }
                                                                            if (item.institution){
                                                                                checkString += ' ' + item.institution;
                                                                            }
                                                                            if (item.department){
                                                                                checkString += ' ' + item.department;
                                                                            }
                                                                            if (item.affiliation){
                                                                                checkString += ' ' + item.affiliation;
                                                                            }
                                                                            var re = new RegExp(itemed,"ig");
                                                                            var matches = checkString.match(re);
                                                                            if (matches && matches[0]){

                                                                            }
                                                                            else {
                                                                                var ree = new RegExp(itemed,"ig");
                                                                                var matched = paped.json.match(ree);
                                                                                if (matched && matched[0]){
                                                                                    totalMatchTerm = itemed;
                                                                                }
                                                                                else {
                                                                                    term = itemed;
                                                                                }
                                                                            }
                                                                        }
                                                                        else if (paped.type == 'local' && paped.filename){
                                                                            var data = fs.readFileSync(paped.filename);
                                                                            data = data.toString();
                                                                            data = data.substring(data.indexOf('\n')+1);
                                                                            var matched = data.match(/\<\!DOCTYPE/i);
                                                                            if (matched && matched[0]){
                                                                                data = data.substring(data.indexOf('\n')+1);
                                                                            }
                                                                            var checkString = '';
                                                                            if (item.firstname){
                                                                                checkString += item.firstname;
                                                                            }
                                                                            if (item.lastname){
                                                                                checkString += ' ' + item.lastname;
                                                                            }
                                                                            if (item.institution){
                                                                                checkString += ' ' + item.institution;
                                                                            }
                                                                            if (item.department){
                                                                                checkString += ' ' + item.department;
                                                                            }
                                                                            if (item.affiliation){
                                                                                checkString += ' ' + item.affiliation;
                                                                            }
                                                                            var re = new RegExp(itemed,"ig");
                                                                            var matches = checkString.match(re);
                                                                            if (matches && matches[0]){

                                                                            }
                                                                            else {
                                                                                var ree = new RegExp(itemed,"ig");
                                                                                var matched = data.match(ree);
                                                                                if (matched && matched[0]){
                                                                                    totalMatchTerm = itemed;
                                                                                }
                                                                                else {
                                                                                    term = itemed;
                                                                                }
                                                                            }
                                                                        }
                                                                        complete2(null, itemed);
                                                                    }, function(err,results){
                                                                        item.term = term;
                                                                        item.totalMatchTerm = totalMatchTerm;
                                                                        complete(null, item);
                                                                    });
                                                                }
                                                                else {
                                                                    complete(null, item);
                                                                }
                                                            });
                                                        }, function(err,results){
                                                            results.forEach(function(red){
                                                                if (red.term){
                                                                    if (logged.leadTermArray && logged.leadTermArray[0]){

                                                                    }
                                                                    else {
                                                                        logged.leadTermArray = [];
                                                                    }
                                                                    logged.leadTermArray.push({_id : red._id.toString(), term : red.term});
                                                                }
                                                                else if (red.totalMatchTerm){
                                                                    if (logged.leadTermArray && logged.leadTermArray[0]){

                                                                    }
                                                                    else {
                                                                        logged.leadTermArray = [];
                                                                    }
                                                                    logged.leadTermArray.push({_id : red._id.toString(), term : red.totalMatchTerm});
                                                                }
                                                            });
                                                            logged.markModified('leadTermArray');
                                                            logged.save(function(err, used2){
                                                                if (!isEmpty(used2)){
                                                                    res.send({success : 'Leads obtained successfully.', identifier: history._id.toString(), credits : used2.credits, creditsNeeded : currentAuthorCount - newLeads, total : currentAuthorCount});
                                                                }
                                                            });
                                                        });
                                                    }
                                                }
                                                else {
                                                    res.send({message : 'No leads found.', papers : req.body.ids.length});
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    logged.save(function(err, used2){

                                    });
                                }
                            }
                        });
                }
            });
        }
    });

    app.post('/activateBlast', function(req,res){
        if (req.session && req.session.passport && req.session.passport.user && req.body.id && req.body.ids && req.body.ids[0]){
            User.findById(req.session.passport.user).select('_id blastHistories credits emails').exec(function(err,used3){
                if (!isEmpty(used3)){
                    req.body.ids = req.body.ids.getUnique();
                    var history = used3.blastHistories.id(req.body.id);
                    if (history && !isEmpty(history) && history.inactiveLeads && history.inactiveLeads[0]){
                        var currentAuthorCount = 0;
                        var newLead = false;
                        var newLeads = 0;
                        var newLeadsArray = [];

                        Lead.find({_id: { $in: history.inactiveLeads}}).batchSize(100000).exec(function(err,inactiveLeads){
                            if (!isEmpty(inactiveLeads)){
                                inactiveLeads.forEach(function(author){
                                    currentAuthorCount += 1;
                                    if ((newLeads < used3.credits) && (req.body.ids.indexOf(author._id.toString()) > -1)){
                                        newLead = true;
                                        newLeads += 1;
                                        history.leads.push(author._id);
                                        used3.emails.push(author.email);
                                        newLeadsArray.push(author.toObject());
                                        //checkLinkedIn(author);
                                    }
                                });
                                if (used3.emails && used3.emails[0] && history.inactiveLeads && history.inactiveLeads[0]){
                                    var finalList = [];
                                    var finedList = [];
                                    var finedEmail = [];
                                    var finalListed = [];

                                    inactiveLeads.forEach(function(led2){
                                        if ($.inArray(led2.email, used3.emails) < 0){
                                            finedList.push(led2._id.toString());
                                        }
                                    });

                                    history.inactiveLeads = finedList.getUnique();
                                }
                                if (newLead){
                                    used3.credits = used3.credits - newLeads;
                                    history.activated = true;
                                    history.activatedDate = new Date();
                                    history.leadCount = history.leads.length;
                                    history.totalLeads = history.leads.length + history.inactiveLeads.length;

                                    async.map(newLeadsArray, function (item, complete) {
                                        var term;
                                        var totalMatchTerm;
                                        Paper.findOne({pubMedId : item.pubMedId}, function(err,paped){
                                            if (!isEmpty(paped)){
                                                async.map(history.searchTerms, function (itemed, complete2) {
                                                    if (paped.type == 'remote' && paped.json){
                                                        var checkString = '';
                                                        if (item.firstname){
                                                            checkString += item.firstname;
                                                        }
                                                        if (item.lastname){
                                                            checkString += ' ' + item.lastname;
                                                        }
                                                        if (item.institution){
                                                            checkString += ' ' + item.institution;
                                                        }
                                                        if (item.department){
                                                            checkString += ' ' + item.department;
                                                        }
                                                        if (item.affiliation){
                                                            checkString += ' ' + item.affiliation;
                                                        }
                                                        var re = new RegExp(itemed,"ig");
                                                        var matches = checkString.match(re);
                                                        if (matches && matches[0]){

                                                        }
                                                        else {
                                                            var ree = new RegExp(itemed,"ig");
                                                            var matched = paped.json.match(ree);
                                                            if (matched && matched[0]){
                                                                totalMatchTerm = itemed;
                                                            }
                                                            else {
                                                                term = itemed;
                                                            }
                                                        }
                                                    }
                                                    else if (paped.type == 'local' && paped.filename){
                                                        var data = fs.readFileSync(paped.filename);
                                                        data = data.toString();
                                                        data = data.substring(data.indexOf('\n')+1);
                                                        var matched = data.match(/\<\!DOCTYPE/i);
                                                        if (matched && matched[0]){
                                                            data = data.substring(data.indexOf('\n')+1);
                                                        }
                                                        var checkString = '';
                                                        if (item.firstname){
                                                            checkString += item.firstname;
                                                        }
                                                        if (item.lastname){
                                                            checkString += ' ' + item.lastname;
                                                        }
                                                        if (item.institution){
                                                            checkString += ' ' + item.institution;
                                                        }
                                                        if (item.department){
                                                            checkString += ' ' + item.department;
                                                        }
                                                        if (item.affiliation){
                                                            checkString += ' ' + item.affiliation;
                                                        }
                                                        var re = new RegExp(itemed,"ig");
                                                        var matches = checkString.match(re);
                                                        if (matches && matches[0]){

                                                        }
                                                        else {
                                                            var ree = new RegExp(itemed,"ig");
                                                            var matched = data.match(ree);
                                                            if (matched && matched[0]){
                                                                totalMatchTerm = itemed;
                                                            }
                                                            else {
                                                                term = itemed;
                                                            }
                                                        }
                                                    }
                                                    complete2(null, itemed);
                                                }, function(err,results){
                                                    item.term = term;
                                                    item.totalMatchTerm = totalMatchTerm;
                                                    complete(null, item);
                                                });
                                            }
                                            else {
                                                complete(null, item);
                                            }
                                        });
                                    }, function(err,results){
                                        results.forEach(function(red){
                                            if (red.term){
                                                if (used3.leadTermArray && used3.leadTermArray[0]){

                                                }
                                                else {
                                                    used3.leadTermArray = [];
                                                }
                                                used3.leadTermArray.push({_id : red._id.toString(), term : red.term});
                                            }
                                            else if (red.totalMatchTerm){
                                                if (used3.leadTermArray && used3.leadTermArray[0]){

                                                }
                                                else {
                                                    used3.leadTermArray = [];
                                                }
                                                used3.leadTermArray.push({_id : red._id.toString(), term : red.totalMatchTerm});
                                            }
                                        });
                                        used3.markModified('leadTermArray');
                                        used3.save(function(err, used32){
                                            if (!isEmpty(used32)){
                                                res.send({success : 'Leads obtained successfully.', identifier: history._id.toString(), leads: true, credits : used32.credits, total : currentAuthorCount});
                                            }
                                        });
                                    });
                                }
                                else {
                                    res.send({error : 'Not enough credits.', identifier: history._id.toString(), credits : used3.credits, creditsNeeded : currentAuthorCount - newLeads, total : currentAuthorCount,  difference : (currentAuthorCount - newLeads), priceDifference : ((Number(config.price) * (currentAuthorCount - newLeads)) + Number(config.price))});
                                }
                            }
                        });
                    }
                }
            });
        }
    });

    app.get('/emailList/:id', function(req, res){
        if (req.session && req.session.passport && req.session.passport.user){
            User.findById(req.session.passport.user).select('_id lists tweeted linkedin credits name firstname lastname signature email organization title verified').populate('lists.leads').batchSize(100000).exec(function(err,used){
                if (!isEmpty(used) && used.lists && used.lists[0]){
                    var list = used.lists.id(req.params.id);
                    Lead.find({_id: { $in: list.leads.slice(0, 50)}}).batchSize(50).exec(function(err,leads){
                        if (!isEmpty(leads)){
                            list.leads = leads;
                            if (used.signature){
                                used.signature = used.signature.replace(/\r?\n|\r/g, '');
                            }
                            if (used.dontTalk && used.dontTalk[0]){
                                var finalLeads = [];
                                leads.forEach(function(led){
                                    if ($.inArray(led._id.toString(), used.dontTalk) < 0){
                                        finalLeads.push(led.toObject());
                                    }
                                });
                                list.leads = finalLeads;
                            }
                            var finished;
                            if (list.leads && list.leads.length <= 50){
                                finished = 'true';
                            }
                            else {
                                finished = 'false';
                            }
                            if (!isEmpty(list)){
                                res.render('email', {finished : finished, index : 50, userId : used._id.toString(), list : list, blasted : JSON.stringify(list), twitter : used.tweeted, linkedin : used.linkedin, stripeKey : config.stripePublic, credits : used.credits, firstname : used.firstname, lastname : used.lastname, signature : used.signature, organization : used.organization, title : used.title, id : used._id.toString(), usedIn: true, verified: used.verified, email: used.email});
                            }
                        }
                    });
                }
            });
        }
    });

    app.get('/emailBlast/:id', function(req, res){
        if (req.session && req.session.passport && req.session.passport.user){
            User.findById(req.session.passport.user).select('_id blastHistories tweeted linkedin credits name firstname lastname signature email organization title verified').exec(function(err,used){
                if (!isEmpty(used) && used.blastHistories && used.blastHistories[0]){
                    var blast = used.blastHistories.id(req.params.id);
                    if (used.signature){
                        used.signature = used.signature.replace(/\r?\n|\r/g, '');
                    }
                    if (!isEmpty(blast)){
                        var finalBlast = blast.toObject();

                        Lead.find({_id: { $in: blast.leads.slice(0, 50)}}).batchSize(50).exec(function(err,leads){
                            if (!isEmpty(leads)){
                                finalBlast.leads = leads;
                                if (req.query.postBlast && finalBlast.leads && finalBlast.leads[0] && used.alreadyEmailed && used.alreadyEmailed[0]){
                                    var finalLeads = [];
                                    leads.forEach(function(led){
                                        if ($.inArray(led._id.toString(), used.alreadyEmailed) < 0 && $.inArray(led._id.toString(), used.dontTalk) < 0){
                                            finalLeads.push(led.toObject());
                                        }
                                    });
                                    finalBlast.leads = finalLeads;
                                }
                                else if (used.dontTalk && used.dontTalk[0]){
                                    var finalLeads = [];
                                    leads.forEach(function(led){
                                        if ($.inArray(led._id.toString(), used.dontTalk) < 0){
                                            finalLeads.push(led.toObject());
                                        }
                                    });
                                    finalBlast.leads = finalLeads;
                                }
                                var finished;
                                if (blast.leads && blast.leads.length <= 50){
                                    finished = 'true';
                                }
                                else {
                                    finished = 'false';
                                }
                                res.render('email', {finished : finished, index : 50, userId : used._id.toString(), blast : finalBlast, blasted : JSON.stringify(finalBlast), twitter : used.tweeted, linkedin : used.linkedin, stripeKey : config.stripePublic, credits : used.credits, firstname : used.firstname, lastname : used.lastname, signature : used.signature, organization : used.organization, title : used.title, id : used._id.toString(), usedIn: true, verified: used.verified, email: used.email, firstname: used.firstname, lastname: used.lastname, signature: used.signature});
                           }
                        });
                    }
                }
            });
        }
    });

    app.post('/moreLeads', function(req, res){
        if (req.session && req.session.passport && req.session.passport.user && !isNaN(Number(req.body.index)) && req.body.id){
            User.findById(req.session.passport.user).select('_id blastHistories lists').exec(function(err,used){
                if (!isEmpty(used) && used.blastHistories && used.blastHistories[0]){
                    var blast;
                    if (used.blastHistories.id(req.body.id)){
                        blast = used.blastHistories.id(req.body.id);
                    }
                    else if (used.lists.id(req.body.id)){
                        blast = used.lists.id(req.body.id);
                    }
                    if (!isEmpty(blast)){
                        var finalBlast = blast.toObject();
                        Lead.find({_id: { $in: finalBlast.leads.slice(Number(req.body.index), Number(req.body.index) + 50)}}).batchSize(50).exec(function(err,leads){
                            if (!isEmpty(leads)){
                                var finished;
                                if (finalBlast.leads && Number(req.body.index) >= finalBlast.leads.length){
                                    finished = true;
                                }
                                else {
                                    finished = false;
                                }
                                res.send({success : 'success', finished : finished, index : Number(req.body.index) + 50, leads : leads});
                            }
                        });
                    }
                }
            });
        }
    });

    app.post('/sendBlast', function(req, res){
        if ((typeof req.body.templateMessage == 'string' && typeof req.body.emailFrom == 'string' && typeof req.body.nameFrom == 'string' && typeof req.body.subject == 'string' && typeof req.body.id == 'string' && req.session && req.session.passport && req.session.passport.user) || (typeof req.body.templateMessage2 == 'string' && typeof req.body.emailFrom2 == 'string' && typeof req.body.nameFrom2 == 'string' && typeof req.body.subject2 == 'string' && typeof req.body.id2 == 'string' && req.session && req.session.passport && req.session.passport.user)){
            User.findById(req.session.passport.user).exec(function(err,used){
                if (!isEmpty(used) && used.blastHistories){
                    var blast;
                    var attachments;
                    if (req.body.templateMessage2){
                        req.body.templateMessage = req.body.templateMessage2;
                        req.body.emailFrom = req.body.emailFrom2;
                        req.body.emails = req.body.emails2;
                        req.body.nameFrom = req.body.nameFrom2;
                        req.body.subject = req.body.subject2;
                        req.body.id = req.body.id2;
                        req.body.list = req.body.list2;
                        if (req.files && req.files[0]){
                            attachments = [];
                            req.files.forEach(function(filed){
                                var fileObject = {};
                                fileObject.fileName = filed.name;
                                fileObject.filePath = filed.path;
                                attachments.push(fileObject);
                            });
                        }
                    }

                    if (req.body.list){
                        blast = used.lists.id(req.body.id);
                    }
                    else {
                        blast = used.blastHistories.id(req.body.id);
                    }
                    if (blast.leads){
                        var emailBlast = new EmailBlast({});
                        emailBlast.emailFrom = req.body.emailFrom;
                        emailBlast.nameFrom = req.body.nameFrom
                        emailBlast.templateMessage = req.body.templateMessage;
                        emailBlast.subject = req.body.subject;
                        emailBlast.signature = req.body.signature;
                        emailBlast.date = new Date();
                        emailBlast.followUpMessage = req.body.followUpMessage;
                        emailBlast.attachments = attachments;
                        emailBlast.leads = [];

                        var authorCount = 0;
                        var finalNewArray = [];

                        blast.leads.forEach(function(author){
                            emailBlast.leads.push(author);
                            if ($.inArray(author.toString(), used.dontTalk) < 0){
                                if (author.email && $.inArray(author.toString(), used.alreadyEmailed) >= 0){
                                    authorCount += 1;
                                }
                                else if (author.email && $.inArray(author.toString(), used.alreadyEmailed) < 0){
                                    finalNewArray.push(author.toString());
                                }
                            }
                        });

                        if (authorCount <= used.credits){
                            emailBlast.activated = true;
                            used.credits = used.credits - authorCount;
                            emailBlast.creditsSpent = authorCount;
                            if (finalNewArray && finalNewArray[0]){
                                finalNewArray.forEach(function(finalNew){
                                    used.alreadyEmailed.push(finalNew);
                                });
                            }
                        }

                        emailBlast.markModified('attachments');

                        if (req.body.list){
                            emailBlast.type = 'list';
                        }
                        else {
                            emailBlast.type = 'blast';
                        }

                        emailBlast.blastId = blast._id.toString();

                        var tomorrow = new Date();

                        if (req.body.pickDate && req.body.dateSend){
                            emailBlast.dateSend = req.body.dateSend;
                            tomorrow.setDate(new Date(emailBlast.dateSend).getDate() + 14);
                        }
                        else {
                            tomorrow.setDate(new Date().getDate() + 14);
                        }

                        if (emailBlast.followUpMessage){
                            emailBlast.dateFollowUpSend = tomorrow;
                        }

                        used.emailBlasts.push(emailBlast);
                        used.save(function(err, finalUsed){
                            if (!isEmpty(finalUsed)){
                                if (emailBlast.activated){
                                    if (req.body.followUpMessage){
                                        process.send({ add : {type : 'followUp', id : emailBlast._id, userId : used._id.toString()} });
                                    }

                                    process.send({ add : {type : 'emailMain', id : emailBlast._id, userId : used._id.toString()} });

                                    res.send({success : 'Email blast successful.'});
                                }
                                else {
                                    res.send({more : 'Need more credits.', identifier: emailBlast._id.toString(), totalMessages : emailBlast.leads.length, total : authorCount, credits : used.credits, difference : (authorCount - used.credits), price : Number(config.price), priceDifference : ((Number(config.price) * (authorCount - used.credits)) + Number(config.price))});
                                }
                            }
                        });

                    }
                }
            });
        }
    });

    app.post('/autoRefresh', function(req, res){
        if (req.session && req.session.passport && req.session.passport.user && req.body.refreshId && req.body.on){
            User.findById(req.session.passport.user, '_id blastHistories._id blastHistories.autoRefresh blastHistories.activated', function(err,used){
                if (!isEmpty(used)){
                    var blast = used.blastHistories.id(req.body.refreshId);
                    if (!isEmpty(blast)){
                        blast.autoRefresh = req.body.on;
                        used.save(function(err,uo){
                            if (!isEmpty(uo)){
                                if (req.body.on){
                                    process.send({ add : {type : 'refresh', id : req.body.refreshId, userId : used._id.toString()} });
                                }
                                else {
                                    process.send({ cancel : {id : req.body.refreshId, type : 'refresh'} });
                                }
                                res.send({success : 'Success'});
                            }
                        });
                    }
                }
            });
        }
    });

    app.post('/activateEmailBlast', function(req, res){
        if (req.body.blastId && req.session && req.session.passport && req.session.passport.user && req.body.stripeToken && !isNaN(req.body.amount)){
            User.findById(req.session.passport.user).select('-lists -blastHistories').exec(function(err,used){
                if (!isEmpty(used)){
                    var stripeToken = req.body.stripeToken;
                    var customered;
                    stripe.customers.create({
                        card: stripeToken,
                        description: used._id.toString()
                    }).then(function(customer) {
                            customered = customer.id;
                            return stripe.charges.create({
                                amount: req.body.amount,
                                currency: "usd",
                                customer: customer.id
                            });
                        }).then(function(charge) {
                            if (charge && charge.id && charge.paid){
                                var blast = used.emailBlasts.id(req.body.blastId);

                                if (!isEmpty(blast)){
                                    used.stripeCustomerId = customered;
                                    var amount = (Number(req.body.amount) / 100).toFixed(2);
                                    var credits = Math.floor((Number(req.body.amount) / 100 / Number(price)));
                                    if (used.credits){
                                        used.credits = used.credits + credits;
                                    }
                                    else {
                                        used.credits = credits;
                                    }

                                    blast.activated = true;

                                    var authorCount = 0;
                                    var finalNewArray = [];

                                    Lead.find({_id: { $in: blast.leads}}).batchSize(100000).exec(function(err,leads){
                                        if (!isEmpty(leads)){
                                            leads.forEach(function(author){
                                                if (author.email && $.inArray(author._id.toString(), used.alreadyEmailed) >= 0){
                                                    authorCount += 1;
                                                }
                                                else if (author.email && $.inArray(author._id.toString(), used.alreadyEmailed) < 0){
                                                    finalNewArray.push(author._id.toString());
                                                }
                                            });

                                            if (authorCount <= used.credits){
                                                blast.activated = true;
                                                used.credits = used.credits - authorCount;
                                                blast.creditsSpent = authorCount;
                                                if (finalNewArray && finalNewArray[0]){
                                                    finalNewArray.forEach(function(finalNew){
                                                        used.alreadyEmailed.push(finalNew);
                                                    });
                                                }
                                            }

                                            used.creditHistories.push(new CreditHistory({date : new Date(), price : amount, amount : credits, payMethod : 'Stripe', paymentId : charge.id, type : 'email', typeId : blast._id.toString()}));
                                            used.save(function(err,logged){
                                                if (!isEmpty(logged)){
                                                    if (blast.activated == true){
                                                        if (blast.followUpMessage){
                                                            process.send({ add : {type : 'followUp', id : blast._id, userId : used._id.toString()} });
                                                        }

                                                        process.send({ add : {type : 'emailMain', id : blast._id, userId : used._id.toString()} });
                                                        res.send({success : 'Email blast successful.'});
                                                    }
                                                }
                                            });
                                        }
                                    });
                                }
                            }
                        });
                }
            });
        }
    });

    app.post('/editFollowUp', function(req, res){
        if (req.session && req.session.passport && req.session.passport.user && req.body.blastId){
            User.findById(req.session.passport.user).select('_id emailBlasts._id emailBlasts.followUpDate emailBlasts.dateFollowUpSend').exec(function(err,used){
                if (!isEmpty(used)){
                    var blast = used.emailBlasts.id(req.body.blastId);
                    if (!isEmpty(blast) && !blast.followUpDate){
                        blast.dateFollowUpSend = req.body.dateFollowUpSend;
                        used.save(function(err,user){
                            if (!isEmpty(user)){
                                var emailBlast = user.emailBlasts.id(req.body.blastId);
                                if (!isEmpty(emailBlast)){
                                    process.send({ cancel : {id : emailBlast._id, type : 'followUp'} });
                                    var dated = new Date(req.body.dateFollowUpSend);
                                    if (req.body.dateFollowUpSend && typeof dated.getMonth === 'function'){
                                        process.send({ add : {type : 'followUp', id : emailBlast._id, userId : user._id.toString()} });
                                    }
                                    res.send({success : 'success', identifier : emailBlast._id.toString(), dateFollowUpSend : req.body.dateFollowUpSend});
                                }
                            }
                        });
                    }
                }
            });
        }
    });

    app.post('/cancelFollowUp', function(req, res){
        if (req.session && req.session.passport && req.session.passport.user && req.body.blastId){
            User.findById(req.session.passport.user, '_id emailBlasts._id emailBlasts.followUpDate emailBlasts.dateFollowUpSend', function(err,used){
                if (!isEmpty(used)){
                    var blast = used.emailBlasts.id(req.body.blastId);
                    if (!isEmpty(blast) && !blast.followUpDate){
                        blast.dateFollowUpSend = req.body.dateFollowUpSend;
                        used.save(function(err,user){
                            if (!isEmpty(user)){
                                var blasted = user.emailBlasts.id(req.body.blastId);
                                if (!isEmpty(blasted)){
                                    process.send({ cancel : {id : blasted._id, type : 'followUp'} });
                                    res.send({success : 'success', identifier : blasted._id.toString(), dateFollowUpSend : req.body.dateFollowUpSend});
                                }
                            }
                        });
                    }
                }
            });
        }
    });

    app.post('/editEmailMain', function(req, res){
        var dated = new Date(req.body.dateSend);
        if (req.session && req.session.passport && req.session.passport.user && req.body.blastId && req.body.dateSend && typeof dated.getMonth === 'function'){
            User.findById(req.session.passport.user).select('_id emailBlasts._id emailBlasts.sentDate emailBlasts.dateSend emailBlasts.followUpDate emailBlasts.dateFollowUpSend').exec(function(err,used){
                if (!isEmpty(used)){
                    var blast = used.emailBlasts.id(req.body.blastId);
                    if (!isEmpty(blast) && !blast.sentDate){
                        blast.dateSend = req.body.dateSend;

                        var tomorrow = new Date();

                        tomorrow.setDate(new Date(blast.dateSend).getDate() + 14);

                        blast.dateFollowUpSend = tomorrow;
                        used.save(function(err,user){
                            if (!isEmpty(user)){
                                var emailBlast = user.emailBlasts.id(req.body.blastId);
                                if (!isEmpty(emailBlast)){
                                    process.send({ cancel : {id : emailBlast._id, type : 'emailMain'} });
                                    process.send({ cancel : {id : emailBlast._id, type : 'followUp'} });
                                    process.send({ add : {type : 'followUp', id : emailBlast._id, userId : user._id.toString()} });
                                    process.send({ add : {type : 'emailMain', id : emailBlast._id, userId : user._id.toString()} });
                                    res.send({success : 'success',  identifier : emailBlast._id.toString(), dateFollowUpSend : tomorrow, dateSend : req.body.dateSend});
                                }
                            }
                        });
                    }
                }
            });
        }
    });

    app.post('/cancelEmailMain', function(req, res){
        if (req.session && req.session.passport && req.session.passport.user && req.body.blastId){
            User.findById(req.session.passport.user, '_id emailBlasts._id emailBlasts.sentDate emailBlasts.dateSend emailBlasts.followUpDate emailBlasts.dateFollowUpSend credits', function(err,used){
                if (!isEmpty(used)){
                    var blast = used.emailBlasts.id(req.body.blastId);
                    if (!isEmpty(blast) && !blast.sentDate){
                        blast.dateSend = req.body.dateSend;
                        blast.dateFollowUpSend = req.body.dateFollowUpSend;
                        if (!isNaN(blast.creditsSpent) && blast.creditsSpent > 0){
                            used.credits = used.credits + blast.creditsSpent;
                        }
                        used.save(function(err,user){
                            if (!isEmpty(user)){
                                var blasted = user.emailBlasts.id(req.body.blastId);
                                if (!isEmpty(blasted)){
                                    process.send({ cancel : {id : blasted._id, type : 'emailMain'} });
                                    process.send({ cancel : {id : blasted._id, type : 'followUp'} });
                                    res.send({success : 'success', identifier : blasted._id.toString(), dateFollowUpSend : req.body.dateFollowUpSend, dateSend : req.body.dateSend, credits : user.credits});
                                }
                            }
                        });
                    }
                }
            });
        }
    });

    function parseEmail(affied){
        affied = affied.replace(/ Email\: (?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])\./, '');
        affied = affied.replace(/ Electronic address\: (?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])\./, '');
        affied = affied.replace(/ (?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])./, '');
        affied = affied.replace(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/, '');
        return affied;
    }

    function parseAffiliation(affied){
        if (affied){
            affied = affied.replace(/ Email\: (?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])\./, '');
            affied = affied.replace(/ Electronic address\: (?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])\./, '');
            affied = affied.replace(/ (?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])./, '');
            affied = affied.replace(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/, '');
            affied = affied.split(';')[0];
            var matcherComma = affied.match(/,/g);
            if (matcherComma && matcherComma.length >= 4){
                var affie = affied.split(',');
                affied = affie[0] + ',' + affie[1] + ',' + affie[2];
            }
            else if (matcherComma && (matcherComma.length == 2 || matcherComma.length == 3)){
                var affie = affied.split(',');
                affied = affie[0] + ',' + affie[1];
            }
            else {
                var affie = affied.split(',');
                affied = affie[0];
            }
        }
        return affied;
    }

    function parseResults(jsonText2, finalIdArray){
        var paperArray = [];

        if (jsonText2.PubmedArticleSet && jsonText2.PubmedArticleSet.PubmedArticle){
            jsonText2.PubmedArticleSet.PubmedArticle.forEach(function(article){
                var paperObject = {};
                paperObject.json = article;
                if (article.PubmedData && article.PubmedData[0].ArticleIdList && article.PubmedData[0].ArticleIdList[0] && article.PubmedData[0].ArticleIdList[0].ArticleId){
                    var pubmedId;

                    article.PubmedData[0].ArticleIdList[0].ArticleId.forEach(function(pub){
                        if (pub['$'].IdType == 'pubmed'){
                            pubmedId = pub['_'];
                        }
                    });
                    paperObject.id = pubmedId;
                    finalIdArray.forEach(function(idded){
                        if (idded.id == paperObject.id){
                            paperObject.searchTerm = idded.term;
                        }
                    });

                    if (article.MedlineCitation[0].Article){

                        if (article.MedlineCitation[0].Article[0].ArticleDate){
                            paperObject.date = new Date(article.MedlineCitation[0].Article[0].ArticleDate[0].Year[0], article.MedlineCitation[0].Article[0].ArticleDate[0].Month[0], article.MedlineCitation[0].Article[0].ArticleDate[0].Day[0]);
                        }
                        paperObject.title = article.MedlineCitation[0].Article[0].ArticleTitle[0];

                        var authorList = [];
                        if (article.MedlineCitation[0].Article && article.MedlineCitation[0].Article[0].AuthorList){

                            if (article.MedlineCitation[0].Article[0].AuthorList[0].Author[0]){
                                article.MedlineCitation[0].Article[0].AuthorList[0].Author.forEach(function(auth){
                                    var authorObject = {};
                                    if (auth.ForeName){
                                        authorObject.firstname = auth.ForeName[0];
                                    }
                                    if (auth.Initials){
                                        authorObject.initials = auth.Initials[0];
                                    }
                                    if (auth.LastName){
                                        authorObject.lastname = auth.LastName[0];
                                    }
                                    if (auth.Affiliation){
                                        authorObject.affiliation = auth.Affiliation[0];
                                        authorList.push(authorObject);
                                    }

                                });
                                if (authorList && authorList[0]){
                                    paperObject.authors = authorList;
                                    paperArray.push(paperObject);
                                }
                            }
                        }
                    }
                }
            });
        }
        return paperArray;
    }
}